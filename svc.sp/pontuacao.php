<?php

session_start();

include_once "../config.php";
include_once "../inc.php";
include_once "../db.php";
include_once "../query.php";

header('content-type: application/json; charset=utf-8');

$cpfcnpj   = trim($_POST['cpf']);
$senha     = trim($_POST['senha']);
$email     = trim($_POST['email']);

// Usu�rio sem CNH
//$cpfcnpj = '27984318832';
//$senha = 'teste123';

// Usu�rio com CNH e 1 multa
//$cpfcnpj = '27604097839';
//$senha = 'a1445e356';

// Usu�rio com CNH e varias multas
//$cpfcnpj = '10766533832';
//$senha = 'wcp9002332';


if(!$cpfcnpj or !$senha)
{
   $json = array("erro" => "Um ou mais parametros invalidos", 'id_erro' => '1');
   echo jsonp_encode($json);
   exit;
}

// executa comandos de login no sistema e busca o token de sess�o
require 'login-detran.php';

// Testar se o usu�rio possu� CNH em SP
if(!$ws_login->numeroCnh)
{
    $json = array("erro" => utf8_encode("O usu�rio $ws_login->nome n�o possui CNH no Estado de S�o Paulo!"), 'id_erro' => '1');
    echo json_encode($json);
    exit;
}


// Inicializando variaveis
$dados_condutor =  array(); // saida do json

$dados_condutor['erro'] = 'nao';
$dados_condutor['id_erro'] = '0';
$dados_condutor['condutor'] = array(); // ficha do condutor
$dados_condutor['total_pontos'] = 0;
$dados_condutor['pontos'] = array();

$dados_condutor['cpf'] = $cpfcnpj;
$dados_condutor['cnh'] = $ws_login->numeroCnh;


// -------------------------------------------------------------------------------
// Buscar pontuacao da CNH
// -------------------------------------------------------------------------------

$url = "http://mobile.detran.sp.gov.br/DetranWsMultas/obterPontos";

$parametros = array(
    "token: $ws_login->senha",
    "tipo: 1",
    "numero: $ws_login->numeroCnh",
    "dataNascimento: ".date("d/m/Y", rand(53938,694277938))
);

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HTTPHEADER, $parametros);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setproxy($ch, 'sp');
curl_setopt($ch, CURLOPT_USERAGENT, "Apache-HttpClient/UNAVAILABLE (java 1.4)");

$ws_dados = json_decode(curl_exec($ch));

if(!$ws_dados)
{
    $json = array("erro" => utf8_encode("Servidor do Detran-SP temporariamente indispon�vel... :("), 'id_erro' => '1');
    echo json_encode($json);
    exit;
}

if($ws_dados->codErro)
{
    $json = array("erro" => utf8_encode($ws_dados->mensagem), 'id_erro' => $ws_dados->codErro);
    echo json_encode($json);
    exit;
}

// -------------------------------------------------------------------------------
// fazendo parse da pontuacao do condutor
// -------------------------------------------------------------------------------

if(sizeof($ws_dados->multas) == 1)
{
    $dados_condutor['pontos'][] = array(
        utf8_encode('Infra��o') => $ws_dados->multas->infracao,
        utf8_encode('Munic�pio') => $ws_dados->multas->municipioInfra,
        'Placa' => $ws_dados->multas->placa,
        'Local' => $ws_dados->multas->localInfra,
        'Data' => $ws_dados->multas->dataInfra,
        'Hora' => $ws_dados->multas->horaInfra,
        'Pontos' => $ws_dados->multas->pontos
    );
}
else
{
    foreach($ws_dados->multas as $multa)
    {
        $dados_condutor['pontos'][] = array(
            utf8_encode('Infra��o') => $multa->infracao,
            utf8_encode('Munic�pio') => $multa->municipioInfra,
            'Placa' => $multa->placa,
            'Local' => $multa->localInfra,
            'Data' => $multa->dataInfra,
            'Hora' => $multa->horaInfra,
            'Pontos' => $multa->pontos
        );
    }
}

$dados_condutor['total_pontos'] = intval($ws_dados->qtdPontos);


// -------------------------------------------------------------------------------
// montando o vetor json a pontuacao
// -------------------------------------------------------------------------------

$dados_condutor['condutor'] = array(
    'Nome' => $ws_dados->nome,
    'CNH' => $ws_dados->registro,
    'CPF' => $cpfcnpj
);

$json = jsonp_encode($dados_condutor);

if(!empty($email))
{
    $db = new db();
    $db->query("call add_log_condutor('SP', '".$email."', '".$cnh."', '', '', '".$_SESSION['cpfcnpj']."', ".$dados_condutor['total_pontos'].", '".$json."');");
}

echo $json;








