<?php

session_start();

include_once "../config.php";
include_once "../inc.php";
include_once "../db.php";
include_once "../query.php";

header('content-type: application/json; charset=utf-8');

// Buscando parametros de consulta
$cpfcnpj   = trim($_POST['cpfcnpj']);
$senha     = trim($_POST['senha']);
$renavam   = trim($_POST['renavan']);
$placa     = strtoupper(trim($_POST['placa']));
$email     = trim($_POST['email']);

// Usu�rio com muitas multas
//$cpfcnpj  = '27984318832';
//$senha    = 'teste123';
//$renavam  = '119860198';
//$placa    = 'EGY3722';

// Inicializando variaveis
$dados_veiculo =  array(); // saida do json

$dados_veiculo['erro'] = 'nao';
$dados_veiculo['id_erro'] = '0';

$dados_veiculo['placa'] = null;
$dados_veiculo['chassi'] = null;
$dados_veiculo['renavam'] = $renavam;
$dados_veiculo['cpf'] = $cpfcnpj;

$dados_veiculo['veiculo'] = array();
$dados_veiculo['imposto'] = array();

$dados_veiculo['infracao']['multas'] = array();
$dados_veiculo['infracao']['autuacoes'] = array();


if(!$cpfcnpj or !$senha or !$renavam or !$placa)
{
   $json = array("erro" => "Um ou mais parametros invalidos", 'id_erro' => '1');
   echo jsonp_encode($json);
   exit;
}

// executa comandos de login no sistema e busca o token de sess�o
require 'login-detran.php';

// -----------------------------------------------------------------------------
// Buscar DADOS do ve�culo
// -----------------------------------------------------------------------------

$url = "http://mobile.detran.sp.gov.br/DetranWsMultas/obterRestricoesVeiculo";

$parametros = array(
    "renavam: $renavam",
    "placa: $placa"
);

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HTTPHEADER, $parametros);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setproxy($ch, 'sp');
curl_setopt($ch, CURLOPT_USERAGENT, "Apache-HttpClient/UNAVAILABLE (java 1.4)");

$ws_dados = json_decode(curl_exec($ch));

if(!$ws_dados)
{
    $json = array("erro" => utf8_encode("Servidor do Detran-SP temporariamente indispon�vel... :("), 'id_erro' => '1');
    echo json_encode($json);
    exit;
}

if($ws_dados->codErro)
{
    $json = array("erro" => utf8_encode($ws_dados->mensagem), 'id_erro' => $ws_dados->codErro);
    echo json_encode($json);
    exit;
}

// -------------------------------------------------------------------------------
// Buscar MULTAS do ve�culo
// -------------------------------------------------------------------------------

$url = "http://mobile.detran.sp.gov.br/DetranWsMultas/obterMultasVeiculo";

$parametros = array(
    "renavam: $renavam",
    "token: $ws_login->senha"
);

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HTTPHEADER, $parametros);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setproxy($ch, 'sp');
curl_setopt($ch, CURLOPT_USERAGENT, "Apache-HttpClient/UNAVAILABLE (java 1.4)");

$ws_multas = json_decode(curl_exec($ch));

if(!$ws_multas)
{
    $json = array("erro" => utf8_encode("Servidor do Detran-SP temporariamente indispon�vel... :("), 'id_erro' => '1');
    echo json_encode($json);
    exit;
}

if($ws_multas->codErro && $ws_multas->codErro != "317")
{
    $json = array("erro" => utf8_encode($ws_multas->mensagem), 'id_erro' => $ws_multas->codErro);
    echo json_encode($json);
    exit;
}

// -------------------------------------------------------------------------------
// montando o vetor json com os dados do ve�culo
// -------------------------------------------------------------------------------


$marca_modelo = preg_split('/\//sim', $ws_dados->marca);

$dados_veiculo['placa'] = $placa;

$dados_veiculo['veiculo']['Placa'] = $placa;
$dados_veiculo['veiculo']['Renavam'] = $renavam;
$dados_veiculo['veiculo']['Marca'] = $marca_modelo[0];
$dados_veiculo['veiculo']['Modelo'] = $marca_modelo[1];
$dados_veiculo['veiculo']['Cor'] = $ws_dados->cor;
$dados_veiculo['veiculo'][utf8_encode('Munic�pio')] = $ws_dados->descMunicipio;

$dados_veiculo['veiculo'][utf8_encode('Ano Fabrica��o')] = $ws_dados->anoFabricacao;
$dados_veiculo['veiculo']['Ano Modelo'] = $ws_dados->anoModelo;

$dados_veiculo['veiculo']['Categoria'] = $ws_dados->categoria;
$dados_veiculo['veiculo'][utf8_encode('Combust�vel')] = $ws_dados->combustivel;
$dados_veiculo['veiculo']['Tipo'] = $ws_dados->tipo;

$dados_veiculo['veiculo']['Roubo/Furto']                             = $ws_dados->bloqueioFurto;
$dados_veiculo['veiculo'][utf8_encode('Restri��o Administrativa')]   = $ws_dados->restricaoADM;
$dados_veiculo['veiculo'][utf8_encode('Restri��o Tribut�ria')]       = $ws_dados->restricaoTributaria;
$dados_veiculo['veiculo'][utf8_encode('Restri��o Judici�ria')]       = $ws_dados->restricaoJudiciaria;
$dados_veiculo['veiculo'][utf8_encode('Restri��o Financeira')]       = $ws_dados->restricaoFinanceira;
$dados_veiculo['veiculo'][utf8_encode('Restri��o Guincho')]          = $ws_dados->registroGuincho;
$dados_veiculo['veiculo'][utf8_encode("Inspe��o veicular")]          = $ws_dados->inspecaoVeicular;

$dados_veiculo['veiculo'][utf8_encode("Observa��o")]                 = $ws_dados->obs;

$dados_veiculo['imposto']['IPVA'][date("Y")] = $ws_dados->IPVA;
$dados_veiculo['imposto']['Licenciamento'][$ws_dados->anoLicenciamento] = '';

// -------------------------------------------------------------------------------
// Montar tabela de multas
// -------------------------------------------------------------------------------


if(sizeof($ws_multas->multas) == 1) {
    $dados_veiculo['infracao']['multas'][$ws_multas->multas->municipioInfra][] = array(
        utf8_encode('Infra��o') => $ws_multas->multas->infracao,
        'Data' => $ws_multas->multas->dataInfra,
        'Hora' => $ws_multas->multas->horaInfra,
        'Local' => $ws_multas->multas->localInfra,
        utf8_encode('Munic�pio') => $ws_multas->multas->municipioInfra,
        'UF' => $ws_multas->multas->uf,
        'Valor' => $ws_multas->multas->valorInfra
    );
}
else {
    foreach ($ws_multas->multas as $multa) {

        $dados_veiculo['infracao']['multas'][$multa->municipioInfra][] = array(
            utf8_encode('Infra��o') => $multa->infracao,
            'Data' => $multa->dataInfra,
            'Hora' => $multa->horaInfra,
            'Local' => $multa->localInfra,
            utf8_encode('Munic�pio') => $multa->municipioInfra,
            'UF' => $multa->uf,
            'Valor' => $multa->valorInfra
        );
    }
}

//print_r($dados_veiculo);
//exit;

$json = jsonp_encode($dados_veiculo);

if(!empty($email))
{
    $db = new db();
    $db->query("call add_log_veiculo ('SP', '".$email."', '".$dados_veiculo['placa']."', '', '".$dados_veiculo['renavam']."', '".$dados_veiculo['cpf']."', '".$json."');");
}

echo $json;








