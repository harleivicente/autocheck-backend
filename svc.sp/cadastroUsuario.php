<?php

require_once "./lib/SpLibCadastroUsuario.php";
require_once "../library/SpLib.php";

/* 
    @param $_POST['cpf'] - 01718985169
    @param $_POST['nome']
    @param $_POST['data_nascimento'] - 27/10/1990
    @param $_POST['email']
    @param $_POST['captcha']
    @param $_POST['token'] - dado obtido com cadastroUsuarioPre.php

    Formato do token:
        token: {
            action: '',
            post_vars: '',
            captcha_url: '',
            cookie: ''
        }

    @return json of {
        erro: 'sim' | 'nao',
        id_erro: numero 
            1 - parametros incorretos
            2 - email já cadastrado
            4 - captcha errado
            3 - other
    }

*/

set_error_handler(function($code, $msg) {
    echo json_encode(array ('erro' => 'sim', 'id_erro' => '3','msg' => $msg));
    exit;
});

$params = array("cpf", "nome", "data_nascimento", "email", "captcha");
foreach ($params as $key => $param) {
    if(!array_key_exists($param, $_POST)){
        echo json_encode(array ('erro' => 'sim', 'id_erro' => '1', 'msg' => "Parametros incorretos."));
        exit;
    }
}

$token = (object) $_POST['token'];
$token->post_vars = (object) $token->post_vars;

$dados = array(
    'nome' => $_POST['nome'],
    'dataNascimento' => $_POST['data_nascimento'],
    'email' => $_POST['email'],
    'captcha' => $_POST['captcha'],
    'cpf' => $_POST['cpf']    
);

$resultado = SpLibCadastroUsuario::cadastrarUsuario($token->cookie, $dados, $token);

if($resultado == "email_usado") {
    echo json_encode(array ('erro' => 'sim', 'id_erro' => '2'));
} elseif ($resultado == "ok") {
    echo json_encode(array ('erro' => 'nao', 'id_erro' => '0'));
} else if ($resultado == "captcha") {
    echo json_encode(array ('erro' => 'sim', 'id_erro' => '4'));
} else {
    echo json_encode(array ('erro' => 'sim', 'id_erro' => '3'));
}