<?php

include_once '../config.php';

/*
   Executa comandos de login no sistema e guarda o token de sess�o
*/
// Usu�rio de teste
//$cpfcnpj = '27984318832';
//$senha = 'teste123';

// -----------------------------------------------------------------------------
// Fazer login no sistema
// -----------------------------------------------------------------------------

$url = "http://mobile.detran.sp.gov.br/DetranWsMultas/autenticar";

$loginSenha = array(
    "userId: $cpfcnpj",
    "userPass: $senha"
);

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HTTPHEADER, $loginSenha);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setproxy($ch, 'sp');
curl_setopt($ch, CURLOPT_USERAGENT, "Apache-HttpClient/UNAVAILABLE (java 1.4)");

$ws_login = json_decode(curl_exec($ch));

if(!$ws_login)
{
    $json = array("erro" => utf8_encode("Servidor do Detran-SP temporariamente indispon�vel... :("), 'id_erro' => '1');
    echo json_encode($json);
    exit;
}

if($ws_login->codErro)
{
    $json = array("erro" => utf8_encode($ws_login->mensagem), 'id_erro' => $ws_login->codErro);
    echo json_encode($json);
    exit;
}


