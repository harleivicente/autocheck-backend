<?php

require_once "../vendor/autoload.php";
require_once "../library/SpLib.php";
require_once "./SpConfig.php";

use PHPHtmlParser\Dom;
use PHPHtmlParser\Dom\HtmlNode;


class SpLibTestarUsuario {

        /* 
        Extrai o layout do form que deve ser enviado
        durante o log in.

        @cookie

        @return stdClass (
            'post_vars' => stdClass (
                '123123:nome' => '', ...
            ),
            'action' => ''
        )

        @triggers error
    */
    static function extrairFormDataLogin ($cookie) {
        $headers = array(SpLib::makeCookieHeader($cookie));
        $resultado = new stdClass();

        $pagina = SpLib::curl_get(DETRAN_HOME, array(), array(
            CURLOPT_HEADER => 0,
            CURLOPT_FOLLOWLOCATION => 1            
        ), $headers);

        // Interpretar o dom        
        $dom = new Dom;
        if(!$dom->load($pagina))
            trigger_error("Dom parsing failed");

        // Identificar form
        $forms = $dom->find('div#areaLoginModal > form');
        if(!$forms || sizeof($forms) != 3) 
            trigger_error("Failed to find form in html.");
        $form = $forms[1];
        
        // Extrair dados do form
        $formLayout = SpLib::extrairFormatoDeForm($form);
        if(!$formLayout) {
            trigger_error("Failed to extract form.");
        }
        $resultado->action = $formLayout->action;
        $resultado->post_vars = $formLayout->post_vars;
 
        return $resultado;          
    }

    /*
        @param $cookie

        @param $formData - Estrutura do form de login no home page.
            stdClass(
                action => '',
                post_vars => ''
            )
        @param $cpf 
        @param $senha

        @return stdClass(
            status => STRING ['ok', 'primeiro_acesso', 'erro'],
            token => STRING
        )
    
        @triggers error
    */
    static function obterLtpaToken($cookie, $formData, $cpf, $senha) {
        $resposta = new stdClass();

        $formData->post_vars = SpLib::preencherVariaveisPost($formData->post_vars, (object) array(
            'urlRedirect' => '/wps/myportal/portaldetran/cidadao/servicos/meuDetran',
            'ipUsuario' => SpLib::randomIp(),
            'numeroLogin' => $cpf,
            'senhaLogin' => $senha
        ));

        $result = SpLib::curl_post($formData->action, $formData->post_vars, array(
            CURLOPT_HEADER => 1,
            CURLOPT_FOLLOWLOCATION => 0
        ), array(
            SpLib::makeCookieHeader($cookie))
        );

        /* 
            Procurar pela definicao do cookie: Set-Cookie: LtpaToken2 ou
            por um redirect para a pagina de primeiro acesso.
        */
        $encontrou = preg_match('/LtpaToken2=(?<cookie>[^;]+);/i', $result, $matches);

        if($matches && $matches['cookie']) {
            $resposta->status = 'ok';
            $resposta->token = $matches['cookie'];
        } elseif(preg_match('/servicos\/primeiroAcesso/i', $result)) {
            $resposta->status = "primeiro_acesso";
        } else {
            $resposta->status = "erro";
        }
        return $resposta;
    }

    /* 
        Acessa o página do detran e verifica se usuario está logado.

        @param $cookie - JSESSIONID=00013ASDFA43334df ...
        @param $ltpaToken STRING || FALSE 00013ASDFA43334df...

        @return STRING
            'valido',
            'invalido'

        @triggers error
    */
    static function verificarUsuario($cookie, $ltpaToken) {
        $url = "https://www.detran.sp.gov.br/wps/myportal/portaldetran/cidadao/servicos/meuDetran";

        $cookieHeader = "Cookie: detransp_cookie=true; ";
        $cookieHeader .= $cookie . "; ";
        $cookieHeader .= "LtpaToken2=" . $ltpaToken . ";";

        $resultado = SpLib::curl_get($url, array(), array(
            CURLOPT_HEADER => 1,
            CURLOPT_FOLLOWLOCATION => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_ENCODING => ""         
        ), array(
            $cookieHeader,
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'User-Agent: Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
        ));

        $padrao_a = "/Veja aqui todos os servi/i";
        $padrao_b = "/Seu ip de acesso/i";
        $existe_padrao_a = preg_match($padrao_a, $resultado);
        $existe_padrao_b = preg_match($padrao_b, $resultado);
        if($existe_padrao_a && $existe_padrao_b) {
            return 'valido';
        }

        return 'invalido';
    }
}
