<?php

require_once "../vendor/autoload.php";
require_once "../library/SpLib.php";
require_once "./SpConfig.php";

use PHPHtmlParser\Dom;
use PHPHtmlParser\Dom\HtmlNode;


class SpLibCadastroUsuario {

    /* 
        Extrai o layout do form que deve ser enviado
        durante o pre cadastro de usuario.

        @param cookie - usar obterCookie para obter

        @return stdClass (
            'post_vars' => stdClass (
                '123123:nome' => '', ...
            ),
            'action' => ''
        )

        @triggers error
    */
    static function extrairFormPreCadastroUsuario ($cookie) {
        $headers = array(SpLib::makeCookieHeader($cookie));
        $resultado = new stdClass();

        $pagina = SpLib::curl_get(DETRAN_HOME, array(), array(
            CURLOPT_HEADER => 0,
            CURLOPT_FOLLOWLOCATION => 1            
        ), $headers);

        // Interpretar o dom        
        $dom = new Dom;
        if(!$dom->load($pagina))
            trigger_error("Dom parsing failed");

        // Identificar form
        $forms = $dom->find('div#areaLoginModal > form');
        if(!$forms || sizeof($forms) != 3) 
            trigger_error("Failed to find form in html.");
        $form = $forms[2];

        // Extrair dados do form
        $formLayout = SpLib::extrairFormatoDeForm($form);
        if(!$formLayout) {
            trigger_error("Failed to extract form.");
        }
        $resultado->action = $formLayout->action;
        $resultado->post_vars = $formLayout->post_vars;
 
        return $resultado;      
    }

    /* 
        Extrai o formato do formulario para cadastro de usuario.

        @param $cookie - Mesmo cookie usado desde o inicio do processo.
        @param $formPreCadastro stdClass (
            'post_vars' => stdClass (),
            'action' => ''
        )
        @param $cpf string

        @return mixed :
            'cpf_cadastrado' - Cpf ja usado em algum cadastro,
            'cpf_cadastrado_passado' - Cpf já foi utilizado em algum
            momento no passado. Usuário deve acessar o site.
            OU
            stdClass (
                'post_vars' => stdClass ('' => '', ...),
                'action' => '',
                'captcha_url' => ''
            )        
    */
    static function extrairFormCadastro($cookie, $formPreCadastro, $cpf) {
        $resultado = new stdClass();

        SpLib::preencherVariaveisPost($formPreCadastro->post_vars, (object) array(
            'urlRedirect' => '/wps/myportal/portaldetran/cidadao/servicos/meuDetran',
            'ipUsuario' => SpLib::randomIp(),
            'numeroCadastro' => $cpf
        ));
        
        // Enviar post e seguir redirecinamento
        $response = SpLib::curl_post($formPreCadastro->action, $formPreCadastro->post_vars, array(
            CURLOPT_FOLLOWLOCATION => 1            
        ), array(SpLib::makeCookieHeader($cookie)));

        if($response === false)
            trigger_error("Failed to post request.");

        // Interpretar o dom        
        $dom = new Dom;
        $dom_loaded = $dom->load($response);   
        if($dom_loaded === false)
            trigger_error("Failed to parse html");

        // Verificar se cpf ja esta sendo usado
        $padrao = "/cadastrado com CPF ou CNPJ/i";
        if(preg_match($padrao, $response)){
            return 'cpf_cadastrado';
        }

        // Verificar se cpf ja foi usado no passado
        $padrao = "/Para efetuar o seu recadastro/i";
        if(preg_match($padrao, $response)) {
            return 'cpf_cadastrado_passado';
        }

        // Obter o action do form
        $forms = $dom->find('form');
        if(!$forms || sizeof($forms) < 7) {
            echo $dom;
            trigger_error("Failed to find form in html.");
        }
        $form = $forms[6];

        $formData = SpLib::extrairFormatoDeForm($form);
        if(!$formData) {
            trigger_error("Failed to extract data from form.");
        }
        $resultado->action = $formData->action;
        $resultado->post_vars = $formData->post_vars;

        // Obter o url da imagem do captcha
        $captchas = $dom->find(".imgCaptha img");
        if(!$captchas || sizeof($captchas) < 1)
            trigger_error("Failed to find captcha in form.");
        $captcha = $captchas[0]->getAttribute('src');
        $captcha = "http://www.detran.sp.gov.br" . $captcha;
        $resultado->captcha_url = $captcha;
        
        return $resultado;
    }


    /* 
        Executa o cadastro do usuario.

        @param $cookie
        @param $dados array(
            'nome' => '',
            'dataNascimento' => '27/10/1990',
            'email' => ''
            'captcha' => '',
            'cpf' => '01718985169'
        )
        
        @param $formData stdClass (
            'post_vars' => stdClass('a' => 'value', ...),
            'action' => 'uri'
        )

        @return mixed
            'captcha'
            'email_usado'
            'ok'
            | FALSE
    */
    static function cadastrarUsuario($cookie, $dados, $formData) {
        $dados['nome'] = strtoupper($dados['nome']);

        $formData->post_vars = SpLib::preencherVariaveisPost($formData->post_vars, (object) array(
            'nome' => $dados['nome'],
            'dataNascimento' => $dados['dataNascimento'],
            'email' => $dados['email'],
            'confereEmail' => $dados['email'],
            'captcha' => $dados['captcha'],
            'cpf' => $dados['cpf'],
            'fraseSeguranca' => 'Frase de seguranca',
            'termoResponsabilidade' => 'true',
            'ipUsuario' => SpLib::randomIp()
        ));

        /* 
            Remover 'receberEmail' e 'receberSMS'
            Substituir ':btSair' por 'form:_idcl'
        */
        foreach ($formData->post_vars as $key => $value) {
            if (preg_match('/:receberEmail$/i', $key)) {
                unset($formData->post_vars->$key);
            }                                         
            if (preg_match('/:receberSMS$/i', $key)) {
                unset($formData->post_vars->$key);
            }           
            if (preg_match('/:btSair$/i', $key)) {
                $padrao = "/btSair$/";
                $nova_key = preg_replace($padrao, 'form:_idcl', $key);
                $novo_valor = preg_replace($padrao, 'btAvancarPessoaFisica', $key);
                $formData->post_vars->$nova_key = $novo_valor;
                unset($formData->post_vars->$key);
            }
        }

        $headers = array();
        $headers[] = SpLib::makeCookieHeader($cookie);
        $headers[] = "Referer: http://www.detran.sp.gov.br/wps/portal/portaldetran/cidadao/servicos/formularioCadastro";
        $headers[] = "Origin: http://www.detran.sp.gov.br";
        $headers[] = "Host: www.detran.sp.gov.br";
    
        $response = SpLib::curl_post($formData->action, $formData->post_vars, array(
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_HEADER => 0            
        ), $headers);

        // Verificar se captcha esta errado
        $padrao = "/lidos, Digite novamente.<\/span>/i";
        if(preg_match($padrao, $response)){
            return 'captcha';
        }

        // Verificar se email ja e utilizado
        $padrao = "/cadastrado na base, utilize outro/i";
        if(preg_match($padrao, $response)){
            return 'email_usado';
        }

        // Interpreta o DOM
        $dom = new Dom;
        $dom_loaded = $dom->load($response);   
        if($dom_loaded === false)
            trigger_error("Failed to parse html"); 

        // Extrair form layout
        $forms = $dom->find("form");
        if(!$forms || sizeof($forms) < 7) {
            trigger_error("Failed to find form in html.");
        }
        $form = $forms[6];        
        $confirmSignupFormData = SpLib::extrairFormatoDeForm($form);
        if(!$confirmSignupFormData) {
            trigger_error("Failed to extract data from form.");
        }

        /* 
            Adicionar linha
            {BASE}:form:_idcl => {BASE}:btAvancar
        */
        foreach ($confirmSignupFormData->post_vars as $key => $value) {
            if (preg_match('/:mobile$/i', $key)) {
                $padrao_clone = $key;
                $BASE = preg_replace('/:mobile$/i', '', $padrao_clone);
                $nova_chave = $BASE . ":form:_idcl";
                $confirmSignupFormData->post_vars->$nova_chave = $BASE . ":btAvancar";
            }                                         
        }        

        // Confirmacao final
        $signupConfirmResult = SpLib::curl_post($confirmSignupFormData->action, $confirmSignupFormData->post_vars, array(
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_HEADER => 0            
        ), [SpLib::makeCookieHeader($cookie)]);

        // Procurar por: 'senha de acesso no e-mail principal'
        $padrao = "/senha de acesso no e-mail principal/i";
        if(preg_match($padrao, $signupConfirmResult)){
            return 'ok';
        } else {
            return false;
        }

    }

}