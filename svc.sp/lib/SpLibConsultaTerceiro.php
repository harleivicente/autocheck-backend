<?php

require_once "../vendor/autoload.php";
require_once "../library/SpLib.php";
require_once "./SpConfig.php";

use PHPHtmlParser\Dom;
use PHPHtmlParser\Dom\HtmlNode;

class SpLibConsultaTerceiro {

    /* 
        Extrair os dados de uma tabela de uma consulta
        de terceiros.

        @param HtmlNode <tabela> ...

        @return array(
            campo => valor, ...
        )
    */
    static function extrairDadosTabela(HtmlNode $tabela) {
        $trs = $tabela->find("tr");
        $resultado = array (
        );
        
        if(sizeof($trs) > 0) {
            
            // Titulo
            // $resultado['titulo'] = $trs[0]->find("td strong")[0]->innerHtml();

            /*
                Conteudo
            */

            // linha
            for ($linhaIndex = 1; $linhaIndex < sizeof($trs); $linhaIndex++) { 
                $tds = $trs[$linhaIndex]->find("td");

                // coluna
                foreach ($tds as $colunaChave => $coluna) {
                    $campo = $coluna->find('strong')[0]->innerHtml();
                    $filhos = $coluna->getChildren();

                    $valor = "";
                    foreach ($filhos as $key => $filho) {
                        $valor .= $filho->innerHtml();
                    }
                    $valor = str_replace($campo, '', $valor);

                    $resultado[$campo] = $valor;
                }
            }
        }

        return $resultado;
    }


    /* 
        Obtem formato correto dos campos do form.

        @return stdClass (
            'post_vars' => stdClass('a' => 'value', ...),
            'action' => 'uri',
            'captcha_url' => ''
        )
    */
    static function consultarPlacaTerceiroPre ($cookie) {
        $response = SpLib::curl_get(DETRAN_CONSULTA_TERCEIROS, array(), array(
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_HEADER => 1              
        ), array(
            SpLib::makeCookieHeader($cookie)
        ), true);

        // Interpretar o dom        
        $dom = new Dom;
        $dom_loaded = $dom->load($response);   
        if($dom_loaded === false)
            trigger_error("Failed to parse html");

        $forms = $dom->find('form');
        if(!$forms) {
            trigger_error("Failed to find form in html.");
        }
        $form = $forms[6];
        $formato = SpLib::extrairFormatoDeForm($form);

        if(!$formato) {
            trigger_error("Failed to extract data from form.");
        }

        // Obter o url da imagem do captcha
        $captchas = $dom->find("form img");
        if(!$captchas || sizeof($captchas) < 2)
            trigger_error("Failed to find captcha in form.");
        $captcha = $captchas[0]->getAttribute('src');
        $captcha = "http://www.detran.sp.gov.br" . $captcha;
        $formato->captcha_url = $captcha;

        return $formato;
    }


    /* 
        Consulta um veiculo no site do detran sp.

        @param $cookie
        @param $formData stdClass (
            'action' =>,
            'post_vars' => stdClass()
        )
        @dados stdClass (
            placa => FGB-1117
            renavam => 305801848
            captcha => ad343d32
        )

        @return 
            'erro_captcha' ||
            'erro_sem_registro' ||
            json no formato: {
                "Último licenciamento efetuado:" : "exercício 2016"
                "IPVA:" : " R$ 3.655,51 - EM ATRASO -  Em caso de d&#250;vidas, consulte www.ipva.fazenda.sp.gov.br "
                "Inspe&#231;&#227;o GNV: ": " NADA CONSTA "
                "Inspe&#231;&#227;o veicular do ano: ": " CONSTA 2013* -  Exig&#234;ncia temporariamente suspensa "
                "Placa: ": "FGB1117"
                "Renavam: " : "305801848"
                "Restri&#231;&#227;o administrativa: " : "NADA CONSTA "
                "Restri&#231;&#227;o financeira: " : "BCO BRADESCO FINANC SA "
                "Restri&#231;&#227;o judici&#225;ria: " : "NADA CONSTA "
                "Restri&#231;&#227;o por bloqueio de furto/roubo: " : "NADA CONSTA "
                "Restri&#231;&#227;o por ve&#237;culo guinchado: " : "NADA CONSTA "
                "Restri&#231;&#227;o tribut&#225;ria: " : "NADA CONSTA "
                "Status do licenciamento: " : " em dia (licenciamento 2017 vencer&#225; em 30/09/2017)* "
                "Total: " : "R$ 3.114,38"
            }

            json no formato: {
                veiculo: {
                    'dado' : 'valor'
                },
                imposto: {
                    'ipva' : {
                        Último licenciamento efetuado: '',
                        IPVA: ''
                        Status do licenciamento: ''
                    }
                }

            }        
    */
    static function consultarPlacaTerceiro ($cookie, $formData, $dados) {
        $dados_extraidos = array(
            'veiculo' => array(),
            'imposto' => array(
                'ipva' => array()
            )
        );

        // Preencher campos
        $formData->post_vars = SpLib::preencherVariaveisPost($formData->post_vars, $dados);

        $resultado = SpLib::curl_post($formData->action, $formData->post_vars, array(
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_HEADER => 1             
        ), array(SpLib::makeCookieHeader($cookie)));

        // Interpretar o dom        
        $dom = new Dom;
        $dom_loaded = $dom->load($resultado);   
        if($dom_loaded === false)
            trigger_error("Failed to parse html");

        // Verifica se o registor de placa foi encontrado
        $padrao = "/NENHUM REGISTRO ENCONTRADO/i";
        $padrao2 = "/O CADASTRADA/i";
        $erro = preg_match($padrao, $resultado);
        $erro2 = preg_match($padrao2, $resultado);
        if($erro || $erro2) {
            return 'erro_sem_registro';
        }

        // Verifica se o captcha esta correto
        $padrao = "/Caracteres inv/i";
        $erro = preg_match($padrao, $resultado);
        if($erro) {
            return 'erro_captcha';
        }

        /* 
            Extrair todas as tabelas em '.contentInternoSemMenu'

            Tabela 0 - Titulo da página
            Tabela 1 - Vazio
        */
        $dadosExtraidos = array();
        $tables = $dom->find(".contentInternoSemMenu table");
        foreach ($tables as $key => $tabela) {
            if($key > 1) {
                $dadosExtraidos += self::extrairDadosTabela($tables[$key]);
            }
        }
        $dados_extraidos['veiculo'] = $dadosExtraidos;
        $dados_extraidos['veiculo'] = self::removerEmArray("/licenciamento/i", $dados_extraidos['veiculo']);
        $dados_extraidos['veiculo'] = self::removerEmArray("/IPVA/i", $dados_extraidos['veiculo']);

        // imposto
        $ipva = self::encontrarEmArray("/IPVA/i", $dadosExtraidos);
        $licenciamento_efetuado = self::encontrarEmArray("/licenciamento efetuado/", $dadosExtraidos);
        $licenciamento_status = self::encontrarEmArray("/do licenciamento/", $dadosExtraidos);
        $dados_extraidos['imposto']['ipva'] = array(
            'IPVA' => $ipva,
            'Último licenciamento efetuado' => $licenciamento_efetuado,
            'Status do licenciamento' => $licenciamento_status
        );

        return $dados_extraidos;
    }

    /*
        Busca um item em um array associativo.

        @return false | valor
    */
    static function encontrarEmArray($regex, $array) {
        foreach ($array as $key => $item) {
            if(preg_match($regex, $key)) {
                return $item;
            }
        }
        return false;
    }

    /* 
        Remove item de array associativo.

    */
    static function removerEmArray($regex, $array) {
        foreach ($array as $key => $item) {
            if(preg_match($regex, $key)) {
                unset($array[$key]);
            }
        }
        return $array;
    }


}