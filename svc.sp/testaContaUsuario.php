<?php

require_once "../library/SpLib.php";
require_once "./lib/SpLibTestarUsuario.php";

/* 
    @param $_POST['cpf'] - 01718985169
    @param $_POST['senha']

    @return json of {
        erro: 'sim' | 'nao',
        id_erro: numero 
            0 - sem erro
            2 - alterar_senha
            1 - invalido - CPF E OU SENHA INCORRETOS
    }

*/

set_error_handler(function($code, $msg) {
    echo json_encode(array ('erro' => 'sim', 'id_erro' => '3','msg' => $msg));
    exit;
});

$params = array("cpf", "senha");
foreach ($params as $key => $param) {
    if(!array_key_exists($param, $_POST)){
        echo json_encode(array ('erro' => 'sim', 'id_erro' => '3'));
        exit;
    }
}

$cookie = SpLib::obterCookie();
$formData = SpLibTestarUsuario::extrairFormDataLogin($cookie);
$ltpa = SpLibTestarUsuario::obterLtpaToken($cookie, $formData, $_POST['cpf'], $_POST['senha']);

if($ltpa->status == "primeiro_acesso") {
    echo json_encode(array ('erro' => 'sim', 'id_erro' => '2', 'msg' => 'Usuario deve trocar a senha.'));    
    exit;
} elseif ($ltpa->status == "erro") {
    echo json_encode(array ('erro' => 'sim', 'id_erro' => '1'));
    exit;
}

$resultado = SpLibTestarUsuario::verificarUsuario($cookie, $ltpa->token);

if($resultado == "valido") {
    echo json_encode(array ('erro' => 'nao', 'id_erro' => '0'));
    exit;
} elseif ($resultado == "invalido") {
    echo json_encode(array ('erro' => 'sim', 'id_erro' => '1'));
    exit;
}

