<?php

require_once "./lib/SpLibCadastroUsuario.php";
require_once "../library/SpLib.php";

/* 
    @param $_POST['cpf'] - 01718985169

    @return json of {
        erro: 'sim' | 'nao',
        id_erro: numero (1 - parametro incorretos, 2 - cpf ja usado, 3 - cpf ja usado no passado),
        token: {
            action: '',
            post_vars: '',
            captcha_url: '',
            cookie: ''
        }
    }

*/

set_error_handler(function($code, $msg) {
    echo json_encode(array ('erro' => 'sim', 'id_erro' => '5','msg' => $msg));
    exit;
});

// cpf
if(array_key_exists('cpf', $_POST)) {
    $cpf = $_POST['cpf'];
} else {
    $cpf = null;
}
if(!$cpf) {
    echo json_encode(array ('erro' => 'sim', 'id_erro' => '1', 'msg' => 'Parametros incorretos.'));
    exit;    
}

$cookie = SpLib::obterCookie();
$preCadastroForm = SpLibCadastroUsuario::extrairFormPreCadastroUsuario($cookie);
$cadastroToken = SpLibCadastroUsuario::extrairFormCadastro($cookie, $preCadastroForm, $cpf);

if($cadastroToken == "cpf_cadastrado") {
    echo json_encode(array ('erro' => 'sim', 'id_erro' => '2', 'msg' => 'Cpf ja usado.'));
} elseif ($cadastroToken == "cpf_cadastrado_passado") {
    echo json_encode(array ('erro' => 'sim', 'id_erro' => '3', 'msg' => 'Cpf ja usado no passado. Usuario deve recadastrar no site.'));
} else {
    $novo_captcha_url = SpLib::baixarCaptcha($cookie, $cadastroToken->captcha_url);
    $cadastroToken->captcha_url = $novo_captcha_url;
    $cadastroToken->cookie = $cookie;
    echo json_encode(array ('erro' => 'nao', 'id_erro' => '0', 'token' => $cadastroToken));
}
