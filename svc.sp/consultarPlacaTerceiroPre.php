<?php

require_once "./lib/SpLibConsultaTerceiro.php";
require_once "../library/SpLib.php";

/* 
    Obtem um token com informações necessárias para consulta
    de placa de terceiros SpLib::consultarPlacaTerceiro.

    @return json {
        erro: nao | sim,
        id_erro: '0',
        token: {
            action: '',
            post_vars: '',
            captcha_url: '',
            cookie: ''
        }
    }
*/

set_error_handler(function($code, $msg) {
    echo json_encode(array ('erro' => 'sim', 'id_erro' => '1','msg' => $msg));
    exit;
});

$cookie = SpLib::obterCookie();
$resultado = SpLibConsultaTerceiro::consultarPlacaTerceiroPre($cookie);
$novo_captcha_url = SpLib::baixarCaptcha($cookie, $resultado->captcha_url);
$resultado->captcha_url = $novo_captcha_url;
$resultado->cookie = $cookie;

echo json_encode(array(
    'erro' => 'nao',
    'id_erro' => '0',
    'token' => $resultado
));
