<?php

require_once "./lib/SpLibConsultaTerceiro.php";
require_once "../library/SpLib.php";

/* 
    Consulta placa de terceiro.

    @param $_POST['token'] json - dado obtido com chamada a consultarPlacaTerceiroPre.php. 
    @param $_POST['placa'] string - FGB-1117
    @param $_POST['renavam'] string - 305801848
    @param $_POST['captcha'] string - ykiinn

    Formato do token:
        token: {
            action: '',
            post_vars: '',
            captcha_url: '',
            cookie: ''
        }

    @return json no formato: {
        erro: nao | sim,
        id_erro: numero, (0 - sem erro, 1 - parametros incompletos, 2 - registor nao encontrado, 3 - captcha errado)
        ...
    }
*/

// token
if(array_key_exists('token', $_POST)) {
    $token = (object) $_POST['token'];
    $token->post_vars = (object) $token->post_vars;
} else {
    $token = null;
}

// placa
if(array_key_exists('placa', $_POST))
    $placa = $_POST['placa'];
else 
    $placa = null;

//renavam
if(array_key_exists('renavam', $_POST))
    $renavam = $_POST['renavam'];
else 
    $renavam = null;

//captcha
if(array_key_exists('captcha', $_POST))
    $captcha = $_POST['captcha'];
else 
    $captcha = null;


if(!$token || !$placa || !$renavam || !$captcha) {
    echo json_encode(array ('erro' => 'sim', 'id_erro' => '1', 'msg' => 'Parametros incorretos.'));
    exit;
}

set_error_handler(function($code, $msg) {
    echo json_encode(array ('erro' => 'sim', 'id_erro' => '4', 'msg' => $msg));
    exit;
});

if($token == NULL) {
    trigger_error("Unable to decode token.");
}

$dados = SpLibConsultaTerceiro::consultarPlacaTerceiro($token->cookie, $token, (object) array(
    'placa' => $placa,
    'renavam' => $renavam,
    'captcha' => $captcha
));

if($dados == "erro_captcha") {
    echo json_encode(array ('erro' => 'sim', 'id_erro' => '3', 'msg' => 'Captcha errado.'));
} elseif ($dados == "erro_sem_registro") {
    echo json_encode(array ('erro' => 'sim', 'id_erro' => '2', 'msg' => 'Registro não encontrado.'));
} else {
    echo json_encode(array ('erro' => 'nao', 'id_erro' => '0') + $dados);
}
