<?php
require_once "../library/SpLib.php";
require_once "../vendor/autoload.php";
require_once '../config.php';
require_once '../inc.php';
require_once "../db.php";
require_once "../query.php";
require_once "../library/Captcha.php";
require_once "./veiculo_rs_lib.php";

use PHPHtmlParser\Dom;
use PHPHtmlParser\Dom\HtmlNode;

session_start();

/* 
    @param placa
    @param renavam
*/
$params = array("renavan", "placa");
foreach ($params as $key => $param) {
    if(!array_key_exists($param, $_POST)){
        echo json_encode(array ('erro' => 'Erro interno do sistema. Tente novamente mais tarde.', 'id_erro' => '1', 'msg' => "Parametros incorretos."));
        exit;
    }
}

if(array_key_exists('email', $_POST)){
    $email = trim($_POST['email']);
}
if(array_key_exists('debug', $_POST)){
    $debug = $_POST['debug'];
} else {
    $debug = false;
}

if(!$debug) {
    $url = "https://www.portaldetransito.rs.gov.br/dtw2/servico/vei2/veiculo-form.xhtml";
    
    $home = SpLib::curl_get($url, array(), array(
        CURLOPT_HEADER => 1,
        CURLOPT_FOLLOWLOCATION => 0,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0  
    ), array());
    
    if(!$home) {
        echo json_encode(array('erro' => 'Detran indisponvível...', 'id_erro' => 4));
        exit;
    }
    
    // Obter os cookies JSESSIONID
    $session_regex = "/JSESSIONID=([^;]+);/i";
    preg_match($session_regex, $home, $session);
    if(!$session[1]) {
        echo json_encode(array('erro' => 'Detran indisponvível...', 'id_erro' => 4));
        exit;
    }
    $session = $session[1];
    
    // Obter view state do form
    $dom = new Dom;
    if(!$dom->load($home)){
        echo json_encode(array ('erro' => 'Erro interno. Tente novamente mais tarde.', 'id_erro' => '3', 'msg' => 'Failed to parse DOM.'));
        exit;    
    }
    $input = $dom->find("input[name='javax.faces.ViewState']");
    if(sizeof($input) < 1) {
        echo json_encode(array ('erro' => 'Erro interno. Tente novamente mais tarde.', 'id_erro' => '3', 'msg' => 'Cant find viewstate from form.'));
        exit;        
    }
    $viewstate = $input[0]->__get('value');
    
    // Obter id do butao consultar
    $buttons = $dom->find('button');
    if(sizeof($buttons) < 1){
        echo json_encode(array ('erro' => 'Erro interno. Tente novamente mais tarde.', 'id_erro' => '3', 'msg' => 'Failed to find button consultar.'));
        exit;    
    }
    $id = $buttons[0]->__get('id');
    
    // Sessions, Viewstate, Id do butao consultar
    $session;
    $viewstate;
    $id;
    
    // Quebrar captcha
    $url = "https://www.portaldetransito.rs.gov.br/dtw2/servico/vei2/veiculo-form.xhtml";
    $captcha = captcha_service ($url, '6LfmYBsTAAAAABE3Nc1rnp9wD97bTQZVM3S48hNx');
    
    if($captcha === false) {
        echo json_encode(array ('erro' => 'Não possível resolver o Captcha. Tente novamente.', 'id_erro' => '456'));
        exit;
    }
    
    // Fazer consulta
    $url = "https://www.portaldetransito.rs.gov.br/dtw2/servico/vei2/veiculo-form.xhtml";
    $post = array(
        'form' => 'form',
        'form:placa' => $_POST['placa'],
        'form:renavam' => $_POST['renavan'],
        'g-recaptcha-response' => $captcha,
        $id => '',
        'javax.faces.ViewState' => $viewstate
    );
    $consulta = SpLib::curl_post($url, $post, array(
        CURLOPT_HEADER => 0,
        CURLOPT_FOLLOWLOCATION => 1,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0      
    ), array(
        'Cookie: JSESSIONID=' . $session . ';',
    ));
    
    // Check if request was successful
    $regex = '/<span class="label-form">Placa: <\/span><span style="font-weight: normal; margin-right: 20px">'.$_POST['placa'].' <\/span>/i';
    if(!preg_match($regex, $consulta)) {
        echo json_encode(array ('erro' => 'Placa e/ou Renavam incorretos.', 'id_erro' => '3', 'msg' => 'Consulta n teve sucesso.'));
        exit;
    }
    
} else {
    // Read from text file
    $consulta = file_get_contents("./result_html_test.html");
}
    
    /* FORMATO DOS DADOS */
    $dados_veiculo =  array(); // saida do json
    $dados_veiculo['erro'] = 'nao';
    $dados_veiculo['id_erro'] = '0';
    $dados_veiculo['placa'] = '';
    $dados_veiculo['chassi'] = null;
    $dados_veiculo['renavam'] = $_POST['renavan'];
    $dados_veiculo['veiculo'] = array(
        // 'Placa' => 'p',
    );
    $dados_veiculo['imposto'] = array(
        // '2017' => array(
        //     'a' => 'b'
        // )
    );
    $dados_veiculo['infracao']['multas'] = array(
    // 'cidade' => array (
        //     array('key' => 'value', 'Valor' => 'R$443')
        // )
    );
    // $dados_veiculo['infracao']['autuacoes'] = array(
    // 'cidade' => array(
    //         array('chave' => 'valor')
    //     )
    // );


// Dados do veiculo
$consulta_dom = new Dom;
if(!$consulta_dom->load($consulta)) {
    trigger_error("Could not load dom of request response.");
}
$dados_veiculo['veiculo'] = extract_car_details($consulta_dom);
$dados_veiculo['imposto'] = extract_tax_info($consulta_dom);
$dados_veiculo['infracao']['multas'] = extrair_multas($consulta_dom);

$json = json_encode($dados_veiculo);
echo $json;

if(!empty($email) && $placa && $chassi) {
    $db = new db();
    $db->query("call add_log_veiculo ('RS', '".$email."', '".$placa."', '".$chassi."', '".$_POST['renavan']."', '', '".$json."');");
}

