<?php

// ---------------------------------------------------------------------------------------------------------------------
//  VEICULO RS
//  http://www.detran.rs.gov.br/index.php?frame=detran-veiculo-consulta_tela
//----------------------------------------------------------------------------------------------------------------------

include '../config.php';
include '../inc.php';
include "../db.php";
include "../query.php";

session_start();

header('content-type: application/json; charset=utf-8');

$placa = strtoupper(trim($_POST['placa']));
$renavam = strtoupper(trim($_POST['renavan']));
$captcha = strtoupper(trim($_POST['captcha']));
$email = trim($_POST['email']);

if (!$placa or !$renavam or !$captcha) {
    $json = array("erro" => "Um ou mais parametros invalidos", 'id_erro' => '1');
    echo jsonp_encode($json);
    exit;
}

$url = "http://webgen.procergs.com.br/cgi-bin/webgen2.cgi?TR=detran-veiculo-pesquisar&A7-placa=&N11-renavam=";

$ch = curl_init();

$data = "TR=detran-veiculo-consultav2".
    "&IDTR01=".$_SESSION['cod_captcha'].
    "&A15-TRANS-ID=NET-VEI-CON".
    "&A01-FILLER=".
    "&N01-MODO=1".
    "&N08-DATA=".
    "&N06-HORA=".
    "&N01-TIPO-HEADER=6".
    "&A08-FILLER=".
    "&N02-VERSAO=9".
    "&A03-FUNCAO=".
    "&A11-FILLER=".
    "&A08-CLIENTE=".
    "&N10-OPERADOR=".
    "&N12-TICKET=".
    "&A03-CARIMBO=%FA%FB%FD".
    "&A01-FILLER=".
    "&A7-nroplaca=".$placa.
    "&N11-renavam=".$renavam.
    "&IDTR01PARM=".$captcha;

curl_setopt($ch, CURLOPT_URL, $url);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);

curl_setproxy($ch, 'rs');

curl_setopt($ch, CURLOPT_REFERER, "http://webgen.procergs.com.br/cgi-bin/webgen2.cgi?TR=detran-veiculo-pesquisar&A7-placa=&N11-renavam=");
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_NOBODY, 0);

$pg = curl_exec($ch);

//echo $pg;
//exit;

// ---------------------------------------------------------------------------------------------------------------------
// TRATAMENTO DE ERROS
// ---------------------------------------------------------------------------------------------------------------------

$tem_erro =  preg_match("/" .
    "<div class=\"cCaixaUiTitulo ui-widget-header ui-corner-top\">Consulta<\\/div>\r\n".
    "<div class=\"cCaixaUiConteudo ui-widget-content ui-corner-bottom\">\r\n".
    "<p>(.*?)<\\/p>\r\n".
    "<\\/div>\r\n".
    "<\\/div>\r\n".
    "<\\/body>\r\n".
    "/sim",
    $pg, $erro);

if ($tem_erro)
{
    $erro = preg_replace("/\n|<br>/", '', $erro[1]);
    $json = array("erro" => utf8_encode(trim($erro)), 'id_erro' => '3');
    echo jsonp_encode($json);
    exit;
}

if (!preg_match('/Placa em n�meros/sim', $pg)) // estourou o timeout ou erro no servidor do detran
{
    echo jsonp_encode(array(
        "erro" => utf8_encode("Servidor do Detran temporariamente indispon�vel... :("),
        'id_erro' => 2
    ));

    exit;
}

//exit;

// ---------------------------------------------------------------------------------------------------------------------
// INICIALIZANDO VARIAVEIS DE SA�DA
// ---------------------------------------------------------------------------------------------------------------------

$dados_veiculo = array(); // saida do json

$dados_veiculo['erro'] = 'nao';
$dados_veiculo['id_erro'] = '0';

$dados_veiculo['placa'] = $placa;
$dados_veiculo['chassi'] = null;
$dados_veiculo['renavam'] = $renavam;

$dados_veiculo['veiculo'] = array();
$dados_veiculo['imposto'] = array();

$dados_veiculo['infracao']['multas'] = array();
$dados_veiculo['infracao']['autuacoes'] = array();


// ---------------------------------------------------------------------------------------------------------------------
// PARSE DOS DADOS DO VE�CULO
// ---------------------------------------------------------------------------------------------------------------------

// pegando numero de chassi
preg_match("/Chassi: <\\/span>(.*?)<br>/sim", $pg, $chassi);
$chassi = trim($chassi[1]);

$dados_veiculo['chassi'] = $chassi;
$dados_veiculo['veiculo']['Chassi'] = $chassi;

// detalhes do veiculo
preg_match_all('/<table[^>]*>(.*?)<\/table>/si', $pg, $tables);
$tables = $tables[1];

preg_match_all('/<td[^>]*>(.*?)<\/td>/si', $tables[0], $campos);
$campos = $campos[1];

for($i = 0 ; $i < 13 ; $i += 2)
{
    $label = html_entity_decode(trim(preg_replace("/\n|\r|\t|:/sim", '', $campos[$i])));

    $campo = trim(preg_replace("/\n|\r|\t|:/sim", '', $campos[$i+1]));

    if($label == 'Fabrica��o/Modelo')
    {
        $fabric_model = explode('/', $campo);
        $dados_veiculo['veiculo'][utf8_encode('Fabrica��o')] = $fabric_model[0];
        $dados_veiculo['veiculo'][utf8_encode('Modelo')] = $fabric_model[1];
    }
    else
    {
        $dados_veiculo['veiculo'][utf8_encode($label)] = $campo;
    }
}

// pegando informacao de alienacao
preg_match("/Ve�culo Alienado:(.*?)<br>/sim", $pg, $alienacao_fiduciaria);
$alienacao_fiduciaria = trim($alienacao_fiduciaria[1]);

if($alienacao_fiduciaria)
{
    $dados_veiculo['veiculo'][utf8_encode('Aliena��o')] = $alienacao_fiduciaria;
}

//print_r($dados_veiculo);
//exit;

// Atualmente a ficha de ve�culo possui pelo menos 13 colunas
// Se o script encontrar um n�mero menor de colunas, alguma coisa deu errado
if(sizeof($campos) < 13)
{
    echo jsonp_encode(array(
        "erro" => utf8_encode("Servidor do Detran temporariamente indispon�vel."),
        'id_erro' => 4
    ));

    exit;
}

// ---------------------------------------------------------------------------------------------------------------------
// PARSE DOS IMPOSTOS DO VE�CULO
// ---------------------------------------------------------------------------------------------------------------------

//print_r($tables);
//exit;

$ind_tb = 2;
if(preg_match('/<td class="obs" /sim', $pg)) {
    $ind_tb++;
}

// IPVA
preg_match_all('/<td width="15%">(.*?)<\/td>/si', $tables[$ind_tb], $anos_ipva);
$anos_ipva = $anos_ipva[1];

preg_match_all('/<td width="12%" align="right">(.*?)<\/td>/si', $tables[$ind_tb+1], $valores_ipva);
$valores_ipva = $valores_ipva[1];

//print_r($anos_ipva);
//echo "\n\n";
//print_r($valores_ipva);
//exit;

$anos_ipva[0] = preg_replace("/\n| /", '', $anos_ipva[0]);
$valores_ipva[0] = preg_replace("/\n| /", '', $valores_ipva[0]);

$dados_veiculo['imposto']['IPVA'][$anos_ipva[0]] = intval($valores_ipva[0]) ? 'R$ ' . $valores_ipva[0] : 'Liquidado';

//print_r($dados_veiculo);
//exit;

// SEGURO OBRIGATORIO
preg_match_all('/<tr>(.*?)<\/tr>/sim', $tables[$ind_tb+4], $linhas);
$linhas = $linhas[1];

array_shift($linhas); // tirando o cabecalho da tabela de seguro

foreach($linhas as $linha)
{
    preg_match_all('/<td[^>]*>(.*?)<\/td>/si', $linha, $seguro);
    $seguro = $seguro[1];

    $seguro[1] = preg_replace("/\n| /", '', $seguro[1]);
    $seguro[4] = preg_replace("/\n| /", '', $seguro[4]);

    $dados_veiculo['imposto']['SEGURO OBRIGATORIO'][$seguro[1]] = intval($seguro[4]) ? 'R$ ' . $seguro[4] : 'Liquidado';
}

//print_r($dados_veiculo);
//exit;

// LICENCIAMENTO
preg_match_all('/<tr>(.*?)<\/tr>/sim', $tables[$ind_tb+3], $linhas);
$linhas = $linhas[1];

array_shift($linhas); // tirando o cabecalho da tabela de licenciamento

foreach($linhas as $linha)
{
    preg_match_all('/<td[^>]*>(.*?)<\/td>/si', $linha, $licenciamento);
    $licenciamento = $licenciamento[1];

    $licenciamento[1] = preg_replace("/\n| /", '', $licenciamento[1]);
    $licenciamento[4] = preg_replace("/\n| /", '', $licenciamento[4]);

    $dados_veiculo['imposto']['LICENCIAMENTO'][$licenciamento[1]] = intval($licenciamento[4]) ? 'R$ ' . $licenciamento[4] : 'Liquidado';
}

//print_r($dados_veiculo);
//exit;

preg_match('/<table[^>]*id="tableInfracoes" name="tableInfracoes">(.*?)<\/table>/si', $pg, $tables);
$tables = $tables[1];

$linhas = preg_split('/<td class="lblcampo" colspan="4">/sim', $tables);

array_shift($linhas); // tirar o cabe�alho da tabela
array_pop($linhas); // retira a �ltima linha (sempre vazia) da tabela

//print_r($linhas);
//exit;

foreach($linhas as $linha)
{
    preg_match_all('/<td class="campo">(.*?)<\/td>/sim', $linha, $colunas);
    $colunas = $colunas[1];

    // remover caracteres especiais
    foreach($colunas as $n=>$coluna)
    {
        $colunas[$n] = html_entity_decode(trim(preg_replace("/\n|\r|\t/sim", '', $coluna)));
    }

//    print_r($colunas);
//    echo "\n\n";
//Array
//(
//    [0] => 31/07/2013 11:10 - data/hora
//    [1] => PORTO ALEGRE-RS - orgao autuador
//    [2] => E010505688 - s�rie
//    [3] => 74630 - infracao
//    [4] => EXC.VELOC.DE 20 A 50% MAX - descricao
//    [5] =>  127,69 - valor
//    [6] => AV. EDVALDO PEREIRA PAIVA, KM - local
//    [7] => Vencida - situacao
//)

    $data_hora = explode(' ', $colunas[0]);

    $multa = array(
        utf8_encode('Infra��o') => $colunas[4],
        'Local' => $colunas[6],
        'Data' => $data_hora[0],
        'Hora' => $data_hora[1],
        utf8_encode('S�rie') => $colunas[2],
        utf8_encode('C�digo') => $colunas[3],
        utf8_encode('Situa��o') => $colunas[7],
        'Valor' => 'R$ ' . $colunas[5]
    );

    $dados_veiculo['infracao']['multas'][$colunas[1]][] = $multa;

}


// ---------------------------------------------------------------------------------------------------------------------
// GERAR A RESPOSTA
// ---------------------------------------------------------------------------------------------------------------------

//print_r($dados_veiculo);
//exit;

$json = jsonp_encode($dados_veiculo);

if(!empty($email))
{
    $db = new db();
    $db->query("call add_log_veiculo ('RS', '".$email."', '".$placa."', '".$dados_veiculo['chassi']."', '".$dados_veiculo['renavam']."', '', '".$json."');");
}

echo $json;




