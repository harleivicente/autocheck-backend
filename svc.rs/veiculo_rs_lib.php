<?php
require_once "../library/SpLib.php";
require_once "../vendor/autoload.php";
require_once '../config.php';
require_once '../inc.php';
require_once "../db.php";
require_once "../query.php";
require_once "../library/Captcha.php";
require_once "./veiculo_rs_lib.php";

use PHPHtmlParser\Dom;
use PHPHtmlParser\Dom\HtmlNode;


/* 
    @param $dom
    @return array(
        'key' => '',
        ...
    )
*/
function extract_car_details($dom) {
    $output = array();

    // cabecalho
    $header = $dom->find("[data-widget='widget_form_panel_info_veiculo']");
    if(sizeof($header) < 1) {
        trigger_error("could not find header");
    }
    $header_items = $header[0]->find(".ui-panel-title span");
    $nof_pairs = sizeof($header_items)/2;
    for ($pair = 0; $pair < $nof_pairs; $pair++) { 
        $base = $pair * 2;
        $key_name = $header_items[$base]->innerHtml();
        $key_value = $header_items[$base+1]->innerHtml();
        $output[$key_name] = $key_value;
    }

    // corpo
    $corpo = $dom->find("[data-widget='widget_form_panel_info_veiculo'] > div");
    if(sizeof($corpo) < 2) {
        trigger_error("could not find body.");
    }
    $corpo = $corpo[1];
    $items = $corpo->find("td");
    foreach($items as $item) {
        $key = $item->find("span")[0]->innerHtml();
        $value = $item->text();
        $output[$key] = $value;
    }

    return $output;
}

/* 
    @return array(
        'IPVA (Exercicio 2017)' => array(
            'key' => 'value'
        ),
        'Seguro DPVAT (Exercicio 2017) => array(
            'key' => 'value'
        )
        ...
    )
*/
function extract_tax_info($dom) {
    $output = array();

    // Encontrar o exercicio mais recente
    $exercicios = $dom->find(".ui-panel-content .ui-datagrid tr");
    if(sizeof($exercicios) >= 1) {
        $recente = $exercicios[0];
        $title = $recente->find('.ui-panel-title');
        if(sizeof($title) < 1){
            trigger_error('cant find title.');
        }
        $title = $title[0]->text();

        // IPVA e DPVAT
        $output['IPVA (' . $title . ')'] = extract_tax_tab_info($recente, "/ipva/i");
        $output['DPVAT (' . $title . ')'] = extract_tax_tab_info($recente, "/seguro/i", 1);
        
        // Licenciamento
        $output['Licenciamento'] = extract_licenciamento($dom);
    }
    
    return $output;
}

/* 
    @return array(
        'key' => 'value', ...
    )
*/
function extract_licenciamento($dom) {
    $output = array();
    $licenciamento = $dom->find("[data-widget=widget_form_panel_crlv]");
    if(sizeof($licenciamento) > 0) {
        // Extract items
        $data_items = $licenciamento[0]->find("table td")->toArray();
        array_shift($data_items);
        for($key=0; $key < sizeof($data_items); $key++) {
            $data_item = $data_items[$key];

            /* 
                If current td has span and text it
                is self contained. Otherwise the current
                and next td make up one item.
            */
            $text = trim($data_item->text());
            
            if($text) {
                $key_name = $data_item->find("span")[0]->innerHtml();
                $value = $data_item->text();
                $output[$key_name] = $value;
            } else {
                $spans = $data_item->find("span");
                if(sizeof($spans)>=1 && $data_items[$key+1]) {
                    $key_name = $spans[0]->innerHtml();
                    $value = $data_items[$key+1]->text();
                    $output[$key_name] = $value;
                    $key++;
                }
            }
        }        
    }
    return $output;
}

/* 
    @param $dom
    @return array(
        'key' => 'value',
        ...
    )
*/
function extract_tax_tab_info($dom_exercico_recente, $tab_name_regex, $ignore_count = 0) {
    $output = array();
    $dom = $dom_exercico_recente;
    $tablist = $dom->find("[role=tablist]");
    $panes = $dom->find(".ui-tabs-panels > div");
    if(sizeof($tablist) < 1 || sizeof($panes) < 1){
        trigger_error("could not find  tax table.");
    }

    // Search for tab whose text match the given regex
    $tab_items = $tablist->find("[role=tab] a");
    $found_index = -1;
    foreach($tab_items as $tab_key => $tab_item) {
        $text = $tab_item->innerHtml();
        if(preg_match($tab_name_regex, $text)) {
            $found_index = $tab_key;
            break;
        }
    }
    if($found_index < 0) {
        trigger_error("could not find tab.");
    }
    
    // Found pane
    if(sizeof($panes)-1 < $found_index) {
        trigger_error("could not find tab.");
    }
    $pane = $panes[$found_index];

    // Extract items
    $data_items = $pane->find("table td")->toArray();
    for ($i=1; $i <= $ignore_count; $i++) { 
        array_pop($data_items);
    }
    for($key=0; $key < sizeof($data_items); $key++) {
        $data_item = $data_items[$key];

        /* 
            If current td has span and text it
            is self contained. Otherwise the current
            and next td make up one item.
        */
        $text = trim($data_item->text());
        
        if($text) {
            $key_name = $data_item->find("span")[0]->innerHtml();
            $value = $data_item->text();
            $output[$key_name] = $value;
        } else {
            $spans = $data_item->find("span");
            if(sizeof($spans)>=1 && $data_items[$key+1]) {
                $key_name = $spans[0]->innerHtml();
                $value = $data_items[$key+1]->text();
                $output[$key_name] = $value;
                $key++;
            }
        }
    }

    return $output;
}


/* 
    'cidade' => array (
        array('key' => 'value', 'Valor' => 'R$443')
    )
*/
function extrair_multas  ($dom) {
    $output_todas = array();
    $output = array();
    $gaveta = obter_gaveta_multa($dom);
    if($gaveta) {
        $abas = extrair_nome_abas($gaveta);
        
        $containers = $gaveta->find(".ui-tabs-panels div");

        foreach ($containers as $index => $container) {
            $multas = $container->find("tr.ui-datagrid-row table");
            foreach($multas as $m_index => $multa) {
                $output_todas[] = extrair_multa_individual($multa, $abas[$index]);
            }
        }

        // Organizar por cidade
        foreach($output_todas as $key => $multa) {
            $cidade = get_multa_key("/Autuador/i", $multa);
            if(!$output[$cidade]) {
                $output[$cidade] = array();
            }
            $output[$cidade][] = $multa;
        }
    }
    return $output;
}

/* 
    Obtem gaveta de multas.
*/
function obter_gaveta_multa ($dom) {
    $r = $dom->find("[data-widget=widget_form_panel_infracoes_multas]");
    if(sizeof($r) > 0) {
        return $r[0];
    } else {
        return false;
    }
}

function get_multa_key($regex, $multa) {
    $output = "";
    foreach($multa as $key => $value) {
        if(preg_match($regex, $key)){
            return $value;
        }
    }
    return $output;
}

/* 
    Obtem nomes das abas na seccao de multas
*/
function extrair_nome_abas ($gaveta) {
    $output = array();
    $abas = $gaveta->find("[role=tablist] li");
    if(sizeof($abas)>0){
        foreach ($abas as $key => $aba) {
            $nome = $aba->find("a")[0]->innerHtml();
            $regex = "/\([0-9]+\)/i";
            $nome = preg_replace($regex, "", $nome);
            $output[] = $nome;
        }
    }
    return $output;
}

/* 
    recebe um td que deve possuir texto e um span.
    @param tipo Paga, Vencida ...

    @return array(
        'key' => 'value'
    )
*/
function extrair_multa_individual ($multa, $tipo) {
    $output = array();
    $items = $multa->find("td");
    foreach ($items as $item) {
        $text = trim($item->text());
        $spans = $item->find("span");
        if($text && sizeof($spans) > 0) {
            $key_name = $spans[0]->innerHtml();
            if(preg_match("/valor/i", $key_name) && $tipo) {
                $key_name = "Valor";
                $text = $text . " (" . trim($tipo) . ")";
            }
            $output[$key_name] = $text;
        }
    }
    return $output;
}