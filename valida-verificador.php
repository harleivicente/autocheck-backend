<?php

include_once 'config.php';
include_once "db.php";
include_once "query.php";
include_once "inc.php";

header('content-type: application/json; charset=utf-8');

$cod_verificacao    = addslashes(trim($_POST['cod_verificacao']));
$email              = addslashes(trim($_POST['email']));

//$cod_verificacao = '3619';
//$email = 'trololo@localhost';

if(!$cod_verificacao or !$email)
{
    $json = array("erro" => "Um ou mais parametros invalidos", 'id_erro' => '1');
    echo json_encode($json);
    exit;
}

$db = new db();

// Inicializando variaveis
$json =  array(); // saida do json
$json['erro'] = 'nao';
$json['id_erro'] = '0';

$sql = "select idusuario from usuario where email = '".$email."' and cod_verificacao = '".$cod_verificacao."'";

$rs = new query($db, $sql);

if($rs->erro())
    exit;

$rs = $rs->fetch();

if(!$rs['idusuario'])
{
    $json['erro'] = utf8_encode('C�digo de verifica��o inv�lido!');
    $json['id_erro'] = '1';

    echo json_encode($json);
    exit;
}

echo json_encode($json);
