<?php

include_once "config.php";
error_reporting(1);

function formatarCPF_CNPJ($campo, $formatado = true){
	//retira formato
	$codigoLimpo = preg_replace("[' '-./ t]",'',$campo);
	// pega o tamanho da string menos os digitos verificadores
	$tamanho = (strlen($codigoLimpo) -2);
	//verifica se o tamanho do código informado é válido
	if ($tamanho != 9 && $tamanho != 12){
		return false;
	}

	if ($formatado){
		// seleciona a máscara para cpf ou cnpj
		$mascara = ($tamanho == 9) ? '###.###.###-##' : '##.###.###/####-##';

		$indice = -1;
		for ($i=0; $i < strlen($mascara); $i++) {
			if ($mascara[$i]=='#') $mascara[$i] = $codigoLimpo[++$indice];
		}
		//retorna o campo formatado
		$retorno = $mascara;

	}else{
		//se não quer formatado, retorna o campo limpo
		$retorno = $codigoLimpo;
	}

	return $retorno;

}

function curPageURL() {
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].dirname($_SERVER["REQUEST_URI"]);
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"].dirname($_SERVER["REQUEST_URI"]);
    }
    return $pageURL;
}



function jsonp_encode($json)
{
   if($_GET['callback'])
      return $_GET['callback'] . '('.json_encode($json).')';
   else
      return json_encode($json);
}

// Gera o caminho de um arquivo temporário único
function tmp_file_name()
{
   return sys_get_temp_dir().DIRECTORY_SEPARATOR.md5(uniqid(rand(), true));
}


function TrimArray($Input){

    if (!is_array($Input))
        return trim($Input);

    return array_map('TrimArray', $Input);
}

function get_multas($url, $i=0, $lista=array(), $tipo_tb)
{
   $ch = curl_init();

   $url = 'https://www.detran.mg.gov.br'.$url;

   curl_setopt($ch, CURLOPT_URL, $url);
   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
   curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
   curl_setopt($ch, CURLOPT_REFERER, "https://www.detran.mg.gov.br/veiculos/situacao-do-veiculo/consulta-a-situacao-do-veiculo/-/exibe_dados_veiculo/");
   curl_setopt($ch, CURLOPT_COOKIE, $_SESSION['cookie']);
   curl_setopt($ch, CURLOPT_HEADER, 0);
   curl_setopt($ch, CURLOPT_NOBODY, 0);

   $pg = curl_exec($ch);

   preg_match('/<div class="retorno-formulario">(.*?)<div class="acoes">/si', $pg, $tb);

   preg_match_all('/<p id="[^>]*>(.*?)<\/p>/si', $tb[1], $dados);
   $dados = $dados[1];

   $lista[$i] = array(
       utf8_encode('Infração') => $dados[6],
       'Placa' => $dados[0],
       utf8_encode('Situação') => $dados[1],
       utf8_encode('Código') => $dados[3],
       'Data' => $dados[4],
       'Hora' => $dados[5],
       'Local' => $dados[7],
       utf8_encode('Município') => $dados[8],
       utf8_encode('Data Inclusão') => $dados[9],
       'AIT' => $dados[10],
       'Processamento' => $dados[11]
   );

   if($dados[13] and $tipo_tb=='multas')
   {
       $lista[$i]['Valor'] = $dados[13];
       $lista[$i]['Fator Multiplicador'] = $dados[12];
   }
    else if($tipo_tb=='multas')
    {
        $lista[$i]['Valor'] = $dados[12];
    }

   preg_match('/<p><a href="(.*?)">Pr/', $pg, $prox_url);

   if(!empty($prox_url[1]))
   {
      return get_multas($prox_url[1], $i+1, $lista, $tipo_tb);
   }
   else
   {
      return $lista;
   }
}

function gera_verificador()
{
    return rand(1000, 9999);
}

function enviaEmail($remetente, $remetente_nome, $destinatario, $assunto, $corpo_mensagem)
{
    $header  = 'MIME-Version: 1.0' . "\r\n";
    $header .= "Content-type: text/html; charset=iso-8859-15\r\n";

    $header .= 'From: '.$remetente_nome.' <'.$remetente.'>' . "\r\n";

    @mail($destinatario, $assunto, $corpo_mensagem, $header);
}

function formata_email_verificacao($nome_usuario, $cod_verificacao)
{
    ob_start();

    ?>
    <html>
    <body style="margin: 0; padding: 0; width:100%; color: #000000; font-family: sans-serif; font-size: 16px;">
        <p>
            Olá <b><?php echo $nome_usuario?></b>! Seja bem vindo(a)!<br><br>
            Seu acesso ao Aplicativo Autocheck Detran foi liberado com sucesso.<br><br>
            Utilize a chave de segurança abaixo para liberar a consulta:<br><br>
           <div style="text-align: center; width: 100%; font-weight: bold; font-size: 40px"><?php echo $cod_verificacao ?></div><br><br>

            Essa autenticação será necessária uma única vez.<br><br>
        </p>

        <table style="font-size: 14px; width: 100%; background-color: #515b60">
            <tr>
                <td style="color: #FFFFFF">
                    <i>Atenciosamente,</i><br>
                    Team Autocheck Detran
                </td>
                <td style="text-align: right">
                    <img alt="Autoweb" src="cid:logo-app" />
                </td>
            </tr>
        </table>
    </body>
    </html><?php

    $corpo_mensagem = ob_get_contents();
    ob_end_clean();

    return $corpo_mensagem;
}

function formata_email_verificacao_plain($nome_usuario, $cod_verificacao)
{
    ob_start();

    ?>
    Olá <?php echo $nome_usuario?>! Seja bem vindo(a)!
    Seu acesso ao Aplicativo Autocheck Detran foi liberado com sucesso.
    Utilize a chave de segurança abaixo para liberar a consulta: <?php echo $cod_verificacao ?>

    Essa autenticação será necessária uma única vez.

    Atenciosamente,
    Team Autocheck Detran
    <?php

    $corpo_mensagem = ob_get_contents();
    ob_end_clean();

    return $corpo_mensagem;
}


if ( !function_exists('sys_get_temp_dir'))
{
    function sys_get_temp_dir()
    {
        if( $temp=getenv('TMP') )
            return $temp;
        if( $temp=getenv('TEMP') )
            return $temp;
        if( $temp=getenv('TMPDIR') )
            return $temp;
        $temp=tempnam(__FILE__,'');

        if (file_exists($temp))
        {
            unlink($temp);
            return dirname($temp);
        }
        return null;
    }
}


/**
 *  An example CORS-compliant method.  It will allow any GET, POST, or OPTIONS requests from any
 *  origin.
 *
 *  In a production environment, you probably want to be more restrictive, but this gives you
 *  the general idea of what is involved.  For the nitty-gritty low-down, read:
 *
 *  - https://developer.mozilla.org/en/HTTP_access_control
 *  - http://www.w3.org/TR/cors/
 *
 */
function enable_cors() {

   // Allow from any origin
   if (isset($_SERVER['HTTP_ORIGIN'])) {
      header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
      header('Access-Control-Allow-Credentials: true');
      header('Access-Control-Max-Age: 86400');    // cache for 1 day
   }

   // Access-Control headers are received during OPTIONS requests
   if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

      if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
         header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

      if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
         header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");


   }
}
