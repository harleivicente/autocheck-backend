<?php

include_once '../config.test.php';

$ch = curl_init();

$parms = array(
   'cnh' => '04369737103',
   'data_nascimento' => '08/09/1989',
   'data_habilitacao' => '28/05/2008'
);

curl_setopt($ch, CURLOPT_URL, TEST_URL.'/svc.mg/pontuacao.php');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $parms);

curl_setopt($ch, CURLOPT_COOKIEJAR, getcwd().DS.'cookie.txt');
curl_setopt($ch, CURLOPT_COOKIEFILE, getcwd().DS.'cookie.txt');
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_NOBODY, 0);

$x = curl_exec($ch);

// resultado esperado
/*
object(stdClass)[1]
  public 'condutor' => string 'Marcelo Pereira de Assis' (length=24)
  public 'total_pontos' => int 13 (length=2)
  public 'pontos' =>
    array
      0 =>
        array
          0 => string 'GUJ6381' (length=7)
          1 => string 'Transitar em velocidade superior a maxima permitida em mais de 20 ate 50' (length=72)
          2 => string '30/09/2012' (length=10)
          3 => string '13:46' (length=5)
          4 => string 'Av Risoleta Neves, a 138m da Rua R' (length=34)
          5 => string '5' (length=1)
          6 => string '746-30' (length=6)
      1 =>
        array
          0 => string 'GUJ6381' (length=7)
          1 => string 'Transitar em velocidade superior a maxima permitida em ate 20' (length=61)
          2 => string '29/12/2012' (length=10)
          3 => string '09:52' (length=5)
          4 => string 'Av Cristiano Machado 10732 Centro-' (length=34)
          5 => string '4' (length=1)
          6 => string '745-50' (length=6)
      2 =>
        array
          0 => string 'GUJ6381' (length=7)
          1 => string 'Transitar em velocidade superior a maxima permitida em ate 20' (length=61)
          2 => string '23/03/2013' (length=10)
          3 => string '17:05' (length=5)
          4 => string 'Via Expressa de Contagem N  12122' (length=33)
          5 => string '4' (length=1)
          6 => string '745-50' (length=6)
  public 'erro' => string 'nao' (length=3)
  public 'id_erro' => string '0' (length=1)
*/


$x = json_decode($x);


$erro_teste = false;

if($x->erro != 'nao')
{
   echo "Erro na asser��o 1<br>";
   echo "C�digo do erro: ".$x->id_erro.'<br>';
   echo "Descri��o do erro: ".$x->erro.'<br>';
   $erro_teste = true;
}
elseif($x->pontos[0][6] != '746-30')
{
   echo "Erro na asser��o 2<br>";
   echo "C�digo da infra��o inv�lido<br>";
   $erro_teste = true;
}
elseif($x->pontos[0][1] != 'Transitar em velocidade superior a maxima permitida em mais de 20 ate 50')
{
   echo "Erro na asser��o 3<br>";
   echo "Descricao da infra��o inv�lida<br>";
   $erro_teste = true;
}
elseif($x->pontos[1][5] != 4)
{
   echo "Erro na asser��o 4<br>";
   echo "Pontua��o inv�lida<br>";
   $erro_teste = true;
}

elseif($x->total_pontos != 13)
{
   echo "Erro na asser��o 5<br>";
   echo "Soma de pontos errada!<br>";
   $erro_teste = true;
}



if($erro_teste)
{
   echo "ERRO NOS TESTES DE SERVI�O!";
}
else
{
   echo "Teste finalizado sem erros!";
}

echo "<br><br><br>\n\n\n";

var_dump($x);
