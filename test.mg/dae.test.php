<?php

include_once '../config.test.php';

$ch = curl_init();

$parms = array(
   'placa' => 'HMG7145',
   'renavam' => '00881954985',
   'n' => '5065894',
   'valor' => '8513'
);

curl_setopt($ch, CURLOPT_URL, TEST_URL.'/svc.mg/dae.php');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $parms);

curl_setopt($ch, CURLOPT_COOKIEJAR, getcwd().DS.'cookie.txt');
curl_setopt($ch, CURLOPT_COOKIEFILE, getcwd().DS.'cookie.txt');
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_NOBODY, 0);

$x = curl_exec($ch);

// resultado esperado
// {"codigo_barras":"87780000000 3 85135827131 6 72500506589 3 41131007366 2","erro":"nao","id_erro":"0"}

$x = json_decode($x);


$erro_teste = false;

if($x->erro != 'nao')
{
   echo "Erro na asser??o 1<br>";
   echo "C?digo do erro: ".$x->id_erro.'<br>';
   echo "Descri??o do erro: ".$x->erro.'<br>';
   $erro_teste = true;
}
elseif($x->codigo_barras != '87780000000 3 85135827131 6 72500506589 3 41131007366 2')
{
   echo "Erro na asser??o 2<br>";
   echo "C?digo de barras inv?lido<br>";
   $erro_teste = true;
}

if($erro_teste)
{
   echo "ERRO NOS TESTES DE SERVI?O!";
}
else
{
   echo "Teste finalizado sem erros!";
}

echo "<br><br><br>\n\n\n";

var_dump($x);
