

## Instalação de requisitos (Ubuntu)
* sudo apt-get install mysql-server
* mysql_secure_installation (aumenta a segurança)
* sudo apt-get install php libapache2-mod-php php-mcrypt php-mysql (php apache-php bridge and php-mysql bridge)
* sudo apt-get install php-curl
### Install composer
* php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
* php composer-setup.php
* mv composer.phar /usr/local/bin/composer // global location
* Possible error: unable to conect to https://getcomposer.org/version
* Fix: https://getcomposer.org/doc/articles/troubleshooting.md#operation-timed-out-ipv6-issues-
## Install PHP extension mb_string
* sudo apt-get install php7.0-mbstring
# Install PHP extension libxml
* sudo apt-get install php7.0-xml

## Instalando e configurando proxy HTTP (Squid)
* sudo apt-get update
* sudo apt-get upgrade
* sudo apt-get install squid
* Add these lines to /etc/squid/squid.conf:
```javascript
        http_access deny all
        auth_param basic program /usr/lib/squid/basic_ncsa_auth /etc/squid/passwords
        auth_param basic realm proxy
        acl authenticated proxy_auth REQUIRED
        http_access allow authenticated 
```
### Add username and password to system:
* sudo htpasswd -c /etc/squid3/passwords username_you_like
* sudo service squid restart   

## Deploy
* preparar banco de dados: FIPE, AUTOCHECK, stored procedures x 4
* instalar requisitos
* composer install
* Atualizar arquivos do backend: 'config.php', 'config.json', 'config.dev.json' e '/svc.sp/SpConfig.php'
* Fornecer acesso para que php possa escrever na pasta '/svc.sp/captcha_images'
> chmod 777 captcha_images
* Configurar inicialização automatica de serviços: apache e squid (proxy http)

## Dados
### Servidor (HOST 1 PLUS) ssh
* IP: 181.41.220.208
* user: root
* Password: 8^ss2^;TgOgHhY
### Servidor MySql
* username: root
* password: asdFGfDf$3@4
* database: autocheck
### Proxy HTTP (Squid)
* username: autocheck_server
* password: df4$gfkg@D3FJGd3
* port: 8888   