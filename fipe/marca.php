<?php

include_once "../config.php";
include_once "../db.php";
include_once "../query.php";

header('content-type: application/json; charset=utf-8');

$db = new db(DADOS_SERVER, DADOS_USUARIO, DADOS_SENHA, DADOS_FIPE);

/*
 * V = Autom�vel
 * M = Motocicleta
 * C = Caminh�es e �nibus
 */
$tipo_veiculo = addslashes($_POST['tipo_veiculo']);
//$tipo_veiculo = "V";

if(!$tipo_veiculo)
    exit;

$sql = "select ma.idmarca as id, ma.nome from marca ma
        inner join modelo mo on mo.idmarca = ma.idmarca
        where idtipo = '".$tipo_veiculo."'
        group by ma.idmarca
        order by ma.nome asc";

$q = new query($db, $sql);

if($q->erro())
    exit;

$json = array();
while($rs = $q->fetch())
{
    $rs = array(
        'id' => utf8_encode($rs['id']),
        'nome' => utf8_encode($rs['nome'])
    );

    $json[] = $rs;
}

echo json_encode($json);