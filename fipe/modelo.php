<?php

include_once "../config.php";
include_once "../db.php";
include_once "../query.php";

header('content-type: application/json; charset=utf-8');

$db = new db(DADOS_SERVER, DADOS_USUARIO, DADOS_SENHA, DADOS_FIPE);

$idmarca = addslashes(intval($_POST['idmarca']));
$tipo = addslashes($_POST['tipo']);
//$idmarca = '6';

if(!$idmarca or !$tipo)
    exit;

$sql = "select idmodelo as id, nome from modelo where idmarca = '".$idmarca."' and idtipo = '".$tipo."' order by nome asc";

$q = new query($db, $sql);

if($q->erro())
    exit;

$json = array();
while($rs = $q->fetch())
{
    $rs = array(
        'id' => utf8_encode($rs['id']),
        'nome' => utf8_encode($rs['nome'])
    );

    $json[] = $rs;
}

echo json_encode($json);