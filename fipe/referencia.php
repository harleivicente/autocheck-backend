<?php

include_once "../config.php";
include_once "../db.php";
include_once "../query.php";

header('content-type: application/json; charset=utf-8');

$db = new db(DADOS_SERVER, DADOS_USUARIO, DADOS_SENHA, DADOS_FIPE);

$idmodelo = addslashes(intval($_POST['idmodelo']));
//$idmodelo = '53';

if(!$idmodelo)
    exit;

$sql = "select r1.ano_modelo, r1.preco_medio from referencia r1
        where r1.idmodelo = '".$idmodelo."'
        and r1.mes_ano = (select max(r2.mes_ano) as ma from referencia r2 where r2.idmodelo = '".$idmodelo."')";

$q = new query($db, $sql);

if($q->erro())
    exit;

$json = array();
while($rs = $q->fetch())
{
    $rs = array(
        'ano_modelo' => $rs['ano_modelo'],
        'preco_medio' =>  'R$ '.number_format($rs['preco_medio'], 2, ',', '.')
    );

    $json[] = $rs;
}

echo json_encode($json);