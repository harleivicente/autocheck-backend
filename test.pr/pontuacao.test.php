<?php

include_once '../config.test.php';

if(!$_GET['captcha'])
{
   echo "Erro! Esqueceu a captcha?";
   exit;
}

$ch = curl_init();

// paulo - muitos pontos
$parms = array(
    'cnh' => '04347959167',
    'cpf' => '53682815953',
    'captcha' => $_GET['captcha']
);

// Waldir - muitos pontos
/*$parms = array(
    'cnh' => '1097929369',
    'cpf' => '83084410925',
    'captcha' => $_GET['captcha']
);*/


// Quintino - 100% limpo
/*$parms = array(
    'cnh' => '2817666469',
    'cpf' => '28499603904',
    'captcha' => $_GET['captcha']
);*/


curl_setopt($ch, CURLOPT_URL, TEST_URL.'/svc.pr/pontuacao.php');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parms));

curl_setopt($ch, CURLOPT_COOKIEJAR, getcwd().DS.'cookie.txt');
curl_setopt($ch, CURLOPT_COOKIEFILE, getcwd().DS.'cookie.txt');
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_NOBODY, 0);

$x = curl_exec($ch);

echo $x;

