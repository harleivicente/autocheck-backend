<?php

include '../config.php';

session_start();

// -------------------------------------------------------------------------
// Abrir de consulta pontos e buscar cookies
// -------------------------------------------------------------------------


$url = "http://www2.detran.rj.gov.br/portal/veiculos/consultaCadastro";

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_REFERER, "http://www.detran.rj.gov.br/_monta_aplicacoes.asp?cod=14&tipo=crlv");
curl_setproxy($ch, 'rj');
curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HEADER, 1);
curl_setopt($ch, CURLOPT_NOBODY, 1);

$x = curl_exec($ch);
//echo $x;
//exit;

preg_match('/Set-Cookie: (.*?);/si', $x, $cookie);

$_SESSION['cookie'] = $cookie[1];

// -------------------------------------------------------------------------
// Abrir captcha
// -------------------------------------------------------------------------

$url = "http://www2.detran.rj.gov.br/portal/veiculos/captcha_image?v=".time().rand(100,999);

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_REFERER, "http://www2.detran.rj.gov.br/portal/veiculos/consultaCadastro");
curl_setproxy($ch, 'rj');
curl_setopt($ch, CURLOPT_COOKIE, $_SESSION['cookie']);
curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_NOBODY, 0);


header('Content-type: image/jpeg');

//exibir captcha ao usu�rio
echo curl_exec($ch);
