<?php
    require_once "../library/SpLib.php";
    require_once "../vendor/autoload.php";
    require_once '../config.php';
    require_once '../inc.php';
    require_once "../db.php";
    require_once "../query.php";

    use PHPHtmlParser\Dom;
    use PHPHtmlParser\Dom\HtmlNode;

    session_start();
    
    /* 
        @param cpf
        @param cnh
        @param captcha

        @return array(
            'erro' => '',
            'id_erro' => ERROR CODE,
            'total_pontos' => 343,
            'pontos' => array (
                'AIT' => '',
                'Orgao Autuador' => '',
                'Placa' => '',
                'Local' => '',
                'Data' => '',
                'Hora' => '',
                'Pontos' => ''
            )[],
            'condutor' => array(
                'Nome' => '',
                'CPF' => '',
                'CNH' => ''
            ),
            'cpf' => '',
            'cnh' => ''
            )

            Errors: 
                1 - parametros
                4 - serdivor fora do ar,
                3 - outro,
                35 - captcha incorreto

    */

set_error_handler(function($code, $msg, $file, $line) {
    echo json_encode(array ('erro' => 'Erro inesperado. Tente novamente mais tarde.', 'id_erro' => '3','msg' => $msg, 'file' => $file, 'line' => $line));
    exit;
});   

if(array_key_exists('email', $_POST)){
    $email = trim($_POST['email']);
}

$params = array("cpf", "cnh", "captcha");
foreach ($params as $key => $param) {
    if(!array_key_exists($param, $_POST)){
        echo json_encode(array ('erro' => 'Erro interno do sistema. Tente novamente mais tarde.', 'id_erro' => '1', 'msg' => "Parametros incorretos."));
        exit;
    }
}

// Sessao iniciada em outra chamada a 'pontuacao.auth.v2.php'
$session = $_SESSION['jsession'];

// Fazer consulta
$url = "http://multas.detran.rj.gov.br/gaideweb2/consultaPontuacao/busca";
$dados = (object) array(
    'cpf' => $_POST['cpf'],
    'cnh' => $_POST['cnh'],
    'uf' => 'RJ',
    'captcha' => $_POST['captcha']
);
$consulta_inicial = SpLib::curl_post($url, $dados, array(
    CURLOPT_HEADER => 1,
    CURLOPT_FOLLOWLOCATION => 1        
), array(
    'Cookie: JSESSIONID=' . $session . '; Path=/gaideweb2;',
    'Accept: image/webp,image/apng,image/*,*/*;q=0.8',
    'Referer: http://multas.detran.rj.gov.br/gaideweb2/consultaPontuacao/busca',
    'User-Agent: Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'    
));

// Verifica se houve erro no captcha
$sucesso_regex = '/<p id="errorMsg">O valor da captcha /i';
if(preg_match($sucesso_regex, $consulta_inicial)) {
    echo json_encode(array ('erro' => 'Captcha incorreto.', 'id_erro' => '35','msg' => 'Captcha incorreto'));
    exit;
}
$sucesso_regex = "/CONSULTA TODAS AS INFRA/i";
if(!preg_match($sucesso_regex, $consulta_inicial)) {
    echo json_encode(array ('erro' => 'Dados fornecidos para CPF/CNH incorretos.', 'id_erro' => '3'));
    exit;
}

// Acessar link de todas as infracoes (pontuaveis e mandatorias) dos ultimos 5 anos
$url = "http://multas.detran.rj.gov.br/gaideweb2/consultaPontuacao/busca/5anos";

$infracoes = SpLib::curl_get($url, array(), array(
    CURLOPT_HEADER => 0,
    CURLOPT_FOLLOWLOCATION => 1  
), array(
    'Cookie: JSESSIONID=' . $session . '; Path=/gaideweb2;',
    'Accept: image/webp,image/apng,image/*,*/*;q=0.8',
    'Referer: http://multas.detran.rj.gov.br/gaideweb2/consultaPontuacao/busca',
    'User-Agent: Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'    
));
$sucesso_regex = "/CONSULTA TODAS AS INFRA/i";
if(!preg_match($sucesso_regex, $infracoes)) {
    echo json_encode(array ('erro' => 'Erro inesperado. Tente novamente mais tarde.', 'id_erro' => '3'));
    exit;
}

// Preparar output
$output = array(
    'total_pontos' => '',
    'pontos' => array(),
    'id_erro' => 0,
    'erro' => '',
    'cpf' => $_POST['cpf'],
    'cnh' => $_POST['cnh'],
    'condutor' => array(
        'Nome' => '',
        'CPF' => $_POST['cpf'],
        'CNH' => $_POST['cnh']
    )
);

// Parse no DOM
$dom = new Dom;
if(!$dom->load($infracoes)){
    echo json_encode(array ('erro' => 'Erro interno. Tente novamente mais tarde.', 'id_erro' => '3','msg' => $msg));
    exit;    
}

// Obter contagem de pontos (últimos 5 anos)
// A terceira coluna da primeira linha da primeira tabela
$pagina_inicial_dom = new Dom;
$inicial_load = $pagina_inicial_dom->load($consulta_inicial);
if($inicial_load) {
    $colunas_tabela = $pagina_inicial_dom->find("table td");
    if(sizeof($colunas_tabela) > 3) {
        $output['total_pontos'] = $colunas_tabela[2]->innerHtml();
    }
}

// Verificar multas
$itens = $dom->find("#accordion .panel");
foreach ($itens as $key => $item) {
    $dados_infracao = array();

    /* 
        Para obter o numero da occorrencia
     
        > nome do attributo: .row > div label
        > valor do attributo: .row > div span

     */
    $valores = $item->find(".row > div");
    foreach ($valores as $key => $valor) {
        
        // verificar se coluna é válida
        $nome_attr = $valor->find('label');
        $valor_attr = $valor->find('span');
        if($nome_attr && $valor_attr) {
            $nome_attr_string = utf8_encode($nome_attr[0]->innerHtml());
            $dados_infracao[$nome_attr_string] = utf8_encode($valor_attr[0]->innerHtml());

            if(preg_match("/Resp. Pontos/i", $nome_attr_string)) {
                $output['condutor']['Nome'] = utf8_encode($valor_attr[0]->innerHtml());
            }

        }
    }
    
    $output['pontos'][] = $dados_infracao;
}

if(!empty($email)) {
   $db = new db();
   $db->query("call add_log_condutor('RJ', '".$email."', '".$_POST['cnh']."', '', '', '".$_POST['cpf']."', ".$output['total_pontos'].", '".json_encode($output, JSON_UNESCAPED_UNICODE)."');");
}

echo json_encode($output, JSON_UNESCAPED_UNICODE);
exit;


