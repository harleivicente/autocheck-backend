<?php

include '../config.php';
include '../inc.php';
include "../db.php";
include "../query.php";

$dados_veiculo =  array(); // saida do json

session_start();

header('content-type: application/json; charset=utf-8');

$placa     = strtoupper(trim($_POST['placa']));
$renavan   = strtoupper(trim($_POST['renavan']));
$captcha1  = strtolower(trim($_POST['captcha1']));
$captcha2  = strtolower(trim($_POST['captcha2']));
$email     = trim($_POST['email']);

if(!$placa or !$captcha1 or !$renavan or !$captcha2)
{
   $json = array("erro" => "Um ou mais parametros invalidos", 'id_erro' => '1');
   echo jsonp_encode($json);
   exit;
}

// Inicializando variaveis
$dados_veiculo =  array(); // saida do json

$dados_veiculo['erro'] = 'nao';
$dados_veiculo['id_erro'] = '0';

$dados_veiculo['placa'] = $placa;
$dados_veiculo['chassi'] = null;
$dados_veiculo['renavam'] = $renavan;

$dados_veiculo['veiculo'] = array();
$dados_veiculo['imposto'] = array();

$dados_veiculo['infracao']['multas'] = array();
$dados_veiculo['infracao']['autuacoes'] = array();

// -------------------------------------------------------------------------------
// Buscar dados do ve�culo
// -------------------------------------------------------------------------------

$url = "http://www2.detran.rj.gov.br/portal/veiculos/consultaCadastro";

$ch = curl_init();

$data = array(
   "data[placa]" => $placa,
   "data[code]" => $captcha1
);

curl_setopt($ch, CURLOPT_URL, $url);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);

curl_setproxy($ch, 'rj');

curl_setopt($ch, CURLOPT_HTTPHEADER, array('Origin: http://www2.detran.rj.gov.br'));
curl_setopt($ch, CURLOPT_REFERER, "http://www2.detran.rj.gov.br/portal/veiculos/consultaCadastro");
curl_setopt($ch, CURLOPT_COOKIE, $_SESSION['cookie']);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_NOBODY, 0);

$pg = curl_exec($ch);

// estourou o timeout, erro no servidor do detran
// ou servidor indispon�vel
if(!$pg or preg_match('/tente mais tarde/sim', $pg))
{
   echo jsonp_encode( array(
       "erro" => utf8_encode("Servidor do Detran-RJ temporariamente indispon�vel... :("),
       'id_erro' => 4
   ));

   exit;
}

// --------------------------------------------------------------------------
// Tratamento de erros
// -------------------------------------------------------------------------------

if(preg_match('/cadastrado na base estadual/si', $pg))
{
   $json = array("erro" => utf8_encode("Ve�culo n�o cadastrado no estado do RJ!"), 'id_erro' => '1');
   echo jsonp_encode($json);
   exit;
}
elseif(preg_match('/'.utf8_encode('digo de Seguran�a<\/b> corretamente').'/sim', $pg))
{
   $json = array("erro" => utf8_encode("O Conte�do da imagem 1 n�o corresponde!"), 'id_erro' => '2');
   echo jsonp_encode($json);
   exit;
}

//echo $pg;
//exit;

// -------------------------------------------------------------------------------
// Parse dos dados do ve�culo
// -------------------------------------------------------------------------------

preg_match_all('/<div[^>]*>(.*?)<\/div>/si', $pg, $tabela);

// tirando html da linha de licenciamento
$tabela[1][0] = trim(strip_tags($tabela[1][0]));

//print_r($tabela[1]);
//exit;

/*
AGORA TEMOS EM $TABELA[1]

Array
(
    [0] => 2010    -- licenciamento
    [1] => PREF    -- nome/endereco
    [2] => LBV1344  -- placa
    [3] => LCX0770  -- placa anterior
    [4] => ****************   --- cpf (nunca vem preenchido)
    [5] => PASSAGEIRO -- tipo
    [6] => GASOLINA   -- combustivel
    [7] => VW/QUANTUM 2000 MI -- marca /modelo
    [8] => 1997 -- ano fab
    [9] => 1998 -- ano mod
    [10] => 5 / 117 / 2000
    [11] => OFICIAL -- categoria
    [12] => PRETA -- cor
    [13] => ALIENACAO FIDUCIARIA<BR> -- impedimento
    [14] => STO ANTONIO DE PADUA - municipio
    [15] => &nbsp;
)
*/

$dados_veiculo['veiculo'] = array(
    'Placa' => $placa,
    'Renavam' => $renavan,
    utf8_encode('Ano Fabrica��o') => $tabela[1][8],
    'Ano Modelo' => $tabela[1][9],
    'Categoria' => $tabela[1][11],
    'Cor' => $tabela[1][12],
    'Marca' => $tabela[1][7],
    'Tipo' => $tabela[1][5],
    utf8_encode('Combust�vel') => $tabela[1][6],
    utf8_encode('Munic�pio') => $tabela[1][14],
    'Placa Anterior' => $tabela[1][3] ? $tabela[1][3] : 'N/A',
    'Impedimento' => $tabela[1][13] ? $tabela[1][13] : 'N/A'
);


$dados_veiculo['imposto']["LICENCIAMENTO"][$tabela[1][0]] = '';


//$dados_veiculo['veiculo']['taxa_licenciamento'] = $tabela[1][0];


// -------------------------------------------------------------------------------
// Buscar dados das multas do ve�culo
// -------------------------------------------------------------------------------

$url = "http://www2.detran.rj.gov.br/portal/multas/nadaConsta";

$ch = curl_init();

$data = array(
   "_method" => 'POST',
   "data[Multas][renavam]" => $renavan,
   "data[Multas][code]" => $captcha2
);

curl_setopt($ch, CURLOPT_URL, $url);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);

curl_setproxy($ch, 'rj');

curl_setopt($ch, CURLOPT_HTTPHEADER, array('Origin: http://www2.detran.rj.gov.br'));
curl_setopt($ch, CURLOPT_REFERER, "http://www2.detran.rj.gov.br/portal/multas/nadaConsta");
curl_setopt($ch, CURLOPT_COOKIE, $_SESSION['cookie_multa']);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_NOBODY, 0);

$pg = curl_exec($ch);

//echo $pg;
//exit;

// --------------------------------------------------------------------------
// Tratamento de erros
// -------------------------------------------------------------------------------

if(preg_match('/o consta no cadastro do Detran/si', $pg))
{
    $json = array("erro" => utf8_encode('Ve�culo n�o cadastrado no estado do RJ!'), 'id_erro' => '1');
    echo jsonp_encode($json);
    exit;
}
elseif(preg_match('/'.utf8_encode('digo de Seguran�a<\/b> corretamente').'/sim', $pg))
{
    $json = array("erro" => utf8_encode('Conte�do da imagem 2 n�o corresponde!'), 'id_erro' => '2');
    echo jsonp_encode($json);
    exit;
}

// -------------------------------------------------------------------------------
// Parse das multas do ve�culo
// -------------------------------------------------------------------------------

preg_match_all("/<table[^>]*>(.*?)<\/table>/si", $pg, $tables);
$tables = $tables[1];

array_shift($tables); // retira a primeira linha da tabela de multas (o cabe�alho)


foreach($tables as $multa)
{
   preg_match_all("/<br><\/span>(.*?)<\/td>/si", $multa, $tds);

   //print_r($tds[1]);
   //exit;

   $tmp = array(
      utf8_encode('Infra��o') => $tds[1][6],
      'AIT' => $tds[1][0],
      utf8_encode('C�digo') => $tds[1][1],
      'Data' => $tds[1][4],
      'Hora' => $tds[1][5],
      'Local' => $tds[1][8],
      'Valor' => 'R$ ' . ($tds[1][10] ? $tds[1][10] : $tds[1][9]),
      utf8_encode('Situa��o') => $tds[1][11]
   );

   $dados_veiculo['infracao']['multas'][trim($tds[1][12])][] = $tmp;
}

// gerar resposta ao client

$json = jsonp_encode($dados_veiculo);

if(!empty($email))
{
    $db = new db();
    $db->query("call add_log_veiculo ('RJ', '".$email."', '".$placa."', '', '".$dados_veiculo['renavam']."', '', '".$json."');");
}

echo $json;
