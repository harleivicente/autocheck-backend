<?php
require_once "../library/SpLib.php";
session_start();

/* 
    Retorna erro no padrao do sistema ou uma imagem com o captcha.
*/
    
$url = "http://multas.detran.rj.gov.br/gaideweb2/consultaPontuacao/busca";

$home = SpLib::curl_get($url, array(), array(
    CURLOPT_HEADER => 1,
    CURLOPT_FOLLOWLOCATION => 0      
), array());

if(!$home) {
    echo json_encode(array('erro' => 'Detran RJ indisponvível...', 'id_erro' => 4));
    exit;
}

// Obter os cookies JSESSIONID e Path
$session_regex = "/JSESSIONID=([^;]+);/i";
preg_match($session_regex, $home, $session);
if(!$session[1]) {
    echo json_encode(array('erro' => 'Detran RJ indisponvível...', 'id_erro' => 4));
    exit;
} else {
    $session = $session[1];
    $_SESSION['jsession'] = $session;
} 

// Obter image do captcha
$captcha_url = "http://multas.detran.rj.gov.br/gaideweb2/captcha";
$captcha_img = SpLib::curl_get($captcha_url, array(), array(
    CURLOPT_FOLLOWLOCATION => 1,
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_BINARYTRANSFER => 1    
), array(
    'Cookie: JSESSIONID=' . $session . ';'    
));

if(!$captcha_img) {
    echo json_encode(array('erro' => 'Detran RJ indisponvível...', 'id_erro' => 4));
    exit;
}

header('Content-type: image/jpeg');
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
echo $captcha_img;
