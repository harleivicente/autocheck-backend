<?php

include '../config.php';
include '../inc.php';
include "../db.php";
include "../query.php";

session_start();
error_reporting(0);

header('content-type: application/json; charset=utf-8');

$placa = strtoupper(trim($_POST['placa']));
$email = trim($_POST['email']);

// caminhao governo
//$placa = "KIG0751";
// Land rover roubada
//$placa = "KHT7368";

// caminh�o 100%
//$placa = "KIG3046";

// 100% limpo
//$placa = "KLI2771";

// muita multa
//$placa = 'KJS3324';


if(!$placa)
{
   $json = array("erro" => "Um ou mais parametros invalidos", 'id_erro' => '1');
   echo jsonp_encode($json);
   exit;
}

// Inicializando variaveis
$dados_veiculo =  array(); // saida do json

$dados_veiculo['erro'] = 'nao';
$dados_veiculo['id_erro'] = '0';

$dados_veiculo['placa'] = null;
$dados_veiculo['chassi'] = null;
$dados_veiculo['renavam'] = null;

$dados_veiculo['veiculo'] = array();
$dados_veiculo['imposto'] = array();

$dados_veiculo['infracao']['multas'] = array();
$dados_veiculo['infracao']['autuacoes'] = array();

// -------------------------------------------------------------------------
// Buscar informa��es do ve�culo
// -------------------------------------------------------------------------

$url = "http://online4.detran.pe.gov.br/ServicosWeb/Veiculo/frmConsultaPlaca.aspx?placa=".$placa;

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setproxy($ch, 'pe');
curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_NOBODY, 0);

$html_veiculo = curl_exec($ch);

if(!$html_veiculo) // estourou o timeout, erro no servidor do detran
{
   $json = array("erro" => utf8_encode("Servidor do Detran-PE temporariamente indispon�vel... :("), 'id_erro' => '1');
   echo jsonp_encode($json);
   exit;
}
elseif(preg_match('/<span id="lblErro" class="statusNaoRealizado">(.*?)<\/span>/sim', $html_veiculo, $erro))
{
   $json = array("erro" => "Não foi possível fazer essa consulta.", 'id_erro' => '2');
   echo jsonp_encode($json);
   exit;
}

//echo $html_veiculo;
//exit;

// -------------------------------------------------------------------------
// Buscar campos de controle do aspx
// -------------------------------------------------------------------------
//__VIEWSTATE
//__EVENTVALIDATION
//btnDetalhamento	  = Detalhamento de D�bito
//btnDebitosInvestigacao =	Debitos de Investigacao

preg_match_all('/ value="(.*?)" /sim', $html_veiculo, $val);

$viewstate = $val[1][0];
$event_validation = $val[1][1];

// -------------------------------------------------------------------------
// Consultar detalhamento de debito
// -------------------------------------------------------------------------

$dados_post = array(
	'__VIEWSTATE' => $viewstate,
	'__EVENTVALIDATION' => $event_validation,
	'btnDetalhamento' => utf8_encode('Detalhamento de D�bito')
);


$url = "http://online4.detran.pe.gov.br//ServicosWeb/Veiculo/frmDetalhamentoDebitos.aspx?pPlaca=$placa&pExtrato=N&pTerceiros=I&pPlacaOutraUF=N";

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_REFERER, 'http://online4.detran.pe.gov.br/ServicosWeb/Veiculo/frmConsultaPlaca.aspx?placa='.$placa);
curl_setproxy($ch, 'pe');

curl_setopt($ch, CURLOPT_HTTPHEADER, array("Origin: http://online4.detran.pe.gov.br"));
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($dados_post));

curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_NOBODY, 0);

$html_debito = curl_exec($ch);

// -------------------------------------------------------------------------
// Fazer parse dos dados de veiculos
// -------------------------------------------------------------------------

preg_match_all('/weight:bold">(.*?)<\/span>/sim', $html_veiculo, $dados);
$dados = $dados[1];
//print_r($dados);
//exit;

$dados_veiculo['placa'] = $placa;
$dados_veiculo['chassi'] = $dados[1];

$dados_veiculo['veiculo']['Placa'] = $dados[0];
$dados_veiculo['veiculo']['Chassi'] = $dados[1];
$dados_veiculo['veiculo']['Espécie/Tipo'] = $dados[2];
$dados_veiculo['veiculo']['Combustível'] = $dados[3];

$marca_modelo = explode('/', $dados[4]);

$dados_veiculo['veiculo']['Marca'] = $marca_modelo[0];
$dados_veiculo['veiculo']['Modelo'] = $marca_modelo[1];

$dados_veiculo['veiculo']['Ano Fabrição'] = $dados[5];
$dados_veiculo['veiculo']['Ano Modelo'] = $dados[6];

$cap_pot_cil = explode('/', $dados[7]);

$dados_veiculo['veiculo']['Capacidade'] = $cap_pot_cil[0];
$dados_veiculo['veiculo']['Potência'] = $cap_pot_cil[1] . ' cv';
$dados_veiculo['veiculo']['Cilidradas'] = $cap_pot_cil[2] . ' cc';

$dados_veiculo['veiculo']['Categoria'] = $dados[8];
$dados_veiculo['veiculo']['Cor'] = $dados[9];


// dados das restricoes
preg_match_all('/<span id="Label35" style="color:red;font-size:12px;font-family:Arial">(.*?)<\/div>/sim', $html_veiculo, $dados);
preg_match_all('/font-weight:bold">(.*?)<\/span>/sim', $dados[1][0], $dados);
$dados = $dados[1];

//print_r($dados);
//exit;

$cabecalho2 = 'Restrições';

if(sizeof($dados) > 1) // posssui restricao
{
   foreach($dados as $restricao)
   {
   	if(!empty($restricao))
   		$dados_veiculo['veiculo'][$cabecalho2][] = utf8_encode($restricao);
   }
}
else
{
   $dados_veiculo['veiculo'][$cabecalho2][] = "Sem restrição";
}


// -------------------------------------------------------------------------
// Fazer parse dos dados dos d�bitos do veiculo - IMPOSTOS
// -------------------------------------------------------------------------


// Inicializa todos impostos como "Pago"
$dados_veiculo['imposto']['BOMBEIROS'][date('Y')] = "Pago";
$dados_veiculo['imposto']['LICENCIAMENTO'][date('Y')] = "Pago";
$dados_veiculo['imposto']['IPVA'][date('Y')] = "Pago";
$dados_veiculo['imposto']['SEGURO OBRIGATORIO'][date('Y')] = "Pago";


preg_match('/<div id="pnlLicenciamento">(.*?)<div id="pnlMsgMulta">/sim', $html_debito, $dados);
$dados = $dados[1];

preg_match_all('/<td class="col1">(.*?)<\/td><td class="col2">/sim', $dados, $impostos);
preg_match_all('/<td class="col2">(.*?)<\/td><td class="col3">/sim', $dados, $anos);
preg_match_all('/<td class="col5">(.*?)<\/td>/sim', $dados, $valores);

foreach($impostos[1] as $i=>$imposto)
{
   $dados_veiculo['imposto'][$imposto][$anos[1][$i]] = 'R$ '.$valores[1][$i];
}

// -------------------------------------------------------------------------
// Fazer parse dos dados dos d�bitos do veiculo - MULTAS
// -------------------------------------------------------------------------

preg_match('/<strong>MULTAS<\/strong><\/td>(.*?)<strong>MULTAS COM RECURSO\/SUSPENSIVO<\/strong>/sim', $html_debito, $dados);

//print_r($dados);
//exit;

preg_match_all('/<td colspan="3">Ag.(.*?)Data Afericao:<\/td>/sim', $dados[1], $multas);


foreach($multas[0] as $multa)
{
   preg_match_all('/<td colspan="3">(.*?)<\/td>/sim', $multa, $str_detalhe);
   $orgao = str_replace('Ag.Autuador:', '', $str_detalhe[1][0]);
   //echo $orgao.'<br>';

   preg_match('/<td>Valor\(R\$\): (.*?)<\/td>/sim', $multa, $valor);
   $valor = $valor[1];
   //echo $valor[1].'<br>';

   preg_match('/Vencimento: (.*?)<\/td>/sim', $multa, $vencimento);
   $vencimento = $vencimento[1];
   //echo $valor[1].'<br>';

   $campos = preg_split('/Lote:|Ag\.Autuador:|Serie:|Auto:|Infracao:|Data:|Local:|Amparo Legal:|Velocidade da via:|Aferida:|Considerada:|Equipamento:|Data Afericao:/sim', $str_detalhe[1][1]);

   $data_hora = explode(' ', $campos[6]);

   $campos = array(
       utf8_encode('Infra��o') =>  trim($campos[5]),
      'Lote' =>  trim($campos[1]),
      'Autuador' =>  trim($campos[2]),
      utf8_encode('S�rie') =>  trim($campos[3]),
      'Auto' =>  trim($campos[4]),
      'Data' =>  trim($data_hora[1]),
      'Hora' =>  trim($data_hora[2]),
      'Local' =>  trim($campos[7]),
      'Amparo Legal' =>  trim($campos[8]),
      'Velocidade da via' => trim($campos[9]),
      'Aferida' =>  trim($campos[10]),
      'Considerada' =>  trim($campos[11]),
      'Equipamento' =>  trim($campos[12]),
       utf8_encode('Data Aferi��o') =>  trim($campos[13]),
      'Vencimento' =>  $vencimento,
      'Valor' =>  'R$ '.$valor
   );

   $dados_veiculo['infracao']['multas'][$orgao][] = $campos;

   //print_r($campos);
   //exit;
}

// -------------------------------------------------------------------------
// Fazer parse dos dados dos d�bitos do veiculo - AUTUA��ES
// -------------------------------------------------------------------------

preg_match('/es sujeitos a altera(.*?)<td colspan="3"><strong>INFRA/sim', $html_debito, $dados);

//print_r($dados);
//exit;

preg_match_all('/<td colspan="3">Ag.(.*?)Data Afericao:<\/td>/sim', $dados[1], $multas);


foreach($multas[0] as $multa)
{
   preg_match_all('/<td colspan="3">(.*?)<\/td>/sim', $multa, $str_detalhe);
   $orgao = str_replace('Ag.Autuador:', '', $str_detalhe[1][0]);
   //echo $orgao.'<br>';

   preg_match('/<td>Valor\(R\$\): (.*?)<\/td>/sim', $multa, $valor);
   $valor = $valor[1];
   //echo $valor[1].'<br>';

   preg_match('/o\/Defesa: (.*?)<\/td>/sim', $multa, $vencimento);
   $vencimento = $vencimento[1];
   //echo $valor[1].'<br>';

   $campos = preg_split('/Lote:|Ag\.Autuador:|Serie:|Auto:|Infracao:|Data:|Local:|Amparo Legal:|Velocidade da via:|Aferida:|Considerada:|Equipamento:|Data Afericao:/sim', $str_detalhe[1][1]);

   $data_hora = explode(' ', $campos[6]);

   $campos = array(
       utf8_encode('Infra��o') =>  trim($campos[5]),
      'Lote' =>  trim($campos[1]),
      'Autuador' =>  trim($campos[2]),
       utf8_encode('S�rie') =>  trim($campos[3]),
      'Auto' =>  trim($campos[4]),
      'Data' =>  trim($data_hora[1]),
      'Hora' =>  trim($data_hora[2]),
      'Local' =>  trim($campos[7]),
      'Amparo Legal' =>  trim($campos[8]),
      'Velocidade da via' => trim($campos[9]),
      'Aferida' =>  trim($campos[10]),
      'Considerada' =>  trim($campos[11]),
      'Equipamento' =>  trim($campos[12]),
      utf8_encode('Data Aferi��o') =>  trim($campos[13]),
      'Limite Defesa' =>  $vencimento,
      'Valor' =>  'R$ '.$valor,
   );

   $dados_veiculo['infracao']['autuacoes'][$orgao][] = $campos;

}


$json = jsonp_encode($dados_veiculo);

if(!empty($email))
{
    $db = new db();
    $db->query("call add_log_veiculo ('PE', '".$email."', '".$placa."', '".$dados_veiculo['chassi']."', '".$dados_veiculo['renavam']."', '', '".$json."');");
}

echo $json;
