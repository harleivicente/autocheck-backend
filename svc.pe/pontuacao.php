<?php

include '../config.php';
include '../inc.php';
include "../db.php";
include "../query.php";


session_start();

header('content-type: application/json; charset=utf-8');

$cnh = trim($_POST['cnh']);
$email = trim($_POST['email']);

//$cnh = '03712472864';
//$cnh = '02559607541';

if(!$cnh)
{
    $json = array("erro" => "Um ou mais parametros invalidos", 'id_erro' => '1');
    echo jsonp_encode($json);
    exit;
}

// Inicializando variaveis
$dados_condutor =  array(); // saida do json

$dados_condutor['erro'] = 'nao';

$dados_condutor['id_erro'] = '0';

$dados_condutor['condutor'] = array(); // ficha do condutor
$dados_condutor['total_pontos'] = 0;
$dados_condutor['pontos'] = array();

$dados_condutor['cpf'] = null;
$dados_condutor['cnh'] = $cnh;

// -------------------------------------------------------------------------
// Buscar pontos do condutor
// -------------------------------------------------------------------------

$url = "http://online4.detran.pe.gov.br/NovoSite/detran/cnh/result_ConsultaPontos.aspx?parTipo=2&parChave=".$cnh;

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setproxy($ch, 'pe');
curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_NOBODY, 0);

$html_pontos = curl_exec($ch);

if(!$html_pontos) // estourou o timeout, erro no servidor do detran
{
    $json = array("erro" => utf8_encode("Servidor do Detran-PE temporariamente indispon�vel... :("), 'id_erro' => '1');
    echo jsonp_encode($json);
    exit;
}
elseif(preg_match('/Nada consta referente a registro de pontu/sim', $html_pontos))
{
// Pular essa parte: podem aparecer outras informa��es al�m dos pontos
//    echo jsonp_encode($dados_condutor);
//    exit;
}
elseif(preg_match('/<span id="lblErro" [^>]*>(.*?)<\/span>/sim', $html_pontos, $erro))
{
    $json = array("erro" => $erro[1], 'id_erro' => '2');
    echo jsonp_encode($json);
    exit;
}

//echo $html_pontos;
//exit;

preg_match_all('/<table[^>]*>(.*?)<\/table>/sim', $html_pontos, $tabelas);
$tabelas = $tabelas[1];

//print_r($tabelas[1]);
//exit;

// -------------------------------------------------------------------------
// Buscar ficha do condutor
// -------------------------------------------------------------------------

preg_match_all('/<span[^>]*>(.*?)<\/span>/sim', $tabelas[0], $ficha);
$ficha = $ficha[1];

//print_r($ficha);
//exit;

$dados_condutor['condutor']['Nome'] = $ficha[3];
$dados_condutor['condutor']['CPF'] = $ficha[5];
$dados_condutor['condutor']['CNH'] = $ficha[7];

// -------------------------------------------------------------------------
// Buscar total de pontos
// -------------------------------------------------------------------------

preg_match('/<span id="lblPontos" [^>]*>(.*?)<\/span>/sim', $html_pontos, $tot_pontos);

$dados_condutor['total_pontos'] = intval($tot_pontos[1]);

// -------------------------------------------------------------------------
// Buscar detalhamento de pontos
// -------------------------------------------------------------------------

preg_match_all('/<TABLE id="Table5"[^>]*>(.*?)<\/TABLE>/sim', $html_pontos, $pontos);
$pontos = $pontos[1];

//array_shift($pontos); // retira a primeira linha da tabela (o cabe�alho)

foreach($pontos as $ponto)
{
    $ponto = preg_split('/'.utf8_encode('�rg�o Autuante:|Auto:|Artigo:|S�rie:|Data:|Placa:|Pontos:|Infra��o:|Local da infra��o:').'/sim', strip_tags($ponto));

    foreach($ponto as $k=>$v)
    {
        $ponto[$k] = str_replace(array('&nbsp;', "\r\n", "\r", "\n"), "", trim($v));
    }

    /*
    Array
    (
    [0] =>
    [1] => RECIFE
    [2] => AC
    [3] => 277780
    [4] => 218, I,  ,
    [5] => 29/09/2013
    [6] => KKK8058
    [7] => 4
    [8] => 7455-0 TRANSITAR EM VELOCIDADE SUPERIOR � M�XIMA P
    [9] => Av. Rui Barbosa, n. 1397-Sentido Av. Gov. Agamenon Magalh�es
    )
    */

    $ponto = array(
        utf8_encode('Infra��o') => $ponto[8],
        utf8_encode('�rg�o') => $ponto[1],
        utf8_encode('Auto') => $ponto[2],
        utf8_encode('Artigo') => $ponto[3],
        utf8_encode('S�rie') => $ponto[4],
        utf8_encode('Data') => $ponto[5],
        utf8_encode('Placa') => $ponto[6],
        utf8_encode('Pontos') => $ponto[7],
        utf8_encode('Local') => $ponto[9],
    );


//    print_r($ponto);
    //echo strip_tags($ponto).'<br><br>';

    $dados_condutor['pontos'][] = $ponto;

}

//print_r($pontos);
//exit;

// -------------------------------------------------------------------------
// Checar se a CNH tem impedimento ou est� apreendida
// -------------------------------------------------------------------------

preg_match('/<span id="lblImpedimento" [^>]*>(.*?)<\/span>/sim', $html_pontos, $impedimento);
$impedimento = $impedimento[1];

if($impedimento) {
    $dados_condutor['pontos'][] = array('Impedimento' => $impedimento);

    if(!$dados_condutor['total_pontos']) {
        $dados_condutor['total_pontos'] += 1;
    }
}

// -------------------------------------------------------------------------
// Checar se a CNH tem autua��es pendentes para pontuar
// -------------------------------------------------------------------------

preg_match('/<span id="lblPreRegistro" [^>]*>(.*?)<\/span>/sim', $html_pontos, $preRegistro);
$preRegistro = $preRegistro[1];

if($preRegistro) {
    $dados_condutor['pontos'][] = array(utf8_encode('Pr�-registro') => $preRegistro);

    if(!$dados_condutor['total_pontos']) {
        $dados_condutor['total_pontos'] += 1;
    }
}

$json = jsonp_encode($dados_condutor);

if(!empty($email))
{
    $db = new db();
    $db->query("call add_log_condutor('PE', '".$email."', '".$cnh."', '', '', '".$dados_condutor['condutor']['CPF']."', ".$dados_condutor['total_pontos'].", '".$json."');");
}

echo $json;