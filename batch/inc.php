<?php
/**
 * Created by PhpStorm.
 * User: Marcelo
 * Date: 14/03/14
 * Time: 00:41
 */

function get_input_value($name, $html)
{
    preg_match('/<input [^>]*name="'.$name.'"[^>]* value="(.*?)" \/>/sim', $html, $input);

    if(!empty($input[1]))
        return $input[1];

    preg_match('/hiddenField\|'.$name.'\|(.*?)\|/sim', $html, $input);

    return $input[1];
}