<?php

include_once "../config.php";
include_once "inc.php";
include_once "../db.php";
include_once "../query.php";

echo "+------------------------------------------------------------------------------+\n";
echo "|              ***** AUTOCHECK - PROCESSAMENTO DA TABELA FIPE *****            |\n";
echo "|------------------------------------------------------------------------------|\n";
echo "|                INICIO DE PROCESSAMENTO : ".date('d/m/Y H:i:s')."                 |\n";
echo "+------------------------------------------------------------------------------+\n";
echo "\n";


$db = new db(DADOS_SERVER, DADOS_USUARIO, DADOS_SENHA, DADOS_FIPE);

// Variaveis usadas para reprocessamento
$cod_referencia     = $argv[2];
$reproc_cod_tipo    = $argv[4];
$reproc_cod_marca   = $argv[6];
$reproc_cod_modelo  = $argv[8];

$erron = "ERRO0000";

$tipo_veiculo = array(
    '1' => 'carro',
    '2' => 'moto',
    '3' => 'caminhao'
);

$tipo2 = array(
    '1' => 'V',
    '2' => 'M',
    '3' => 'C'
);

$meses = array(
    'janeiro'   => '01',
    'fevereiro' => '02',
    'mar�o'     => '03',
    'abril'     => '04',
    'maio'      => '05',
    'junho'     => '06',
    'julho'     => '07',
    'agosto'    => '08',
    'setembro'  => '09',
    'outubro'   => '10',
    'novembro'  => '11',
    'dezembro'  => '12'
);

// Caso n�o receber a refer�ncia como par�metro, buscar a �ltima
if(!$cod_referencia) {

    // BUSCAR C�DIGO DE REFERENCIA
    $url = 'http://www2.fipe.org.br/pt-br/indices/veiculos';

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
    curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
    curl_setproxy($ch, 'fipe');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_NOBODY, 0);

    $html = utf8_decode(curl_exec($ch));

    preg_match('/id="selectTabelaReferenciacarro"(.*?)<\/select>/sim', $html, $cod_referencia);
    $cod_referencia = $cod_referencia[1];

    preg_match('/<option value="(.*?)">/sim', $cod_referencia, $cod_referencia);
    $cod_referencia = $cod_referencia[1];
}

foreach($tipo_veiculo as $cod_tipo=>$tipo)
{
    if(intval($reproc_cod_tipo) and intval($cod_tipo) != intval($reproc_cod_tipo))
        continue;
    else
        $reproc_cod_tipo = 0;

    echo "REF $cod_referencia TIPOVEIC $tipo2[$cod_tipo]\n";

    $url = "http://www2.fipe.org.br/IndicesConsulta-ConsultarMarcas?codigoTabelaReferencia=$cod_referencia&codigoTipoVeiculo=$cod_tipo";

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
    curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
    curl_setproxy($ch, 'fipe');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_NOBODY, 0);

    $marcas = json_decode(curl_exec($ch));

    foreach($marcas as $marca)
    {
        if(intval($reproc_cod_marca) and intval($marca->Value) != intval($reproc_cod_marca))
            continue;
        else
            $reproc_cod_marca = 0;

        echo "REF $cod_referencia TIPOVEIC $tipo2[$cod_tipo] MARCA $marca->Value\n";

        $url = "http://www2.fipe.org.br/IndicesConsulta-ConsultarModelos?codigoTipoVeiculo=$cod_tipo&codigoTabelaReferencia=$cod_referencia&codigoModelo=&codigoMarca=$marca->Value&ano=&codigoTipoCombustivel=&anoModelo=&modeloCodigoExterno=";

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
        curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
        curl_setproxy($ch, 'fipe');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_NOBODY, 0);

        $modelos = json_decode(curl_exec($ch));

        foreach($modelos->Modelos as $modelo)
        {
            if($reproc_cod_modelo and $modelo->Value != $reproc_cod_modelo)
                continue;
            else
                $reproc_cod_modelo = 0;

            echo "REF $cod_referencia TIPOVEIC $tipo2[$cod_tipo] MARCA $marca->Value MODELO $modelo->Value\n";

            $url = "http://www2.fipe.org.br/IndicesConsulta-ConsultarAnoModelo?codigoTipoVeiculo=$cod_tipo&codigoTabelaReferencia=$cod_referencia&codigoModelo=$modelo->Value&codigoMarca=$marca->Value&ano=&codigoTipoCombustivel=&anoModelo=&modeloCodigoExterno=";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
            curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
            curl_setproxy($ch, 'fipe');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            $anos = json_decode(curl_exec($ch));

            foreach($anos as $ano)
            {
                echo $marca->Label . " - " . $modelo->Label . " - " . $ano->Label . ": ";

                $ano_combustivel = explode("-", $ano->Value);

                $anoModelo = $ano_combustivel[0];
                $combustivel = $ano_combustivel[1];

                $url = "http://www2.fipe.org.br/IndicesConsulta-ConsultarValorComTodosParametros?codigoTabelaReferencia=$cod_referencia&codigoMarca=$marca->Value&codigoModelo=$modelo->Value&codigoTipoVeiculo=$cod_tipo&anoModelo=$anoModelo&codigoTipoCombustivel=$combustivel&tipoVeiculo=$tipo&modeloCodigoExterno=&tipoConsulta=tradicional";

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
                curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
                curl_setproxy($ch, 'fipe');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_NOBODY, 0);

                $tabela = utf8_decode(curl_exec($ch));

                $tabela = json_decode(curl_exec($ch));

                // m�s e ano da tabela de referencia
                $mes_ano = preg_split("/ de /sim", trim($tabela->MesReferencia));
                $referencia = $mes_ano[1] . '-' . $meses[trim(utf8_decode($mes_ano[0]))] . '-01';

                if(!$tabela->Valor) {
                    $erro = "VALOR NAO ENCONTRADO NA TABELA DE REFERENCIA";
                    $erron = "ERRO0001";
                }

                if(!$erro) {

                    // formatando o tipo numerico para o sql (R$ 1,99 vira 1.99)
                    $preco_medio =  str_replace('R$ ', '', $tabela->Valor);
                    $preco_medio =  str_replace('.', 'XX', $preco_medio);
                    $preco_medio =  str_replace(',', '.', $preco_medio);
                    $preco_medio =  str_replace('XX', '', $preco_medio);

                    if(intval($anoModelo) == 3200) {
                        $anoModelo = "Zero";
                    }

                    $sql = 'call add_modelo_referencia("'.strtoupper($tipo2[$cod_tipo]).'", "' . trim($marca->Label) . '", "'.trim($modelo->Label).'", "'.trim($anoModelo).'", "'.trim($tabela->CodigoFipe).'", STR_TO_DATE("'.$referencia.'", "%Y-%m-%d"), '.$preco_medio.');';

                    $rs = new query($db, $sql);

                    $erro = $rs->erro();

                    if($erro) {
                        $erro = "ERRO OPERACIONAL NO SGBD:\n\n$erro";
                        $erron = "ERRO0002";
                    }
                }

                if($erro)
                {
                    echo "ERRO\n\n";

                    echo "$erron - $erro\n\n";

                    echo "+------------------------------------------------------------------------------+\n";
                    echo "|            ***** PROCESSAMENTO INTERROMPIDO COM ERRO - $erron *****        |\n";
                    echo "|                            ".date('d/m/Y H:i:s')."                               |\n";
                    echo "+------------------------------------------------------------------------------+\n";


                    exit;
                }

                echo "OK\n";

                //exit;
            }

        }

    }
}


echo "\n";
echo "+------------------------------------------------------------------------------+\n";
echo "|         $erron - FIM NORMAL DE PROCESSAMENTO: ".date('d/m/Y H:i:s')."          |\n";
echo "+------------------------------------------------------------------------------+\n";




