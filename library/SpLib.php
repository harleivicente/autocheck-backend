<?php

require_once "../vendor/autoload.php";
use PHPHtmlParser\Dom;
use PHPHtmlParser\Dom\HtmlNode;

class SpLib {

    /* 
        Inicia a sessão no site do detran sp e obten o cookie
        que deve ser usado para dar continuidade as operações.

        @return string OR FALSE
     */
    static function obterCookie() {
        $result = self::curl_get(DETRAN_HOME, array(), array(
            CURLOPT_HEADER => 1,
            CURLOPT_FOLLOWLOCATION => 0
        ), array());
        $encontrou = preg_match('/Set-Cookie:\s(?<cookie>[^;]+);/i', $result, $matches);
        
        if($encontrou > 0) {
            return $matches['cookie'];
        } else {
            return false;
        }
    }

    /** 
    * Send a POST requst using cURL 
    *
    * @param string $url to request 
    * @param stdClass $post values to send 
    * @param array $options for cURL 
    * @param array $headers
    *
    * @return string 
    */ 
    static function curl_post($url, $post, $options, $headers) 
    { 
        // Converter stdClass para Array associativo
        $post_array = array();
        foreach ($post as $key => $value) {
            $post_array[$key] = $value;
        }

        $defaults = array( 
            CURLOPT_POST => 1, 
            CURLOPT_HEADER => 0, 
            CURLOPT_URL => $url, 
            CURLOPT_FRESH_CONNECT => 1, 
            CURLOPT_RETURNTRANSFER => 1, 
            CURLOPT_FORBID_REUSE => 1, 
            CURLOPT_TIMEOUT => 20, 
            CURLINFO_HEADER_OUT => true,
            CURLOPT_POSTFIELDS => http_build_query($post_array)
        ); 

        $ch = curl_init(); 
        curl_setopt_array($ch, ($options + $defaults)); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch); 

        // DEBUG
        if($result === false)
            trigger_error("Error in curl_post. Error: " . curl_error($ch) . ". Http code: " . curl_getinfo($ch, CURLINFO_HTTP_CODE));

        // Logging
        $log_message = ' - ';
        $log_message .=  'Redirects: ';
        $log_message .= (curl_getinfo($ch,  CURLINFO_REDIRECT_COUNT));
        $log_message .=  ' - ';
        $log_message .=  'Outgoing headers:';
        $log_message .= (curl_getinfo($ch, CURLINFO_HEADER_OUT));
        $log_message .=  ' - ';
        $log_message .=  'Http code: ';
        $log_message .= (curl_getinfo($ch, CURLINFO_HTTP_CODE ));
        $log_message .=  ' - ';
        $log_message .=  ' - ';
        error_log($log_message);

        curl_close($ch); 
        return $result; 
    } 

    /** 
    * Send a GET requst using cURL 

    * @param string $url to request 
    * @param array $get values to send 
    * @param array $options for cURL 
    * @param array $headers
    *
    * @return string 
    */ 
    static function curl_get($url, $get, $options, $headers) 
    {    
        $defaults = array( 
            CURLOPT_URL => $url. (strpos($url, '?') === FALSE ? '?' : ''). http_build_query($get), 
            CURLOPT_HEADER => 0, 
            CURLOPT_RETURNTRANSFER => 1, 
            CURLINFO_HEADER_OUT => true,
            CURLOPT_TIMEOUT => 20 
        ); 

        $ch = curl_init(); 
        curl_setopt_array($ch, ($options + $defaults));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch); 
        
        // DEBUG
        if($result === false)
            trigger_error("Error in curl_get. Error: " . curl_error($ch) . ". Http code: " . curl_getinfo($ch, CURLINFO_HTTP_CODE));
        
        // Logging
        $log_message = ' - ';
        $log_message .=  'Redirects: ';
        $log_message .= (curl_getinfo($ch,  CURLINFO_REDIRECT_COUNT));
        $log_message .=  ' - ';
        $log_message .=  'Outgoing headers:';
        $log_message .= (curl_getinfo($ch, CURLINFO_HEADER_OUT));
        $log_message .=  ' - ';
        $log_message .=  'Http code: ';
        $log_message .= (curl_getinfo($ch, CURLINFO_HTTP_CODE ));
        $log_message .=  ' - ';
        $log_message .=  ' - ';
        error_log($log_message);

        curl_close($ch); 
        return $result; 
    }


    /* 
        Preenche os valores das variaveis post para envio
        em requisicao POST.

        @param $postVars stdClass (
            'Z7_afd343aaff:nome' => '', ...
        )
        @param $valores stdClass (
            'nome' => 'valor'
        )

        @return stdClass(
            'Z7_afd343aaff:nome' => 'harlei',
            'Z7_afd343aaff:cpf' => '123456'
        )
    */
    static function preencherVariaveisPost ($variaveisPost, $valores) {
        foreach ($variaveisPost as $nomeVarPost => $postValor) {
            foreach ($valores as $valorNome => $valor) {
                $padrao_regex = "/:" . $valorNome . "$/i";
                if(preg_match($padrao_regex, $nomeVarPost)){
                    $variaveisPost->$nomeVarPost = $valor;
                }
            }
        }
        return $variaveisPost;
    }

    /* 
        Extrai o formato dos dados enviados por uma tag <form>.

        @param $dom PHPHtmlParser\Dom\HtmlNode - HtmlNode do form. O form deve ser
        a raiz da estrutura. Obtido com: $dom->find('form')

        @return FALSE | stdClass (
            'action' => string  // O nome da ação para onde form deve ser enivado.
            'post_vars' => stdClass (
                'Z7_0021_adfefd:cpf' => '', ...
            )
        )
    */
    static function extrairFormatoDeForm(HtmlNode $form = null) {
        if ($form->tag->name() != "form") {
            return false;
        } else {
            $out = new stdClass();

            // Obter o action do form
            $action = $form->getAttribute('action');
            if(!$action) {
                return false;
            }
            $action = preg_replace('/#[[:alnum:]_]+$/i', '', $action);
            $out->action = 'http://www.detran.sp.gov.br' . $action;

            // Obter os campos para o formulário
            $input_hidden_list = $form->find('input');
            if(!$input_hidden_list) {
                return false;
            }
            $post_vars = new stdClass();
            foreach ($input_hidden_list as $key => $input_hidden) {
                $attr_name = $input_hidden->getAttribute('name');
                $post_vars->$attr_name = $input_hidden->getAttribute('value');
            }
            $out->post_vars = $post_vars;   
            return $out;
        }
    }

    /* 
        Gera um ip aleatorio padrao.
    */
    static function randomIp() {
        return "".mt_rand(0,255).".".mt_rand(0,255).".".mt_rand(0,255).".".mt_rand(0,255);
    }


    /* 
        Baixa a imagem captcha para o servidor.

        @cookie - Cookie utilizado durante acesso de pagina que continha
        captcha.

        @url - Image uri
    */
    static function baixarCaptcha ($cookie, $url) {
        $result = self::curl_get($url, array(), array(
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_BINARYTRANSFER => 1
        ), array(
            "Accept: */*",
            self::makeCookieHeader($cookie),
            "User-Agent: Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36",
            "Accept-Language: pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4"
        ), true);

        $nome_arquivo = "captcha_" . mt_rand(0,100000) . ".png";
        $saveto = CAPTCHA_SERVER_FOLDER . DIRECTORY_SEPARATOR . $nome_arquivo;
        if(file_exists($saveto)){
            unlink($saveto);
        }
        $fp = fopen($saveto,'w');
        fwrite($fp, $result);
        fclose($fp);      
        return CAPTCHA_URI . '/' . $nome_arquivo; 
    }        


    /* 
        Gera um header http para o cookie:

            Cookie: detra....; JSESSONID=000.....

        @param cookie string - exemplo: JSESSIONID=00013ASDFA43334df
    */
    static function makeCookieHeader ($cookie) {
        return 'Cookie:	detransp_cookie=true; ' . $cookie . ';';
    }    
    
}

