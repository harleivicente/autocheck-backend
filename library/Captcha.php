<?php

require_once "../library/SpLib.php";

/* 
    @return string | false
        string - code for captcha or false if fail

    Script falha após 60 segundos.
*/
function captcha_service ($target_url, $googlekey) {
    $start = time();
    $url = "http://2captcha.com/in.php";
    $get_params = array(
        "key" => "b7b95a0191428a2dea0e76f4a31dbc35",
        "method" => "userrecaptcha",
        "googlekey" => $googlekey,
        "pageurl" => $target_url,
        "json" => 1,
        "proxy" => 'http://autocheck_server:df4$gfkg@D3FJGd3@181.41.220.208:8888',
        "proxytype" => "HTTP"
    );

    // var_dump($get_params);

    // {"status":0,"request":"ERROR_WRONG_USER_KEY"}
    // {"status":1,"request":"182506032"}
    $captcha_upload_res = SpLib::curl_get($url, $get_params, array(), array());
    $captcha_upload = $captcha_upload_res ? json_decode($captcha_upload_res) : null;
    // var_dump($captcha_upload_res);
    
    if($captcha_upload && $captcha_upload->status) {
        $token_id = $captcha_upload->request;
        $response = true;
        $timeout = 16;
        
        while($response === true) {

            // Give up after 60 seconds
            if(time() - $start > 90) {
                return false;
            }

            sleep($timeout);
            $timeout = 4;
            $response = check_for_response($token_id);
            
            // var_dump($response);
    
            if($response === false) {
                // echo json_encode(array('erro'=> 'Captcha break failed.', 'id_erro' => 4));
                return false;
            } elseif ($response !== true) {
                return $response;
                // echo json_encode(array('erro' => '', 'id_erro' => 0, 'code' => $response));
            }
        }
    
    } else {
        return false;
        // echo json_encode(array('erro' => 'Failed to upload captcha', 'id_erro' => 3));
    }

}

/* 
    @param $id token id
    @return mixed
        - false : Permanent error
        - true : not ready yet
        - string : token
*/
function check_for_response($id) {
    $url = "http://2captcha.com/res.php";
    $get_params = array(
        "key" => "b7b95a0191428a2dea0e76f4a31dbc35",
        "action" => "get",
        "id" => $id,
        "json" => 1
    );
    $response = SpLib::curl_get($url, $get_params, array(), array());

    // echo '->' . $response;
    
    $valid_response = $response ? json_decode($response) : false;
    if($valid_response) {

        if($valid_response->request === "CAPCHA_NOT_READY") {
            return true;
        } elseif (strstr($valid_response->request, "ERROR_")) {
            return false;
        } else {
            return $valid_response->request;
        }

    } else {
        return false;
    }

}
