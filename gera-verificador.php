<?php

include_once 'config.php';
include_once "db.php";
include_once "query.php";
include_once "inc.php";
require 'PHPMailer/PHPMailerAutoload.php';

header('content-type: application/json; charset=utf-8');

$email = addslashes(trim($_POST['email']));
$nome  = addslashes(utf8_decode(trim($_POST['nome'])));

if(!$email or !$nome)
{
    $json = array("erro" => "Um ou mais parametros invalidos", 'id_erro' => '1');
    echo json_encode($json);
    exit;
}

// Inicializando variaveis de status
$json =  array(); // saida do json
$json['erro'] = 'nao';
$json['id_erro'] = '0';


$cod_verificacao = gera_verificador();

$db = new db();

$sql = "call cad_verificacao('" . $email . "', '" . $nome . "', ".$cod_verificacao.")";

$rs = new query($db, $sql);

if($rs->erro()) {
    echo $rs->erro();
    exit;
}

$rs = $rs->fetch();

if($rs['resposta'] != 'sucesso')
    exit;

/*
enviaEmail(
    'autocheck@marceleira.com',
    'Autocheck',
    $email,
    'C�digo de verifica��o',
    formata_email_verificacao($nome, $cod_verificacao)
);*/

$mail = new PHPMailer;

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.autoweb.com.br';  // Specify main and backup server
$mail->Port = 587;  // Specify main and backup server
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'noreply@autoweb.com.br';         // SMTP username
$mail->Password = 'autocheck1234';                           // SMTP password
//$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted

$mail->From = 'noreply@autoweb.com.br';
$mail->FromName = 'Autocheck Detran';

$mail->addAddress($email, $nome);  // Add a recipient

$mail->addEmbeddedImage("logo-app.png", "logo-app", dirname(__FILE__).DS."logo-app.png");

$mail->WordWrap = 50;                                 // Set word wrap to 50 characters

$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'C�digo de verifica��o';
$mail->Body    = formata_email_verificacao($nome, $cod_verificacao);
$mail->AltBody  =  formata_email_verificacao_plain($nome, $cod_verificacao);
$mail->IsHTML(true);


$what = $mail->send();

echo json_encode($json);
