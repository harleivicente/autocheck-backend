<?php
require_once "../library/SpLib.php";
require_once "../vendor/autoload.php";
require_once '../config.php';
require_once '../inc.php';
require_once "../db.php";
require_once "../query.php";

use PHPHtmlParser\Dom;
use PHPHtmlParser\Dom\HtmlNode;

session_start();

/* 
    Retorna erro no padrao do sistema ou uma imagem com o captcha.
*/
    
$url = "http://www.servicos.detran.ba.gov.br/pages/consultaveiculo/consultaveiculoindex.xhtml";

$home = SpLib::curl_get($url, array(), array(
    CURLOPT_HEADER => 1,
    CURLOPT_FOLLOWLOCATION => 0      
), array());

if(!$home) {
    echo json_encode(array('erro' => 'Detran BA indisponvível...', 'id_erro' => 4));
    exit;
}

// Obter os cookies JSESSIONID e Path
$session_regex = "/JSESSIONID=([^;]+);/i";
preg_match($session_regex, $home, $session);
if(!$session[1]) {
    echo json_encode(array('erro' => 'Detran BA indisponvível...', 'id_erro' => 4));
    exit;
} else {
    $session = $session[1];
    $_SESSION['jsession'] = $session;
} 

// Obter view state do form
$dom = new Dom;
if(!$dom->load($home)){
    echo json_encode(array ('erro' => 'Erro interno. Tente novamente mais tarde.', 'id_erro' => '3', 'msg' => 'Failed to parse DOM.'));
    exit;    
}
$input = $dom->find("input[name='javax.faces.ViewState']");
if(sizeof($input) < 2) {
    echo json_encode(array ('erro' => 'Erro interno. Tente novamente mais tarde.', 'id_erro' => '3', 'msg' => 'Cant find viewstate from form.'));
    exit;        
}
$_SESSION['viewstate'] = $input[1]->__get('value');

// Get id of 'Pesquisar' button
$buttons = $dom->find("#divButton button");
if(sizeof($buttons) < 2) {
    trigger_error("Cant find id of pesquisar button.");
}
$_SESSION['pesquisar_id'] = $buttons[1]->__get('id');
