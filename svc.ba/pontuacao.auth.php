<?php
/**
 * Created by PhpStorm.
 * User: Marcelo
 * Date: 02/03/14
 * Time: 19:43
 */

include '../config.php';
include '../inc.php';

session_start();

/* buscar cookies do google  */

$url = "http://www.google.com/";

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setproxy($ch, 'ba');
curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_COOKIEJAR, getcwd().DIRECTORY_SEPARATOR.'cookie.txt');
curl_setopt($ch, CURLOPT_COOKIEFILE, getcwd().DIRECTORY_SEPARATOR.'cookie.txt');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HEADER, 1);
curl_setopt($ch, CURLOPT_NOBODY, 1);

$html = curl_exec($ch);

/*-------------------------------------------------------------------------------------------*/

$url = "http://www.servicos.detran.ba.gov.br/pages/ConsultaPontuacao/consultarPontuacaoIndex.xhtml";

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_REFERER, 'http://www.servicos.detran.ba.gov.br/');
curl_setproxy($ch, 'ba');

curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HEADER, 1);
curl_setopt($ch, CURLOPT_NOBODY, 0);

$html = curl_exec($ch);
//echo $html;

preg_match('/challenge\?k=(.*?)">/sim', $html, $challenge_key);


$url = "http://www.google.com/recaptcha/api/challenge?k=".$challenge_key[1];

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_REFERER, 'http://www.servicos.detran.ba.gov.br/pages/ConsultaPontuacao/consultarPontuacaoIndex.xhtml');
curl_setproxy($ch, 'ba');

curl_setopt($ch, CURLOPT_COOKIEJAR, getcwd().DIRECTORY_SEPARATOR.'cookie.txt');
curl_setopt($ch, CURLOPT_COOKIEFILE, getcwd().DIRECTORY_SEPARATOR.'cookie.txt');

curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HEADER, 1);
curl_setopt($ch, CURLOPT_NOBODY, 0);

$html = curl_exec($ch);
//echo $html;
//exit;

preg_match("/challenge : '(.*?)'/sim", $html, $challenge);
//echo $challenge[1];

$_SESSION['challenge'] = $challenge[1]; // usado no POST do formulário

$url ="http://www.google.com/recaptcha/api/image?c=" . $challenge[1];

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_COOKIEJAR, getcwd().DIRECTORY_SEPARATOR.'cookie.txt');
curl_setopt($ch, CURLOPT_COOKIEFILE, getcwd().DIRECTORY_SEPARATOR.'cookie.txt');

curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_REFERER, 'http://www.servicos.detran.ba.gov.br/pages/ConsultaPontuacao/consultarPontuacaoIndex.xhtml');

curl_setproxy($ch, 'ba');

curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_NOBODY, 0);

//alteracao feita pela empresa - ISSO NUNCA PODE IR PARA PRODUCAO
//enable_cors();

header('Content-type: image/jpeg');

echo curl_exec($ch);