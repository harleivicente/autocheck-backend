<?php
/**
* Created by PhpStorm.
* User: Marcelo
* Date: 02/03/14
* Time: 19:33
*/

session_start();
error_reporting(0);

include_once "../config.php";
include_once "../inc.php";
include "../db.php";
include "../query.php";
include_once "../library/Captcha.php";

header('content-type: application/json; charset=utf-8');

$captcha = trim($_POST['captcha']);
$renavan = trim($_POST['renavan']);
$email = trim($_POST['email']);

if(!$captcha or !$renavan)
{
    $json = array("erro" => "Um ou mais parametros invalidos", 'id_erro' => '1');
    echo jsonp_encode($json);
    exit;
}


/* obter sessao */
$resultado = curl_get("http://www.servicos.detran.ba.gov.br/", array(), array(
    CURLOPT_HEADER => 1,
    CURLOPT_FOLLOWLOCATION => 0    
), array());
$regex = "/Set-Cookie:([^;]+);/";
preg_match($regex, $resultado, $matches);

if(!$resultado[1]) {
    echo json_encode(array("erro" => "Site do detran fora do ar.", 'id_erro' => '14'));
    echo $resultado;
    exit;
} else {
    $session_id = $matches[1];
}

// Inicializando variaveis
$dados_veiculo =  array(); // saida do json

$dados_veiculo['erro'] = 'nao';
$dados_veiculo['id_erro'] = '0';

$dados_veiculo['placa'] = null;
$dados_veiculo['chassi'] = null;
$dados_veiculo['renavam'] = null;

$dados_veiculo['veiculo'] = array();
$dados_veiculo['imposto'] = array();

$dados_veiculo['infracao']['multas'] = array();
$dados_veiculo['infracao']['autuacoes'] = array();


$url = "http://www.servicos.detran.ba.gov.br/pages/consultaveiculo/consultaveiculoindex.xhtml";




/* OBTER CODIGO DE CAPTCHA */
$captcha_result = captcha_service('http://www.servicos.detran.ba.gov.br', '6LdUTx0TAAAAAAYFbJNak8u3nfJfW4e4xtvOOWeu');
if(!$captcha_result) {
    echo json_encode(array("erro" => "Não foi possível resolver o captcha.", 'id_erro' => '13'));
    exit;
}
/* OBTER CODIGO DE CAPTCHA */

// $dados_post = array(
    //     'veiculoform' => 'veiculoform',
    //     'searchParamType' => 'consulta_renavam',
    //     'searchParam' => $renavan,
    //     'javax.faces.ViewState' => $_SESSION['viewstate'],
    //     'recaptcha_challenge_field' => $_SESSION['challenge'],
    //     'recaptcha_response_field' => $captcha
    // );
    
    $dados_post = array(
        'veiculoform' => 'veiculoform',
        'searchParamType' => 'consulta_renavam',
        'searchParam' => $renavan,
        'g-recaptcha-response' => $captcha_result,
        'j_idt59'=> '',
        'javax.faces.ViewState' => '3729191099985814754:-5846559637520937695'
    );
    $html = curl_post($url, (object) $dados_post, array(
        CURLOPT_FOLLOWLOCATION => 1,
        CURLOPT_HEADER => 1
    ), array(
        'Cookie:	' . $session_id . ';',
        'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding: gzip, deflate',
        'Accept-Language: pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4',
        'User-Agent: Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',
        'Origin: http://www.servicos.detran.ba.gov.br',
        'Referer: http://www.servicos.detran.ba.gov.br/pages/consultaveiculo/consultaveiculoindex.xhtml'
    ));
    
    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_URL, $url);
    // curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
    // curl_setopt($ch, CURLOPT_REFERER, 'http://www.servicos.detran.ba.gov.br/pages/consultaveiculo/consultaveiculoindex.xhtml');
    // // curl_setproxy($ch, 'ba');
    
    // // curl_setopt($ch, CURLOPT_HTTPHEADER, array("Origin: http://www.servicos.detran.ba.gov.br", "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7", "Cookie: JSESSIONID=FAfiWwclQNxhR36SRvySuJb4AtmqePJES9yA1-pO.webwdetran2"));
    // curl_setopt($ch, CURLOPT_HTTPHEADER, array("Cookie: " + $session_id + ";"));
    // curl_setopt($ch, CURLOPT_POST, true);
    // curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($dados_post));
    
    // curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
    // curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    // curl_setopt($ch, CURLOPT_HEADER, 1);
    // curl_setopt($ch, CURLOPT_NOBODY, 0);
    
    // $html = curl_exec($ch);
    
    if(!$html) // estourou o timeout, erro no servidor do detran
    {
        $json = array("erro" => utf8_encode("Servidor do Detran-BA temporariamente indispon�vel... :("), 'id_erro' => '1');
        echo jsonp_encode($json);
        exit;
    }
    
    echo $html;
    exit;
    
    
    $html = file_get_contents('result_html_test.html'); // Arquivo HTML para testes. Tamb�m serve como "BACKUP" (Compara��o Vers�o Anterior) caso o Detran atualize o Layout.
    
    // -------------------------------------------------------------------------
    // MENU 1: Ve�culo ( Buscar informa��es Ve�culo )
    // -------------------------------------------------------------------------
    
    function dadosVeiculo( $html, $v ) {
        
        //Funcionar com $html teste => file_get_contents
        $pattern = '/'.utf8_encode($v).':<\/td>\s*<td><label id="\w+" class="[^"]*?">(.*)/';
        
        // Funcionar com $html em produ��o => CURL
        //$pattern = '/'.$v.':<\/td>\s*<td><label id="\w+" class="[^"]*?">(.*)/';
        
        preg_match($pattern, $html, $res);
        
        return trim( strip_tags( $res[1] ) ); //Remove espa�os em branco e codigos html
    }
    
    $dadosVeiculo = array (
        '0'  => 'Placa',
        '1'  => 'Tipo',
        '2'  => 'Categoria',
        '3'  => 'Cor',
        '4'  => 'RENAVAM',
        '5'  => 'Proced�ncia',
        '6'  => 'Esp�cie',
        '7'  => 'Combust�vel',
        '8'  => 'Chassi',
        '9'  => 'Marca\/Modelo',
        '10' => 'Ano Modelo',
        '11' => 'Ano Fabrica��o'
    );
    
    $veiculo = array();
    
    foreach ($dadosVeiculo as $k => $v) {
        
        $veiculo[$k] = dadosVeiculo( $html, $v );
        
        $dados_veiculo['veiculo'][utf8_encode($v)] = $veiculo[$k];
    }
    
    $renavam = str_replace('.','', str_replace('-', '', $dados_veiculo['veiculo']['RENAVAM'] ) );
    $placa   = str_replace('-', '', $dados_veiculo['veiculo']['Placa'] );
    
    $dados_veiculo['renavam'] = $renavam;
    $dados_veiculo['chassi']  = $dados_veiculo['veiculo']['Chassi'];
    $dados_veiculo['placa']   = $placa;
    
    $m = utf8_encode('Marca\/Modelo');
    $marca_modelo = array_map( 'trim', explode( '/', $dados_veiculo['veiculo'][$m] ) );
    
    $dados_veiculo['veiculo']['Marca']  = $marca_modelo[0];
    $dados_veiculo['veiculo']['Modelo'] = $marca_modelo[1];
    
    unset( $dados_veiculo['veiculo'][$m] );
    
    // echo '<pre>';
    // echo var_dump($dados_veiculo['veiculo']);
    // echo jsonp_encode($dados_veiculo['veiculo']);
    // echo '</pre>';
    // exit;
    
    
    // -------------------------------------------------------------------------------------
    // MENU 2: Impostos ( Buscar dados: IPVA | LICENCIAMENTO | DPVAT )
    // -------------------------------------------------------------------------------------
    
    function debitosImpostos( $html, $imposto ) {
        
        if($imposto == 'ipva')
        $imposto = 'IPVA';
        
        if($imposto == 'licenciamento')
        $imposto = 'Licen';
        
        if($imposto == 'seguro obrigatorio')
        $imposto = 'Seguro';
        
        $pattern = '/<td colspan="2">('.$imposto.'.*)<\/td>\s*<td(?:| class="right")>(.*)<\/td>\s*<td(?:| class="right")>(.*)<\/td>\s*<td(?:| class="right")>(.*)<\/td>\s*<td(?:| class="right")>(.*)<\/td>/i';
        
        preg_match_all($pattern, $html, $debitos, PREG_SET_ORDER, 0);
        
        for ($i=0; $i < count($debitos); $i++) 
        unset( $debitos[$i][0] ); //Percorre todo array e Exclui chaves [0]
        
        return $debitos;
    }
    // $debitosIPVA = debitosImpostos( $html, 'ipva' );
    // $debitosLICE = debitosImpostos( $html, 'licenciamento' );
    // $debitosSEGU = debitosImpostos( $html, 'seguro obrigatorio' );
    // echo '<pre>';
    // var_dump($debitosIPVA); 
    // var_dump($debitosLICE); 
    // var_dump($debitosSEGU); 
    // echo '</pre>';
    // exit;
    
    $impostos = array(
        'ipva', 
        'licenciamento', 
        'seguro obrigatorio'
    );
    
    $total                                                   = 0; 
    $dados_veiculo['imposto']['IPVA']['TOTAL']               = 0;
    $dados_veiculo['imposto']['LICENCIAMENTO']['TOTAL']      = 0;
    $dados_veiculo['imposto']['SEGURO OBRIGATORIO']['TOTAL'] = 0;
    
    foreach ($impostos as $imposto) {
        
        $i   = strtoupper($imposto);
        
        $debitos = debitosImpostos( $html, $imposto ); //$imp
        
        foreach ($debitos as $d)
        {
            $dados_veiculo['imposto'][$i][trim( utf8_encode( $d[1] ) )] = trim( $d[5] );
            
            $valor = str_replace('.', '', $d[5]);
            $valor = str_replace(',', '.', $valor);
            
            //VALOR TOTAL de cada imposto
            $total += $d[5];
        }
        
        $dados_veiculo['imposto'][$i]['TOTAL'] = number_format($total, 2, ',', '.');
        
        $total = 0; //zera total para calcular o proximo imposto
    }
    
    // echo jsonp_encode($dados_veiculo['imposto']);
    // exit;
    
    
    // -------------------------------------------------------------------------
    // MENU 3: Multas ( Buscar dados infra��es: MULTAS e NOTIFICA��ES )
    // -------------------------------------------------------------------------
    
    function debitosInfracoes( $html ) {
        
        $pattern = '/<td role="[^"]*?" class="[^"]*?"><label id="[^"]*?" class="[^"]*?">(.*?)<\/label><\/td><td role="[^"]*?" class="[^"]*?"><label id="[^"]*?" class="[^"]*?">(.*?)<\/label><\/td><td role="[^"]*?" class="[^"]*?"><label id="[^"]*?" class="[^"]*?">(.*?)<\/label><\/td><td role="[^"]*?" class="[^"]*?"><label id="[^"]*?" class="[^"]*?">(\d{2}\/\d{2}\/\d{4})<\/label><\/td><td role="[^"]*?" class="[^"]*?"><label id="[^"]*?" class="[^"]*?">(\w{2})<\/label><\/td><td role="[^"]*?" class="[^"]*?"><label id="[^"]*?" class="[^"]*?">(\d+.\d{2})<\/label><\/td>/';
        
        preg_match_all($pattern, $html, $debitos, PREG_SET_ORDER, 0);
        
        for ($i=0; $i < count($debitos); $i++) 
        unset( $debitos[$i][0] ); //Percorre todo array e Exclui chaves [0]
        
        return $debitos;
    }
    
    $todos_status = array(
        "AA" => "AUTUA��O/A.I.T.",
        "AC" => "AUTUA��O/CORREIO",
        "AE" => "AUTUA��O/EDITAL",
        "AI" => "AUTUA��O IMPRESSA",
        "AN" => "AUTUA��O/N�O ENCONTRADO",
        "AP" => "ATIVA��O DOS PONTOS NO RENACH",
        "AR" => "AUTUA��O/AR",
        "CA" => "CADASTRADA",
        "CC" => "CANCELA REGISTRO DE INFRA��O",
        "CI" => "CANCELAMENTO DE INFRA��O",
        "CM" => "CANCELAMENTO MULTA COM RECURSO",
        "CP" => "CANCELA PONTOS NO RENACH",
        "DD" => "DEF PR�VIA PARECER DEFERIDO",
        "DI" => "DEF. PR�VIA PARECER INDEFERIDO",
        "DP" => "DEFESA PR�VIA",
        "DT" => "DEFESA DE AUTUA��O EM TR�NSITO",
        "DX" => "DEFESA AUTUA��O INTEMPESTIVA",
        "EI" => "ESTORNO IDENTIFICA CONDUTOR",
        "EP" => "ESTORNO DE PAGAMENTO",
        "ER" => "COND/PROP ENVIADO AO RENACH",
        "ES" => "EFEITO SUSPENSIVO",
        "IC" => "IDENTIFI��O CONDUTOR",
        "LF" => "LOTE FECHADO",
        "MC" => "MULTA CANCELADA SEM DEVOLU��O",
        "MD" => "MULTA CANCELADA COM DEVOLU��O",
        "MO" => "MULTA CANCELADA POR ORDEM",
        "MP" => "MULTA PAGA",
        "MR" => "MULTA PAGA EM RECURSO",
        "OC" => "OCORR�NCIA CANCELADA",
        "PA" => "PENALIDADE/AR",
        "PC" => "PAGAMENTO COMPROVADO GECOF",
        "PE" => "PENALIDADE/EDITAL",
        "PI" => "PENALIDADE IMPRESSA",
        "PN" => "PENALIDADE/N�O ENCONTRADO",
        "PR" => "PENALIDADE REATIVADA",
        "PS" => "PENALIDADE/SUSPENSA",
        "RC" => "RECURSO CETRAN EM JULGAMENTO",
        "RD" => "RECURSO CETRAN DEFERIDO",
        "RF" => "REC. CETRAN DEFERIDO P FISICA",
        "RI" => "RECURSO CETRAN INDEFERIDO",
        "RJ" => "RECURSO/JULGAMENTO",
        "RM" => "REATIVACAO DA MULTA",
        "RN" => "RECURSO PARECER N�O PROVIMENTO",
        "RP" => "RECURSO COM PROVIMENTO",
        "RS" => "RECURSO SEM PROVIMENTO",
        "RT" => "RECURSO JARI EM TR�NSITO",
        "SM" => "SUSPENS�O DA MULTA",
        "TC" => "TAXA COBRAN�A ESTACIONAMENTO P�TIO DETRAN"
    );
    
    $autuacoes_status = array(
        "AA",
        "AC",
        "AE",
        "AI",
        "AN",
        "AR"
    );
    
    $debitos = debitosInfracoes( $html );
    
    // echo '<pre>';
    // var_dump($debitos);
    // echo '</pre>';
    // exit;
    
    foreach ($debitos as $debito) {
        
        $Status = strip_tags( trim( $debito[5] ) );
        
        $detalhes = array(
            utf8_encode('Infra��o')      => strip_tags( trim( $debito[2] ) ),
            utf8_encode('Org�o Autuador') => strip_tags( trim( $debito[1] ) ),
            utf8_encode('Local')          => strip_tags( trim( $debito[3] ) ),
            utf8_encode('Data')           => strip_tags( trim( $debito[4] ) ),
            utf8_encode('Status')         => $todos_status[ $Status ],
            utf8_encode('Valor')          => strip_tags( trim( $debito[6] ) )
        );      
        
        $orgao = strip_tags( trim( $debito[1] ) );
        
        if( in_array($Status, $autuacoes_status) )
        $dados_veiculo['infracao']['autuacoes'][$orgao][] = $detalhes;
        else
        $dados_veiculo['infracao']['multas'][$orgao][] = $detalhes;
    }
    
    // echo '<pre>';
    // var_dump($dados_veiculo['infracao']);
    // echo '</pre>';
    // //echo jsonp_encode($dados_veiculo['infracao']);
    // exit;
    
    $json = jsonp_encode($dados_veiculo);
    
    if(!empty($email))
    {
        $db = new db();
        $db->query("call add_log_veiculo ('BA', '".$email."', '".$placa."', '".$dados_veiculo['chassi']."', '".$renavan."', '', '".$json."');");
    }
    
    echo $json;