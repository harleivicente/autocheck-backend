<?php
/**
 * Created by PhpStorm.
 * User: Marcelo
 * Date: 02/03/14
 * Time: 19:33
 */

session_start();

include_once "../config.php";
include_once "../inc.php";
include "../db.php";
include "../query.php";

header('content-type: application/json; charset=utf-8');

$captcha = trim($_POST['captcha']);
$cnh = trim($_POST['cnh']);
$email = trim($_POST['email']);

if(!$captcha or !$cnh)
{
    $json = array("erro" => "Um ou mais parametros invalidos", 'id_erro' => '1');
    echo jsonp_encode($json);
    exit;
}

// Inicializando variaveis
$dados_condutor =  array(); // saida do json

$dados_condutor['erro'] = 'nao';

$dados_condutor['id_erro'] = '0';

$dados_condutor['condutor'] = array(); // ficha do condutor
$dados_condutor['total_pontos'] = 0;
$dados_condutor['pontos'] = array();

$dados_condutor['cpf'] = null;
$dados_condutor['cnh'] = $cnh;

$url = "http://detran.ba.gov.br/situacao-da-cnh?p_p_id=pontuacaoPortlet_WAR_PontuacaoPortlet_INSTANCE_h1W4&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=obterPontuacaoAjax&p_p_cacheability=cacheLevelPage&p_p_col_id=column-2&p_p_col_pos=2&p_p_col_count=3";

$ch = curl_init();

$dados_post = array(
    'searchParam' => $cnh,
    'searchParamType' => 'consulta_cnh',
    'recaptcha_challenge_field' => $_SESSION['challenge'],
    'recaptcha_response_field' => $captcha
);

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_REFERER, 'http://www.detran.ba.gov.br/web/guest/situacao-da-cnh');
curl_setproxy($ch, 'ba');

curl_setopt($ch, CURLOPT_HTTPHEADER, array("Origin: http://www.detran.ba.gov.br"));
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($dados_post));

curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_NOBODY, 0);

$html = curl_exec($ch);

if(!$html) // estourou o timeout, erro no servidor do detran
{
    $json = array("erro" => utf8_encode("Servidor do Detran-BA temporariamente indispon�vel... :("), 'id_erro' => '1');
    echo jsonp_encode($json);
    exit;
}

if(preg_match('/sem pontos/sim', $html))
{
    echo jsonp_encode( array(
        'total_pontos' => 0,
        "erro" => utf8_encode("N�o consta pontuacao para esse condutor"),
        'id_erro' => 0
    ) );
    exit;
}

preg_match_all('/<span class="portlet-msg-[^>]*>(.*?)<\/span>/sim', $html, $erro);

$erro = $erro[1];

$erro[1] = str_replace("\r", '', $erro[1]);
$erro[1] = str_replace("\n", '', $erro[1]);
$erro[1] = str_replace("\t", '', $erro[1]);
$erro[1] = trim($erro[1]);

$erro[0] = str_replace("\r", '', $erro[0]);
$erro[0] = str_replace("\n", '', $erro[0]);
$erro[0] = str_replace("\t", '', $erro[0]);
$erro[0] = trim($erro[0]);

$erro = !empty($erro[1]) ? $erro[1] : $erro[0];

if(!empty($erro))
{
    $json = array("erro" => $erro, 'id_erro' => '2');
    echo jsonp_encode($json);
    exit;
}

//echo $html;
//exit;

preg_match('/<table class="tab100">(.*?)<\/table>/sim', $html, $tb);

preg_match_all('/<td[^>]*>(.*?)<\/td>/sim', $tb[1], $tb);
$tb = $tb[1];

//print_r($tb);
//exit;

$dados_condutor['condutor'] = array(
    'Nome' => $tb[9],
    'CNH' => $tb[1],
    'Validade' => $tb[3],
    'Data Nascimento' => $tb[5],
    utf8_encode('Nome da M�e') => $tb[11],
    'RG' => $tb[7]
);


preg_match_all('/<tbody>(.*?)<\/tbody>/sim', $html, $tb);
$tb = $tb[1][1];

preg_match_all('/<tr[^>]*>(.*?)<\/tr>/sim', $tb, $tb);

foreach($tb[1] as $row)
{
    preg_match_all('/<td[^>]*>(.*?)<\/td>/sim', $row, $campos);
    $campos = $campos[1];

    foreach($campos as $key=>$val)
    {
        $val = str_replace("\r", '', $val);
        $val = str_replace("\n", '', $val);
        $val = str_replace("\t", '', $val);
        $val = trim($val);

        $campos[$key] = $val;
    }

    $dataPonto = DateTime::createFromFormat('d/m/Y', $campos[0])->getTimestamp();

    if( ( time() - $dataPonto ) <  31536000) // se o multa tiver ocorrido a mais de um ano da data atual
    {

        $dados_condutor['pontos'][] = array(
            utf8_encode('Infra��o') => $campos[2],
            utf8_encode('AIT') => $campos[1],
            utf8_encode('Data') => $campos[0],
            utf8_encode('�rg�o') => $campos[3],
            utf8_encode('Pontos') => $campos[4],
        );

        $dados_condutor['total_pontos'] += intval($campos[4]);
    }

}

$json = jsonp_encode($dados_condutor);

if(!empty($email))
{
    $db = new db();
    $db->query("call add_log_condutor('BA', '".$email."', '".$cnh."', '', '', '', ".$dados_condutor['total_pontos'].", '".$json."');");
}

echo $json;
