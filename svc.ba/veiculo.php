<?php
    require_once "../library/SpLib.php";
    require_once "../library/Captcha.php";
    require_once "../vendor/autoload.php";
    require_once '../config.php';
    require_once '../inc.php';
    require_once "../db.php";
    require_once "../query.php";

    use PHPHtmlParser\Dom;
    use PHPHtmlParser\Dom\HtmlNode;

    include_once "veiculo.auth.php";

    // session_start();
    
    /* 
        @param renavan

        @return array(
            'erro' => '',
            'id_erro' => ERROR CODE,
            'total_pontos' => 343,
            'pontos' => array (
                'AIT' => '',
                'Orgao Autuador' => '',
                'Placa' => '',
                'Local' => '',
                'Data' => '',
                'Hora' => '',
                'Pontos' => ''
            )[],
            'condutor' => array(
                'Nome' => '',
                'CPF' => '',
                'CNH' => ''
            ),
            'cpf' => '',
            'cnh' => ''
            )

            Errors: 
                1 - parametros
                4 - serdivor fora do ar,
                3 - outro,
                456 - Falha no ReCapcha

    */

function extrair_valor_tabela_imposto ($linha) {
    $tds = $linha->find("td");
    return $tds[sizeof($tds)-1]->find("label")[0]->innerHtml();
}

function extrair_valor_tabela_multa($linha, $coluna) {
    $td = $linha->find("td")[$coluna];
    return $td->find("label")[0]->innerHtml();
}

set_error_handler(function($code, $msg, $file, $line) {
    echo json_encode(array ('erro' => 'Erro inesperado. Tente novamente mais tarde.', 'id_erro' => '3','msg' => $msg, 'file' => $file, 'line' => $line));
    exit;
});   

if(array_key_exists('email', $_POST)){
    $email = trim($_POST['email']);
}

$params = array("renavan");
foreach ($params as $key => $param) {
    if(!array_key_exists($param, $_POST)){
        echo json_encode(array ('erro' => 'Erro interno do sistema. Tente novamente mais tarde.', 'id_erro' => '1', 'msg' => "Parametros incorretos."));
        exit;
    }
}

// Quebrar captcha
$url = "http://www.servicos.detran.ba.gov.br/pages/consultaveiculo/consultaveiculoindex.xhtml";
$captcha = captcha_service ($url, '6LdUTx0TAAAAAAYFbJNak8u3nfJfW4e4xtvOOWeu');

if($captcha === false) {
    echo json_encode(array ('erro' => 'Não possível resolver o Captcha. Tente novamente.', 'id_erro' => '456'));
    exit;
}

// Sessao iniciada em outra chamada a 'pontuacao.auth.v2.php'
$session = $_SESSION['jsession'];

// Fazer consulta
$dados = (object) array(
    'veiculoform' => 'veiculoform',
    'searchParamType' => 'consulta_renavam',
    'searchParam' => $_POST['renavan'],
    'g-recaptcha-response' => $captcha,
    $_SESSION['pesquisar_id'] => '',
    'javax.faces.ViewState' => $_SESSION['viewstate']
);
$consulta_inicial = SpLib::curl_post($url, $dados, array(
    CURLOPT_HEADER => 1,
    CURLOPT_FOLLOWLOCATION => 1        
), array(
    'Cookie: JSESSIONID=' . $session . ';',
    'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Encoding: gzip, deflate',
    'Accept-Language: pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4',
    'User-Agent: Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'
));

$regex = "/Captcha: Validation Error/i";
if(preg_match($regex, $consulta_inicial)) {
    echo json_encode(array ('erro' => 'Erro inesperado. Tente novamente mais tarde.', 'id_erro' => '3', 'msg' => 'Could not solve captcha.'));
    exit;
}

$sucesso_regex = "/Marca\/Modelo:/i";
if(!preg_match($sucesso_regex, $consulta_inicial)) {
    echo json_encode(array ('erro' => 'Registro não encontrado.', 'id_erro' => '3'));
    exit;
}

$dom_consulta_inicial = new Dom;
if(!$dom_consulta_inicial->load($consulta_inicial)) {
    echo json_encode(array ('erro' => 'Erro inesperado. Tente novamente.', 'id_erro' => '3', 'msg' => 'Nao foi possivel fazer parse da pagina de resultado da consulta.'));
    exit;    
}
$view_state_dom = $dom_consulta_inicial->find("input[name='javax.faces.ViewState']");
if(sizeof($view_state_dom) < 2) {
    echo json_encode(array ('erro' => 'Erro inesperado. Tente novamente.', 'id_erro' => '3', 'msg' => 'Nao foi possivel encontrar view state para abrir multas.'));
    exit;    
}

// Exibir multar (simular aperto do butão)
    
// Pegar id do buttao 'Exibir Multas'
$buttons = $dom_consulta_inicial->find("#idSpanMultas button");
if(sizeof($buttons) >= 2) {
    $exibir_multas_id = $buttons[0]->__get('id');
    
    $url = "http://www.servicos.detran.ba.gov.br/pages/consultaveiculo/consultaveiculoindex.xhtml";
    $post = array(
        "formVeiculos" => "formVeiculos",
        $exibir_multas_id => '',
        "javax.faces.ViewState" => $view_state_dom[1]->__get("value")
    );
    $consulta_multa = SpLib::curl_post($url, $post,  array(
        CURLOPT_HEADER => 1,
        CURLOPT_FOLLOWLOCATION => 1        
    ), array(
        'Cookie: JSESSIONID=' . $session . ';',
        'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding: gzip, deflate',
        'Accept-Language: pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4',
        'User-Agent: Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'
    ));
    if(!$consulta_multa) {
        echo json_encode(array ('erro' => 'Erro inesperado. Tente novamente.', 'id_erro' => '3', 'msg' => 'Nao foi possivel abrir lista de multas.'));
        exit;    
    }
} else {
    $consulta_multa = $consulta_inicial;
}

/* FORMATO DOS DADOS */
/* FORMATO DOS DADOS */
$dados_veiculo =  array(); // saida do json
$dados_veiculo['erro'] = 'nao';
$dados_veiculo['id_erro'] = '0';
$dados_veiculo['placa'] = null;
$dados_veiculo['chassi'] = null;
$dados_veiculo['renavam'] = $_POST['renavan'];
$dados_veiculo['veiculo'] = array(
    // 'Placa' => 'p',
);
$dados_veiculo['imposto'] = array();
$dados_veiculo['infracao']['multas'] = array(
    // 'cidade' => array (
    //     array('key' => 'value', 'Valor' => 'R$443')
    // )
);
$dados_veiculo['infracao']['autuacoes'] = array(
    // 'cidade' => array(
    //     array('chave' => 'valor')
    // )
);
            

/* EXTRAIR DADOS */
/* EXTRAIR DADOS */
$consulta_multa_dom = new Dom;
if(!$consulta_multa_dom->load($consulta_multa)) {
    echo json_encode(array('erro' => 'Erro inesperado. Tene novamente mais tarde.', 'id_erro' => '3'));
    exit;
}

/* 
    Dados sobre o veiculo.
*/
$body_of_tables = $consulta_multa_dom->find("#divResultadoVeiculo table");
if(sizeof($body_of_tables) < 5) {
    echo json_encode(array('erro' => 'Erro inesperado. Tene novamente mais tarde.', 'id_erro' => '3', 'msg' => 'Cant find table with id divResultadoVeiculo'));
    exit;
}
$table_tds = $body_of_tables[1]->find("td")->toArray();
$table_tds = array_merge($table_tds, $body_of_tables[2]->find("td")->toArray());
$number_of_pairs = (sizeof($table_tds)/2);
for ($var=0; $var < $number_of_pairs; $var++) { 
    $base = $var * 2;
    $key = $table_tds[$base]->innerHtml();
    $value = $table_tds[$base + 1]->find("label")->innerHtml();
    $dados_veiculo['veiculo'][$key] = $value;
}

/* 
    Dados sobre LICENCIAMENTO, SEGURO, IPVA
*/
$table_impostos = $body_of_tables[4];
$linhas = $table_impostos->find(".results-row");
if(sizeof($linhas) < 7) {
    trigger_error("Nâo foi possível encontrar linhas da tabela de seguro.");
}
// Licenciamento
$dados_veiculo['imposto']['Licenciamento'] = array(
    'Licenciamento Atual' => 'R$'.extrair_valor_tabela_imposto($linhas[0]),
    'Licenc. Exercícios Anteriores' =>  'R$'.extrair_valor_tabela_imposto($linhas[1])
);
// Seguro
$dados_veiculo['imposto']['Seguro'] = array(
    'Seguro Obrigatório' =>  'R$'.extrair_valor_tabela_imposto($linhas[2]),
    'Seguro Exercícios Anteriores' =>  'R$'.extrair_valor_tabela_imposto($linhas[3])
);
// IPVA
$dados_veiculo['imposto']['IPVA'] = array(
    'IPVA Atual' =>  'R$'.extrair_valor_tabela_imposto($linhas[4]),
    'IPVA Exercícios Anteriores' =>  'R$'.extrair_valor_tabela_imposto($linhas[5])
);


// Multas
$chaves = array(
    'Orgão Autuador', 'Descrição', 'Local', 'Data', 'Status', 'Valor'
);
$multas = array();
$multas_trs = $consulta_multa_dom->find("#multasAPagar table tr.ui-widget-content");
if(sizeof($multas_trs) > 0) {
    foreach ($multas_trs as $linha_key => $multa_tr) {
        $multa = array();
        for($i = 0; $i<sizeof($chaves); $i++) {
            if($i == 5) {
                $multa[$chaves[$i]] = 'R$'.extrair_valor_tabela_multa($multa_tr, $i);
            } else {
                $multa[$chaves[$i]] = extrair_valor_tabela_multa($multa_tr, $i);
            }
        }
        $multas[] = $multa;
    }
}
$dados_veiculo['infracao']['multas']['Bahia'] = $multas;
$json = json_encode($dados_veiculo);


// Buscar placa, chassi
foreach ($dados_veiculo['veiculo'] as $key => $value) {
    if(preg_match('/placa/i', $key)) {
        $placa = $value;
    }
    if(preg_match('/chassi/i', $key)) {
        $chassi = $value;
    }
}


if(!empty($email) && $placa && $chassi) {
    $db = new db();
    $db->query("call add_log_veiculo ('BA', '".$email."', '".$placa."', '".$chassi."', '".$_POST['renavan']."', '', '".$json."');");
}
echo $json;