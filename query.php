<?php

class query
{
  private $sql;
  private $result;
  private $buffer;
  private $has_fetch;
  private $db;
  
  public function query($db, $sql)
  {
    $this->db = $db;
    $this->sql = $sql;

    $this->result = $this->db->query($this->sql);
    $this->has_fetch = false;
  }

  public function fetch()
  {
    $this->has_fetch = true;
    return $this->buffer = $this->result->fetch_array(MYSQLI_BOTH);
  }

  public function sql() {
    return $this->sql;
  }

  public function fail(){
    return $this->db->fail();
  }
	
	public function erro(){
		return $this->db->erro();
	}

  public function numColumns() {
    return sizeof($this->buffer);
  }

  public function __destruct()
  {
      $this->db->getConn()->next_result();
  }

}
