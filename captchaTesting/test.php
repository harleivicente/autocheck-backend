<!-- TEST PAGE -->

<?php
    /* TESTS IF CAPTCHA WAS BROKEN */
    
    $response = $_GET['g-recaptcha-response'];
    $ip = $_GET['ip'];
    $url = "https://www.google.com/recaptcha/api/siteverify";
    $data = array(
        'secret' => '6LfT8jAUAAAAAKCVJ0J3ug-Tfqhb9aS937tdLYIV',
        'response' => $response,
        'remoteip' => $ip
    );

    $response = curl_post($url, $data, array(), array());
    error_log($ip);
    error_log($response);
    echo $response;

    /** 
    * Send a POST requst using cURL 
    *
    * @param string $url to request 
    * @param stdClass $post values to send 
    * @param array $options for cURL 
    * @param array $headers
    *
    * @return string 
    */ 
    function curl_post($url, $post, $options, $headers) 
    { 
        // Converter stdClass para Array associativo
        $post_array = array();
        foreach ($post as $key => $value) {
            $post_array[$key] = $value;
        }

        $defaults = array( 
            CURLOPT_POST => 1, 
            CURLOPT_HEADER => 0, 
            CURLOPT_URL => $url, 
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_FRESH_CONNECT => 1, 
            CURLOPT_RETURNTRANSFER => 1, 
            CURLOPT_FORBID_REUSE => 1, 
            CURLOPT_TIMEOUT => 20, 
            CURLINFO_HEADER_OUT => true,
            CURLOPT_POSTFIELDS => http_build_query($post_array)
        ); 

        $ch = curl_init(); 
        curl_setopt_array($ch, ($options + $defaults)); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch); 

        // DEBUG
        if($result === false)
            trigger_error("Error in curl_post. Error: " . curl_error($ch) . ". Http code: " . curl_getinfo($ch, CURLINFO_HTTP_CODE));

        // Logging
        // $log_message = ' - ';
        // $log_message .=  'Redirects: ';
        // $log_message .= (curl_getinfo($ch,  CURLINFO_REDIRECT_COUNT));
        // $log_message .=  ' - ';
        // $log_message .=  'Outgoing headers:';
        // $log_message .= (curl_getinfo($ch, CURLINFO_HEADER_OUT));
        // $log_message .=  ' - ';
        // $log_message .=  'Http code: ';
        // $log_message .= (curl_getinfo($ch, CURLINFO_HTTP_CODE ));
        // $log_message .=  ' - ';
        // $log_message .=  ' - ';
        // error_log($log_message);

        curl_close($ch); 
        return $result; 
    }
    ?>
