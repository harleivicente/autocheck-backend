<?php
    error_reporting(1);
    include_once "../library/Captcha.php";

    /*

        Resolva um ReCaptchaV2 do google.

        @param $_POST['url']
        @param $_POST['googlekey']

    */

    error_reporting(E_ALL);
    set_error_handler(function($code, $msg) {
        echo json_encode(array('erro' => $msg, 'id_erro' => 2));
        exit;
    });

    $params = array("url", "googlekey");
    foreach ($params as $key => $param) {
        if(!array_key_exists($param, $_POST)){
            echo json_encode(array ('erro' => 'sim', 'id_erro' => 1));
            exit;
        }
    }

    var_dump(captcha_service($_POST['url'], $_POST['googlekey']));
