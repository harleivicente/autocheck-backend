<?php

class db
{
  private $conn;

  public function db($server=DADOS_SERVER, $user=DADOS_USUARIO, $pass=DADOS_SENHA, $base=DADOS_BASE)
  {
  	$this->conn = new mysqli($server, $user, $pass, $base);
  }

  public function fail(){
    return mysqli_connect_errno();
  }
	
	public function erro(){	
		return mysqli_error($this->conn);
	}

  public function getConn(){
    return $this->conn;
  }

  public function close()
  {
    $this->conn->close();
    $this->conn = false;
  }

  public function query($sql)
  {
    return $this->conn->query($sql);
  }
	
	public function lastId(){
		return $this->conn->insert_id;
	}
};

?>
