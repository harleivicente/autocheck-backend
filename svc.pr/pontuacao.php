<?php

session_start();
error_reporting(0);

include_once "../config.php";
include_once "../inc.php";
include "../db.php";
include "../query.php";

header('content-type: application/json; charset=utf-8');

$captcha = trim($_POST['captcha']);
$validade = trim($_POST['data_habilitacao']);
$cnh = trim($_POST['cnh']);
$cpf = trim($_POST['cpf']);
$email = trim($_POST['email']);

if(!$captcha or !$cnh or !$cpf or !$validade)
{
    $json = array("erro" => "Um ou mais parametros invalidos", 'id_erro' => '1');
    echo jsonp_encode($json);
    exit;
}

// Inicializando variaveis
$dados_condutor =  array(); // saida do json

$dados_condutor['erro'] = 'nao';

$dados_condutor['id_erro'] = '0';

$dados_condutor['condutor'] = array(); // ficha do condutor
$dados_condutor['total_pontos'] = 0;
$dados_condutor['pontos'] = array();

$dados_condutor['cpf'] = $cpf;
$dados_condutor['cnh'] = $cnh;

$url = "http://www.extratodebito.detran.pr.gov.br/detranextratos/gerarExtratoPontuacao.do?action=viewExtract";

$ch = curl_init();

$dados_post = array(
    'cnh' => $cnh,
    'cpf' => $cpf,
    'tipoCnh' => '2',
    'senha' => $captcha,
    'validadeCnh' => $validade
);

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_REFERER, 'http://www.extratodebito.detran.pr.gov.br/detranextratos/gerarExtratoPontuacao.do?action=viewExtract');
curl_setproxy($ch, 'pr');

curl_setopt($ch, CURLOPT_HTTPHEADER, array("Origin: http://www.extratodebito.detran.pr.gov.br"));
curl_setopt($ch, CURLOPT_COOKIE, $_SESSION['cookie']);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($dados_post));

curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_NOBODY, 0);

$html = curl_exec($ch);

if(!$html) // estourou o timeout, erro no servidor do detran
{
    $json = array("erro" => utf8_encode("Servidor do Detran-PR temporariamente indispon�vel... :("), 'id_erro' => '1');
    echo jsonp_encode($json);
    exit;
} elseif(preg_match('/<p style="text-align:center;color: red"><img [^>]*>(.*?)<\/p>/sim', $html, $erro))
{
    $json = array("erro" => utf8_encode($erro[1]), 'id_erro' => '2');
    echo jsonp_encode($json);
    exit;
}


preg_match_all('/<table id="list_tabela"[^>]*>(.*?)<\/table>/sim', $html, $tbs);
$tbs = $tbs[1];

// -------------------------------------------------------------------------
// Buscar dados do condutor
// -------------------------------------------------------------------------

preg_match_all('/<td[^>]*>(.*?)<\/td>/sim', $tbs[0], $dados);
$dados = $dados[1];

foreach($dados as $k=>$col) {
    $dados[$k] = utf8_encode($col);
}

$dados_condutor['condutor']['Nome'] = $dados[1];
$dados_condutor['condutor']['RG'] = ( !$dados[3] or intval($dados[3]) == 0 ) ? "N/A" : $dados[3];
$dados_condutor['condutor']['CPF'] = $cpf;
$dados_condutor['condutor']['CNH'] = $cnh;
$dados_condutor['condutor']['Modelo'] = $dados[11];
$dados_condutor['condutor']['Categoria'] = $dados[13];
$dados_condutor['condutor']['Validade'] = $dados[16];
$dados_condutor['condutor'][ utf8_encode('Situa��o') ] = utf8_encode($dados[18]);

// -------------------------------------------------------------------------
// Buscar pontos do condutor
// -------------------------------------------------------------------------

$pontos = preg_split('/<hr\/>/sim', $tbs[2]);

array_pop($pontos); // remover a ultima linha (sempre em branco)

$dados_condutor['total_pontos'] = 0;

foreach($pontos as $ponto)
{
    preg_match_all('/<strong[^>]*>(.*?)<\/strong>/sim', $ponto, $ponto);
    $ponto = $ponto[1];

    //print_r($ponto);
    //exit;
    foreach($ponto as $k=>$col) {
        $ponto[$k] = utf8_encode($col);
    }

    $dados_condutor['total_pontos'] += intval($ponto[6]);

    $ponto = array(
        utf8_encode('Infra��o') => $ponto[4],
        utf8_encode('AIT') => $ponto[0],
        'Placa' => $ponto[1],
        'Data' => $ponto[2],
        'Hora' => $ponto[3],
        'Natureza' => $ponto[5],
        'Pontos' => $ponto[6],
        'Local' => $ponto[7],
        'Responsabilidade' => $ponto[8],
        utf8_encode('Apresenta��o do Condutor') => utf8_encode($ponto[9]),
    );

    $dados_condutor['pontos'][] = $ponto;

}

$json = jsonp_encode($dados_condutor);

if(!empty($email))
{
    $db = new db();
    $db->query("call add_log_condutor('PR', '".$email."', '".$cnh."', '', '', '".$cpf."', ".$dados_condutor['total_pontos'].", '".$json."');");
}

echo $json;