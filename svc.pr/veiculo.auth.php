<?php
/**
 * Created by PhpStorm.
 * User: Marcelo
 * Date: 02/03/14
 * Time: 19:43
 */

include '../config.php';
include '../inc.php';

session_start();

$url = "http://www.extratodebito.detran.pr.gov.br/detranextratos/geraExtrato.do?action=iniciarProcesso";

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);

curl_setproxy($ch, 'pr');

curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HEADER, 1);
curl_setopt($ch, CURLOPT_NOBODY, 0);

$html = curl_exec($ch);
//echo $html;

preg_match('/Set-Cookie: (.*?);/si', $html, $cookie);
$_SESSION['cookie'] = $cookie[1];

$url = "http://www.extratodebito.detran.pr.gov.br/detranextratos/jcaptcha.jpg?rndm=" . rand(1000000000, 9999999999);

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_REFERER, 'http://www.extratodebito.detran.pr.gov.br/detranextratos/geraExtrato.do?action=viewExtract');
curl_setopt($ch, CURLOPT_COOKIE, $_SESSION['cookie']);
curl_setproxy($ch, 'pr');

//curl_setopt($ch, CURLOPT_HTTPHEADER, array("Origin: http://www.detran.ba.gov.br"));
//curl_setopt($ch, CURLOPT_POST, true);
//curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($dados_post));
//curl_setopt($ch, CURLOPT_ENCODING, 'gzip');

curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_NOBODY, 0);

header('Content-type: image/jpeg');

echo curl_exec($ch);


