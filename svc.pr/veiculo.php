<?php
/**
 * Created by PhpStorm.
 * User: Marcelo
 * Date: 02/03/14
 * Time: 19:33
 */

session_start();
error_reporting(0);

include_once "../config.php";
include_once "../inc.php";
include "../db.php";
include "../query.php";

header('content-type: application/json; charset=utf-8');

$captcha = trim($_POST['captcha']);
$renavam = trim($_POST['renavan']);
$email = trim($_POST['email']);

if(!$captcha or !$renavam)
{
    $json = array("erro" => "Um ou mais parametros invalidos", 'id_erro' => '1');
    echo jsonp_encode($json);
    exit;
}

// Inicializando variaveis
$dados_veiculo =  array(); // saida do json

$dados_veiculo['erro'] = 'nao';
$dados_veiculo['id_erro'] = '0';

$dados_veiculo['placa'] = null;
$dados_veiculo['chassi'] = null;
$dados_veiculo['renavam'] = null;

$dados_veiculo['veiculo'] = array();
$dados_veiculo['imposto'] = array();

$dados_veiculo['infracao']['multas'] = array();
$dados_veiculo['infracao']['autuacoes'] = array();


$url = "http://www.extratodebito.detran.pr.gov.br/detranextratos/geraExtrato.do?action=viewExtract";

$ch = curl_init();

$vs_post = array(
    'renavam' => $renavam,
    'senha' => $captcha
);

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_REFERER, 'http://www.extratodebito.detran.pr.gov.br/detranextratos/geraExtrato.do?action=viewExtract');
curl_setproxy($ch, 'pr');

curl_setopt($ch, CURLOPT_HTTPHEADER, array("Origin: http://www.extratodebito.detran.pr.gov.br"));
curl_setopt($ch, CURLOPT_COOKIE, $_SESSION['cookie']);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($vs_post));

curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_NOBODY, 0);

$html = curl_exec($ch);

if(!$html) // estourou o timeout, erro no servidor do detran
{
    $json = array("erro" => utf8_encode("Servidor do Detran-PR temporariamente indispon�vel... :("), 'id_erro' => '1');
    echo jsonp_encode($json);
    exit;
}
elseif(preg_match('/<p style="text-align:center;color: red"><img [^>]*>(.*?)<\/p>/sim', $html, $erro))
{
    $json = array("erro" => utf8_encode($erro[1]), 'id_erro' => '2');
    echo jsonp_encode($json);
    exit;
}

// echo $html;
// exit;


//$html = file_get_contents('result_html_test.html'); // Arquivo HTML para testes. Tamb�m serve como "BACKUP" (Compara��o Vers�o Anterior) caso o Detran atualize o Layout.

// -------------------------------------------------------------------------
// MENU 1: Ve�culo ( Buscar informa��es Ve�culo )
// -------------------------------------------------------------------------

function dadosVeiculo( $html, $v ) {
    
    //$pattern = '/'.utf8_encode($v).'\:<\/label>.*?<div>(.*?)<\/div>/sim'; // Funcionar com $html teste => file_get_contents
    $pattern = '/'.$v.'\:<\/label>.*?<div>(.*?)<\/div>/sim'; // Funcionar com $html em produ��o => CURL

    preg_match($pattern, $html, $res);

    return trim( strip_tags( $res[1] ) ); //Remove espa�os em branco e codigos html
}

$dadosVeiculo = array (
 '0'  => 'Renavam',
 '1'  => 'Chassi',
 '2'  => 'Placa',
 '3'  => 'Marca\/Modelo',
 '4'  => 'Município',
 '5'  => 'Ano de fabrição\/modelo',
 '6'  => 'Combustível',
 '7'  => 'Cor',
 '8'  => 'Categoria',
 '9'  => 'Espécie\/Tipo',
 '10' => 'Situação do veículo',
 '11' => 'Tipo de Financiamento\/ Restrição'
 );

$veiculo = array();

foreach ($dadosVeiculo as $k => $v) {
    
    $veiculo[$k] = dadosVeiculo( $html, $v );

    //echo '<p>'.$v.': '.$veiculo[$k].'</p>';
}

$dados_veiculo['renavam'] = str_replace('.','', str_replace('-', '', $veiculo[0] ) );
$dados_veiculo['chassi'] = $veiculo[1];
$dados_veiculo['placa'] = str_replace('-', '', $veiculo[2] );

$veiculo[3] = array_map('trim', explode('/', $veiculo[3])); //Marca e Modelo
$veiculo[5] = array_map('trim', explode('/', $veiculo[5])); //Ano Fabrica��o e Ano Modelo
$veiculo[9] = array_map('trim', explode('/', $veiculo[9])); //Especie e Tipo

$dados_veiculo['veiculo']["Renavam"]                        = $veiculo[0];
$dados_veiculo['veiculo']["Chassi"]                         = $veiculo[1];
$dados_veiculo['veiculo']["Placa"]                          = $veiculo[2];
$dados_veiculo['veiculo']["Marca"]                          = $veiculo[3][0];
$dados_veiculo['veiculo']["Modelo"]                         = $veiculo[3][1];
$dados_veiculo['veiculo']['Município']         = $veiculo[4];
$dados_veiculo['veiculo']['Ano fabricação']    = $veiculo[5][0];
$dados_veiculo['veiculo']["Ano Modelo"]                     = $veiculo[5][1];
$dados_veiculo['veiculo']['Combustível']       = $veiculo[6];
$dados_veiculo['veiculo']["Cor"]                            = $veiculo[7];
$dados_veiculo['veiculo']["Categoria"]                      = $veiculo[8];
$dados_veiculo['veiculo']['Espécie']           = $veiculo[9][0];
$dados_veiculo['veiculo']["Tipo"]                           = $veiculo[9][1];
$dados_veiculo['veiculo']['Situação']          = $veiculo[10];
$dados_veiculo['veiculo']['Restrição']         = $veiculo[11];

//echo jsonp_encode($dados_veiculo['veiculo']);
//exit;


// -------------------------------------------------------------------------------------
// MENU 2: Impostos ( Buscar dados: IPVA | LICENCIAMENTO | DPVAT )
// -------------------------------------------------------------------------------------

function debitosImpostos( $html, $imposto ) {

    if( $imposto == 'TAXA DE LICENCIAMENTO' )
    {
        $pattern = '/('.$imposto.'.*)<\/td>\s*<td class="text-center">(?:\s*|\s*\d{2}\/\d{2}\/\d{4}\s*)<\/td>\s*<td class="text-right">(.*)/';
    }
    else
    {
        $pattern = '/<td>('.$imposto.'.*)\s*<\/td>\s*<td class="text-right">(.*)/';
    }

    preg_match_all($pattern, $html, $debitos, PREG_SET_ORDER, 0);

    for ($i=0; $i < count($debitos); $i++) 
        unset( $debitos[$i][0] ); //Percorre todo array e Exclui chaves [0]

    return $debitos;
}

$impostos = array(
    'ipva', 
    'licenciamento', 
    'seguro obrigatorio'
);

$total                                                   = 0; 
$dados_veiculo['imposto']['IPVA']['TOTAL']               = 0;
$dados_veiculo['imposto']['LICENCIAMENTO']['TOTAL']      = 0;
$dados_veiculo['imposto']['SEGURO OBRIGATORIO']['TOTAL'] = 0;

foreach ($impostos as $imposto) {

    $i   = strtoupper($imposto);
    $imp = ($imposto == 'licenciamento') ? 'TAXA DE LICENCIAMENTO' : $i;

    $debitos = debitosImpostos( $html, $imp );

    foreach ($debitos as $d)
    {
        $dados_veiculo['imposto'][$i][trim( $d[1] )] = trim( $d[2] );
        
        $valor = str_replace('.', '', $d[2]);
        $valor = str_replace(',', '.', $valor);
        //VALOR TOTAL de cada imposto
        $total += $valor;
    }

    $dados_veiculo['imposto'][$i]['TOTAL'] = number_format($total, 2, ',', '.');
    $total = 0;
}

//echo jsonp_encode($dados_veiculo['imposto']);
//exit;


// -------------------------------------------------------------------------
// MENU 3: Multas ( Buscar dados infra��es: MULTAS e NOTIFICA��ES )
// -------------------------------------------------------------------------

function debitosInfracoes( $html ) {
    
    $pattern = '/(.*)\s*<\/div>\s*<\/div>\s*<div class="[^"]*?">\s*<label>Situa��o:<\/label>\s*<div>(.*)<\/div>\s*<\/div>\s*<div class="[^"]*?">\s*<label>�rg�o Competente:<\/label>\s*<div>(.*)<\/div>\s*<\/div>\s*<div class="[^"]*?">\s*<label>Data:<\/label>\s*<div>(.*)<\/div>\s*<\/div>\s*<div class="[^"]*?">\s*<label>Hora:<\/label>\s*<div>(.*)<\/div>\s*<\/div>\s*<div class="[^"]*?">\s*<label>Infra��o:<\/label>\s*<div>(.*)<\/div>\s*<\/div>\s*<div class="[^"]*?">\s*<label>Local:<\/label>\s*<div>(.*)<\/div>\s*<\/div>\s*<div class="[^"]*?">\s*<label>Vencimento do Auto:<\/label>\s*<div>(.*)<\/div>\s*<\/div>\s*<div class="[^"]*?">\s*<label>Valor \(R\$\):<\/label>\s*<div>(.*)\s*<\/div>/';

    preg_match_all($pattern, $html, $debitos, PREG_SET_ORDER, 0);

    for ($i=0; $i < count($debitos); $i++) 
        unset( $debitos[$i][0] ); //Percorre todo array e Exclui chaves [0]

    return $debitos;
}

//$html_infracoes = file_get_contents('result_html_test_infracoes.html');

$debitos = debitosInfracoes( $html );

foreach ($debitos as $key => $debito) {

    $detalhes = array(
        utf8_encode('Infra��o')  => strip_tags( trim( $debito[6] ) ),
        'Auto'                   => strip_tags( trim( $debito[1] ) ),
        utf8_encode('Situa��o')  => strip_tags( trim( $debito[2] ) ),
        utf8_encode('Munic�pio') => strip_tags( trim( $debito[3] ) ),
        'Data'                   => strip_tags( trim( $debito[4] ) ),
        'Hora'                   => strip_tags( trim( $debito[5] ) ),
        'Local'                  => strip_tags( trim( $debito[7] ) ),
        'Data Vencimento'        => strip_tags( trim( $debito[8] ) ),
        'Valor'                  => 'R$ '.strip_tags( trim( $debito[9] ) )
    );      

    $orgao = $debito[3];
    
    if($debito[2] == 'Notificado')
        $dados_veiculo['infracao']['autuacoes'][$orgao][] = $detalhes;
    else
        $dados_veiculo['infracao']['multas'][$orgao][] = $detalhes;
}

//echo jsonp_encode($dados_veiculo['infracao']);
//exit;

$json = jsonp_encode($dados_veiculo);

if(!empty($email))
{
    $db = new db();
    $db->query("call add_log_veiculo ('PR', '".$email."', '".$dados_veiculo['placa']."', '".$dados_veiculo['chassi']."', '".$dados_veiculo['renavam']."', '', '".$json."');");
}

echo $json;