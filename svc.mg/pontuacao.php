<?php

include '../config.php';
include '../inc.php';
include "../db.php";
include "../query.php";

session_start();

header('content-type: application/json; charset=utf-8');

$cnh               = trim($_POST['cnh']);
$data_nascimento   = trim($_POST['data_nascimento']);
$data_habilitacao  = trim($_POST['data_habilitacao']);
$email = trim($_POST['email']);

/*
$cnh               = '04369737103';
$data_nascimento   = '08/09/1989';
$data_habilitacao  = '28/05/2008';
*/

if(!$cnh or !$data_nascimento or !$data_habilitacao)
{
   $json = array("erro" => "Um ou mais parametros invalidos", 'id_erro' => '1');
   echo jsonp_encode($json);
   exit;
}

$parms = array(
   '_method' => 'POST',
   'data[ConsultarPontuacaoCondutor][tipo_cnh]' => '1',
   'data[ConsultarPontuacaoCondutor][numero_cnh]' => $cnh,
   'data[ConsultarPontuacaoCondutor][data_nascimento]' => $data_nascimento,
   'data[ConsultarPontuacaoCondutor][data_primeira_habilitacao]' => $data_habilitacao
);

$url = "https://www.detran.mg.gov.br/habilitacao/cnh-e-permissao-para-dirigir/consulta-pontuacao/-/consulta_pontuacao_condutor/";

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_REFERER, "https://www.detran.mg.gov.br/habilitacao/cnh-e-permissao-para-dirigir/consulta-pontuacao/-/consulta_pontuacao_condutor/");

curl_setopt($ch, CURLOPT_HTTPHEADER, array("Origin: https://www.detran.mg.gov.br"));
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parms));

if(!empty($_SESSION['cookie']))
{
   curl_setopt($ch, CURLOPT_COOKIE, $_SESSION['cookie']);
}

curl_setproxy($ch, 'mg');

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_NOBODY, 0);

$tabela = curl_exec($ch);

if(!$tabela) // estourou o timeout, erro no servidor do detran
{
   echo jsonp_encode(array(
   "erro" => utf8_encode("O servidor do Detran indispon�vel. Tente mais tarde."),
   'id_erro' => 3
   ));

   exit;
}

preg_match("/<p id=\"nome_condutor\">(.*?)<\/p>/si", $tabela, $condutor);

if(!$condutor[1] and !preg_match('/Nao consta pontuacao para esse condutor/si', $tabela)) // n�o existem pontos!
{
  // Exemplo de erros:
  //Data de nascimento invalida
  //Data da primeira habilitacao invalida

  preg_match('/<div class="error-message">(.*?)<\/div>/sim', $tabela, $msg);

  if(!$msg[1])
  {
      preg_match('/<div id="flashMessage" class="message">(.*?)<\/div>/sim', $tabela, $msg);
  }

  echo jsonp_encode(array(
     "erro" => utf8_encode($msg[1]),
     'id_erro' => 2
  ));

  exit;

}

// Inicializando variaveis
$dados_condutor =  array(); // saida do json

$dados_condutor['erro'] = 'nao';

$dados_condutor['id_erro'] = '0';
$dados_condutor['cpf'] = null;
$dados_condutor['cnh'] = $cnh;
$dados_condutor['data_habilitacao'] = $data_habilitacao;
$dados_condutor['data_nascimento'] = $data_nascimento;

$dados_condutor['condutor'] = array(
    "Nome" => $condutor[1],
    "CNH" => $cnh
);
$dados_condutor['total_pontos'] = 0;
$dados_condutor['pontos'] = array();


preg_match_all("/<tr>(.*?)<\/tr>/si", $tabela, $tabela);

for($i = 0; isset($tabela[1][$i]) ; $i++)
{
   preg_match_all("/<td>(.*?)<\/td>/si", $tabela[1][$i], $ln);
}

foreach($tabela[1] as $ln)
{
   preg_match_all("/<td>(.*?)<\/td>/si", $ln,  $cols);

   if(sizeof($cols[1]) > 1)
   {
      $txt = explode("\n", $cols[1][1]);

      $cols[1][1] = trim($txt[2]);
      $cols[1][6] = trim(str_replace('&ensp;', '', $txt[1]));

      $dados_condutor['total_pontos'] += intval($cols[1][5]);

       $dados_condutor['pontos'][] = array(
          'Placa' => $cols[1][0],
          utf8_encode('infra��o') => $cols[1][1],
          'Data' => $cols[1][2],
          'Hora' => $cols[1][3],
          'Local' => $cols[1][4],
          'Pontos' => $cols[1][5],
          utf8_encode('C�digo') => $cols[1][6]
       );
   }
}

$json = jsonp_encode($dados_condutor);

if(!empty($email))
{
    $db = new db();
    $db->query("call add_log_condutor('MG', '".$email."', '".$cnh."', '".$data_nascimento."', '".$data_habilitacao."', '', ".$dados_condutor['total_pontos'].", '".$json."');");
}

echo $json;