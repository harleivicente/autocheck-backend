<?php

include '../config.php';

session_start();

//echo date(DATE_RFC822, time() + (3600*4) );

//echo $_SESSION['cookie'];

//exit;

$url = "https://www.detran.mg.gov.br/veiculos/situacao-do-veiculo/consulta-a-situacao-do-veiculo/";

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

curl_setproxy($ch, 'mg');

curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_HEADER, 1);
curl_setopt($ch, CURLOPT_NOBODY, 1);

$x = curl_exec($ch);

preg_match('/Set-Cookie: (.*?); expires=/si', $x, $cookie);

if(!empty($cookie[1]))
{
   $_SESSION['cookie'] = $cookie[1];
}

// -------------------------------------------------------------------------

$url = "https://www.detran.mg.gov.br/component/servicosmg/servico/-/captcha2/captcha/";

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);

curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('If-Modified-Since: ' . date(DATE_RFC822, time() + (3600*4) )));
curl_setopt($ch, CURLOPT_REFERER, "https://www.detran.mg.gov.br/veiculos/situacao-do-veiculo/consulta-a-situacao-do-veiculo");
curl_setopt($ch, CURLOPT_COOKIE, $_SESSION['cookie']);

curl_setproxy($ch, 'mg');

curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_NOBODY, 0);

header('Content-type: image/jpeg');

echo curl_exec($ch);

