<?php

include '../config.php';
include '../inc.php';
include "../db.php";
include "../query.php";

session_start();

header('content-type: application/json; charset=utf-8');

$placa = strtoupper(trim($_POST['placa']));
$chassi = strtoupper(trim($_POST['chassi']));
$captcha = strtolower(trim($_POST['captcha']));
$email = trim($_POST['email']);

if (!$placa or !$chassi or !$captcha) {
    $json = array("erro" => "Um ou mais parametros invalidos", 'id_erro' => '1');
    echo jsonp_encode($json);
    exit;
}

$url = "https://www.detran.mg.gov.br/veiculos/situacao-do-veiculo/consulta-a-situacao-do-veiculo/-/exibe_dados_veiculo/";

$ch = curl_init();

$data = array(
    "_method" => "POST",
    "data[ConsultarSituacaoVeiculo][placa]" => $placa,
    "data[ConsultarSituacaoVeiculo][chassi]" => $chassi,
    "data[ConsultarSituacaoVeiculo][captcha]" => $captcha
);

curl_setopt($ch, CURLOPT_URL, $url);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);

curl_setproxy($ch, 'mg');

curl_setopt($ch, CURLOPT_HTTPHEADER, array('Origin: https://www.detran.mg.gov.br'));
curl_setopt($ch, CURLOPT_REFERER, "https://www.detran.mg.gov.br/veiculos/situacao-do-veiculo/consulta-a-situacao-do-veiculo");
curl_setopt($ch, CURLOPT_COOKIE, $_SESSION['cookie']);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_NOBODY, 0);

$pg = curl_exec($ch);

if (!$pg) // estourou o timeout, erro no servidor do detran
{
    echo jsonp_encode(array(
        "erro" => utf8_encode("O servidor do Detran indispon�vel. Tente mais tarde."),
        'id_erro' => 4
    ));

    exit;
}

//echo $pg;
//exit;

// --------------------------------------------------------------------------
// Tratamento de erros

if (preg_match('/Chassi nao pertence ao veiculo/si', $pg))
{
    $json = array("erro" => utf8_encode("Chassi ou placa incorreto(a)!"), 'id_erro' => '2');
    echo jsonp_encode($json);
    exit;
}
elseif (preg_match('/Por favor, repita os caracteres ao lado/si', $pg))
{
    $json = array("erro" => utf8_encode("Conte�do da imagem n�o corresponde!"), 'id_erro' => '3');
    echo jsonp_encode($json);
    exit;
}

// Inicializando variaveis
$dados_veiculo = array(); // saida do json

$dados_veiculo['erro'] = 'nao';
$dados_veiculo['id_erro'] = '0';

$dados_veiculo['placa'] = $placa;
$dados_veiculo['chassi'] = $chassi;
$dados_veiculo['renavam'] = null;

$dados_veiculo['veiculo'] = array();
$dados_veiculo['imposto'] = array();

$dados_veiculo['infracao']['multas'] = array();
$dados_veiculo['infracao']['autuacoes'] = array();


// --------------------------------------------------------------------------

// parse da tabela de veiculos
preg_match('/<div class="dados-veiculo">(.*?)<div class="mais-opcoes">/si', $pg, $dados);
//preg_match_all('/<p id="[^>]*>(.*?)<\/p>/si', $dados[1], $dados);

preg_match_all('/<div class="segunda_coluna">(.*?)<\/div>/si', $dados[1], $segunda_coluna);

$dados = array();
foreach($segunda_coluna[1] as $coluna)
{
    preg_match('/<p id="[^>]*>(.*?)<\/p>/si', $coluna, $coluna);
    $dados[] = $coluna[1];
}

//print_r($dados);
//exit;

$especie_tipo_marca_modelo = explode('/', $dados[4]);

$dados_veiculo['veiculo'] = array(
    'Placa' => $dados[0],
    'Placa Anterior' => $dados[1] ? $dados[1] : 'N/A',
    utf8_encode('Munic�pio') => $dados[2],
    utf8_encode('Munic�pio anterior') => $dados[3] ? $dados[3] : 'N/A',
    utf8_encode('Esp�cie') => $especie_tipo_marca_modelo[0],
    'Tipo' => $especie_tipo_marca_modelo[1],
    'Marca' => $especie_tipo_marca_modelo[2],
    'Modelo' => $especie_tipo_marca_modelo[3],
    utf8_encode('Ano Fabrica��o') => $dados[5],
    'Ano Modelo' => $dados[6]
);

if(intval($dados[8]) == 1 or intval($dados[8]) == 2 or intval($dados[8]) == 3 or intval($dados[8]) == 4)
{
    $dados[8] = "Parcela ".$dados[8];
}

$dados_veiculo['imposto']['IPVA'][$dados[7]] = $dados[8];

if($dados[9])
{
    $dados_veiculo['imposto']['SEGURO OBRIGATORIO'][trim($dados[9])] = trim($dados[10]);
}
else
{
    $dados_veiculo['imposto']['SEGURO OBRIGATORIO'][trim($dados[11])] = trim($dados[12]);
}


$dados_veiculo['imposto']['LICENCIAMENTO'][$dados[13]] = $dados[14];

//print_r($dados_veiculo);
//exit;



// ---------------------------------------------------------------------------

// tabelas de multa e autuacoes
preg_match_all("/<table>(.*?)<\/table>/si", $pg, $tabelas);
$tabelas = $tabelas[1];

for ($ind_tb = 0; $tabelas[$ind_tb]; $ind_tb++)
{
    if (preg_match('/exibe_autuacoes_veiculo/si', $tabelas[$ind_tb])) // a tabela e de autuacao
    {
        $tipo_tb = 'autuacoes';
    }
    elseif (preg_match('/exibe_multas_veiculo/si', $tabelas[$ind_tb])) // a tabela � de multas
    {
        $tipo_tb = 'multas';
    }
    else
    {
        continue;
    }

    preg_match_all("/<a href=\"(.*?)\">/", $tabelas[$ind_tb], $urls);
    preg_match_all('/">(.*?)<\/a>/', $tabelas[$ind_tb], $orgaos);

    for ($i = 0; $urls[1][$i]; $i++) {
        $dados_veiculo['infracao'][$tipo_tb][$orgaos[1][$i]] = get_multas($urls[1][$i], 0, array(), $tipo_tb);
    }

}

// --------------------------------------------------------------------------------------------

// parse do numero de renavam

preg_match('/ulta_paga\/' . $placa . '\/(.*?)\//', $pg, $renavan);

$dados_veiculo['veiculo']['Renavam'] = $renavan[1];
$dados_veiculo['renavam'] = $renavan[1];


// --------------------------------------------------------------------------------------------
// buscar impedimento do veiculo


$url = "https://www.detran.mg.gov.br/veiculos/situacao-do-veiculo/consulta-a-situacao-do-veiculo/-/consulta_impedimentos_veiculo/" . $placa . "/";

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);

curl_setproxy($ch, 'mg');

curl_setopt($ch, CURLOPT_REFERER, "https://www.detran.mg.gov.br/veiculos/situacao-do-veiculo/consulta-a-situacao-do-veiculo/-/exibe_dados_veiculo/");
curl_setopt($ch, CURLOPT_COOKIE, $_SESSION['cookie']);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_NOBODY, 0);

$pg_imped = curl_exec($ch);

preg_match('/<\/h3><p>(.*?)<\/p><\/div>/', $pg_imped, $imped);

$dados_veiculo['veiculo']['Impedimento'] = $imped[1];


// ---------------------------------------------------------------------------------------------
// gerar a resposta

$json = jsonp_encode($dados_veiculo);

if(!empty($email))
{
    $db = new db();
    $db->query("call add_log_veiculo ('MG', '".$email."', '".$placa."', '".$dados_veiculo['chassi']."', '".$dados_veiculo['renavam']."', '', '".$json."');");
}

echo $json;




