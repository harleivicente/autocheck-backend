<?php

include '../config.php';
include '../inc.php';

session_start();

header('content-type: application/json; charset=utf-8');

$placa     = $_POST['placa'];
$renavam   = $_POST['renavam'];
$n         = $_POST['n'];
$val       = $_POST['valor'];

if(!$placa or !$renavam or !$n or !$val)
{
   $json = array("erro" => "Um ou mais parametros invalidos", 'id_erro' => '1');

   echo jsonp_encode($json);

   exit;
}

$encontrou = false;
for($i = 1 ; $i < 5 ; $i++)
{
   $url = "https://www.detran.mg.gov.br/veiculos/situacao-do-veiculo/emissao-de-extrato-de-multas/-/lista_tipo_infracoes_para_extrato/".$placa."/".$renavam."/0/0/0/0/0/0/".$i."/";

   $ch = curl_init();

   curl_setopt($ch, CURLOPT_URL, $url);
   curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
   curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
   curl_setopt($ch, CURLOPT_HTTPHEADER, array("Origin: https://www.detran.mg.gov.br"));
   curl_setopt($ch, CURLOPT_COOKIE, $_SESSION['cookie']);
   curl_setproxy($ch, 'mg');
   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($ch, CURLOPT_HEADER, 0);
   curl_setopt($ch, CURLOPT_NOBODY, 0);

   $listagem = curl_exec($ch);

   if( !(strpos($listagem, $n) === false) )
   {
      $encontrou = true;
      break;
   }

}

if(!$encontrou)
{
   $json = array("erro" => utf8_encode("Boleto indispon�vel no momento. Tente mais tarde."), 'id_erro' => '2');
   echo jsonp_encode($json);
   exit;
}

$listagem = curl_exec($ch);

preg_match_all("/<a href=\"\/veiculos\/situacao-do-veiculo\/(.*?)\"><img/si", $listagem, $links);

foreach($links[1] as $link)
{
   if(strpos($link, $n) !== false)
   {
      preg_match("/\/".$placa."\/".$renavam."\/(.*?)\/".$n."/si", $link, $param);
      break;
   }
}

// requisi��o da p�gina que gera o pdf de boleto
$url = "https://www.detran.mg.gov.br/veiculos/situacao-do-veiculo/emissao-de-extrato-de-multas/-/emissao_extrato_multa/".$placa."/".$renavam."/".$param[1]."/".$n."/".$val."/";

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_USERAGENT, USER_AGENT);
curl_setopt($ch, CURLOPT_TIMEOUT, TIMEOUT);
curl_setopt($ch, CURLOPT_COOKIE, $_SESSION['cookie']);
curl_setproxy($ch, 'mg');
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_NOBODY, 0);

$boleto = curl_exec($ch);

$tmp_pdf = tmp_file_name();
$tmp_txt = tmp_file_name();

file_put_contents($tmp_pdf, $boleto);

exec('pdftotext '.$tmp_pdf.' '.$tmp_txt);

$boleto = file_get_contents($tmp_txt);
$boleto = str_replace("\t", "", $boleto); // Removendo o caracter CR e deixando apenas o LF

// separando o c�digo de barras do boleto
preg_match("/vel: (.*?)\n\(AUTENTICA/si", $boleto, $codigo_barras);

$resposta = array(
   'codigo_barras' => $codigo_barras[1],
   'url' => $url,
   'erro' => 'nao',
   'id_erro' => '0'
);

echo jsonp_encode($resposta);
